// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // apiUrl: 'http://127.0.0.1:8000/api/',
  // apiUrl: 'https://cryptic-reaches-84550.herokuapp.com/api/',
  // apiUrl: 'https://fathomless-scrubland-53018.herokuapp.com/api/',
  apiUrl: 'https://fathomless-scrubland-53018.herokuapp.com/api/',
  firebase: {
    apiKey: 'AIzaSyDmx7h9FeuEnYdpEKbtNpIZ2Tl5diByo5M',
    authDomain: 'baremo-640e1.firebaseapp.com',
    projectId: 'baremo-640e1',
    storageBucket: 'baremo-640e1.appspot.com',
    messagingSenderId: '152620989430',
    appId: '1:152620989430:web:bfdfd5a4130229743ee5d7',
    measurementId: 'G-PPJ0CNZ4FD'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
