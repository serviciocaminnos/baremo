import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToolModule } from './pages/tool/tool.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { ChartsModule } from 'ng2-charts';
import { SharedModule } from './shared/shared.module';

import { LayoutModule } from '@angular/cdk/layout';
import { AngularMaterialModule } from './shared/material-angular/angular-material.module';
import { AngularFireModule } from '@angular/fire';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../environments/environment';


import { GraphicPieComponent } from './shared/components/graphic-pie/graphic-pie.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { SignInComponent } from './pages/sign-in/sign-in.component';
import { AddToolComponent } from './pages/add-tool/add-tool.component';
import { TestimonialsModule } from './module/caminnos/components/testimonials/testimonials.module';
@NgModule({
  declarations: [
    AppComponent,
    SignInComponent,
    AddToolComponent,
    GraphicPieComponent,
    PageNotFoundComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    FormsModule,
    AppRoutingModule,
    ToolModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    ChartsModule,
    SharedModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireStorageModule,
    AngularFirestoreModule,
    LayoutModule,
    AngularMaterialModule,
    TestimonialsModule,
  ],
  exports: [RouterModule],
  providers: [
    HttpClientModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
