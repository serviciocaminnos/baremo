import { Component } from '@angular/core';
import { slider } from './router-animation';
import { RouterOutlet, Router } from '@angular/router';
import { AuthConstants } from './core/config/auth-constanst';
import { StorageService } from './core/services/baremo/storage.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    slider
  ]
})
export class AppComponent {
  
  title = 'baremo';
  constructor ( private storage: StorageService, 
                private router : Router
  ) {
  }
  prepareRoute(outlet: RouterOutlet): any {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
  }
  verifyLocalStorage(): any{
    if (localStorage.length === 0){
      console.log('Iniciando Baremo');
    }else{
      this.storage.get(AuthConstants.AUTH).then(token => {
        if (token != null){
          this.router.navigate(['/tool']);
        }else{
          this.router.navigate(['/login']);
        }
      });
    }
  }
}
