import {
    trigger,
    transition,
    style,
    query,
    group,
    animateChild,
    animate,
    keyframes,
    state
  } from '@angular/animations';


export const slider =
  trigger('routeAnimations', [
    transition('Content => Tool', slideTo('left') ),
    transition('Tool => Content', slideTo('right') ),
    // transition('Content => Organization', slideTo('left') ),
    transition('Per => Org', slideTo('right') ),
    transition('Org => Eva', slideTo('right') ),
    transition('Eva => Org', slideTo('left') ),
    transition('Org => Per', slideTo('left') )
  ]);
export const iconStateBlock =
trigger('openClose', [
  // ...
  state('open', style({
    opacity: 1,
  })),
  state('closed', style({
    opacity: 0.5,
    transform: 'rotate(-180deg)'
  })),
  transition('open => closed', [
    animate('0.3s')
  ]),
  transition('closed => open', [
    animate('0.3s')
  ]),
]);
function slideTo(direction): any {
  const optional = { optional: true };
  return [
    query(':enter, :leave', [
      style({
        position: 'absolute',
        top: 0,
        [direction]: 0,
        width: '100%'
      })
    ], optional),
    query(':enter', [
      style({ [direction]: '-100%'})
    ]),
    group([
      query(':leave', [
        animate('600ms ease', style({ [direction]: '100%'}))
      ], optional),
      query(':enter', [
        animate('600ms ease', style({ [direction]: '0%'}))
      ])
    ]),
    // Normalize the page style... Might not be necessary

    // Required only if you have child animations on the page
    // query(':leave', animateChild()),
    // query(':enter', animateChild()),
  ];
}
