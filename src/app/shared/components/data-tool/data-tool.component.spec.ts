import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataToolComponent } from './data-tool.component';

describe('DataToolComponent', () => {
  let component: DataToolComponent;
  let fixture: ComponentFixture<DataToolComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataToolComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataToolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
