import { Component, OnInit, Input, DoCheck, AfterViewInit, OnChanges, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { AuthConstants } from 'src/app/core/config/auth-constanst';
import { EditComponent } from 'src/app/pages/tool/content/organization/components/edit/edit.component';
import { ToolService } from 'src/app/core/services/baremo/tool.service';
import { StorageService } from 'src/app/core/services/baremo/storage.service';
import { DataBridgeService } from 'src/app/core/services/baremo/data-bridge.service';
import { SendDataService } from 'src/app/core/services/baremo/SendData.service';

@Component({
  selector: 'app-data-tool',
  templateUrl: './data-tool.component.html',
  styleUrls: ['./data-tool.component.css']
})
export class DataToolComponent implements OnChanges {
  @Input() blocks;
  @Output() newAddEvent = new EventEmitter<any>();
  // blocks: [] = [];
  measures: [] = [];
  questions: [] = [];

  // toolContent;
  token;
  idUser;
  rol;
  /*params variables */
  varCss = '';
  idTool;
  idBlock;
  progressStatus = true;
  panelOpenState = false;
  titleTool: any;
  blockForm: FormGroup;
  measureForm: FormGroup;
  questionForm: FormGroup;
  rolhidden = true;
  hiddenProgressListBlock = false;
  hiddenProgressToolContent = true;

  constructor(
    private toolService: ToolService,
    private storage: StorageService,
    private router: Router,
    private formbuilder: FormBuilder,
    public dialog: MatDialog,
    public dataBridge: DataBridgeService,
  ) {
    this.buildForm();
    this.storage.get(AuthConstants.AUTH).then((token) => {
      this.token = token;
    });
    this.storage.get(AuthConstants.USER_ID).then((idUser) => {
      this.idUser = idUser;
    });
    this.storage.get(AuthConstants.ROL).then((rol) => {
      this.rol = rol;
    });
    this.storage.get(AuthConstants.ID_TOOL).then((idTool) => {
      this.idTool = idTool;
    });
  }
  ngOnChanges(): void {
    if (this.blocks === 'undefined' || Array.isArray(this.blocks) === false) {
      console.log('no se hace ninguna operación');
      // this.ngOnInit();
    } else {
        // console.log(this.toolContent[0]);
        // this.makeData(this.blocks);
        // console.log(this.blocks);
    }
    // console.log(this.toolContent[0]);
  }
  buildForm(): any{
    this.blockForm = this.formbuilder.group({
      tool_id: [''],
      name: ['Bloque'],
      porcentage: ['0'],
      description: ['Bloque de la herramienta'],
    });
    this.measureForm = this.formbuilder.group({
      block_id: [''],
      name: ['Medida'],
      porcentage: ['0'],
      description: ['Medida del bloque'],
    });
    this.questionForm = this.formbuilder.group({
      measure_id: [''],
      name: ['Pregunta'],
      description: ['Pregunta de la Medida'],
      porcentage: ['0'],
    });
  }

  addNewBlock(): any{
    // console.log(toolId);
    // tslint:disable-next-line: no-string-literal
    this.blockForm.controls['tool_id'].setValue(this.idTool);
    this.hiddenProgressListBlock = true;
    console.log(this.blockForm.value);
    const blockRequest = new SendDataService();
    blockRequest.createServiceName('block');
    blockRequest.addAuthorization(this.token);
    blockRequest.addResponseType('json');
    blockRequest.addData(this.blockForm.value);
    this.toolService.createData(blockRequest).subscribe(
      (blockData) => {
        // console.log(blockData);
        // tslint:disable-next-line: triple-equals
        if (blockData.status_code == '200') {
          // tslint:disable-next-line: no-string-literal
          this.measureForm.controls['block_id'].setValue(blockData.id);
          const measureRequest = new SendDataService();
          measureRequest.createServiceName('measure');
          measureRequest.addAuthorization(this.token);
          measureRequest.addResponseType('json');
          measureRequest.addData(this.measureForm.value);
          this.toolService.createData(measureRequest).subscribe(
            (measureData) => {
              // tslint:disable-next-line: triple-equals
              if (measureData.status_code == '200') {
                // tslint:disable-next-line: no-string-literal
                this.questionForm.controls['measure_id'].setValue(measureData.id);
                const questionRequest = new SendDataService();
                questionRequest.createServiceName('question');
                questionRequest.addAuthorization(this.token);
                questionRequest.addResponseType('json');
                questionRequest.addData(this.questionForm.value);
                this.toolService.createData(questionRequest).subscribe(
                  (questionData) => {
                    // tslint:disable-next-line: triple-equals
                    if (questionData.status_code == '200') {
                      // console.log(questionData);
                      this.newAddEvent.emit('addBlock');
                      this.hiddenProgressListBlock = false;
                    }
                  },
                  (error) => {
                    console.log(error);
                  }
                );
              }
            },
            (error) => {
              console.log(error);
            }
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }
  addNewMeasure(idBlock): any{
    this.hiddenProgressListBlock = true;
    // tslint:disable-next-line: no-string-literal
    this.measureForm.controls['block_id'].setValue(idBlock);
    const measureRequest = new SendDataService();
    measureRequest.createServiceName('measure');
    measureRequest.addAuthorization(this.token);
    measureRequest.addResponseType('json');
    measureRequest.addData(this.measureForm.value);
    this.toolService.createData(measureRequest).subscribe(
      (measureData) => {
        // tslint:disable-next-line: triple-equals
        if (measureData.status_code == '200') {
          // tslint:disable-next-line: no-string-literal
          this.questionForm.controls['measure_id'].setValue(measureData.id);
          const questionRequest = new SendDataService();
          questionRequest.createServiceName('question');
          questionRequest.addAuthorization(this.token);
          questionRequest.addResponseType('json');
          questionRequest.addData(this.questionForm.value);
          this.toolService.createData(questionRequest).subscribe(
            (questionData) => {
              // tslint:disable-next-line: triple-equals
              if (questionData.status_code == '200') {
                // console.log(questionData);
                this.newAddEvent.emit('addMeasure');
                this.hiddenProgressListBlock = false;
              }
            },
            (error) => {
              console.log(error);
            }
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }
  addNewQuestion(idMeasure): any {
    this.hiddenProgressListBlock = true;
    // tslint:disable-next-line: no-string-literal
    this.questionForm.controls['measure_id'].setValue(idMeasure);
    const questionRequest = new SendDataService();
    questionRequest.createServiceName('question');
    questionRequest.addAuthorization(this.token);
    questionRequest.addResponseType('json');
    questionRequest.addData(this.questionForm.value);
    this.toolService.createData(questionRequest).subscribe(
      (questionData) => {
        // tslint:disable-next-line: triple-equals
        if (questionData.status_code == '200') {
          // console.log(questionData);
          this.hiddenProgressListBlock = false;
          this.router.navigateByUrl('/tool/content/organization', { skipLocationChange: true }).then(() => {
            this.router.navigate(['content/organization']);
          });
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }
  navigationBlock(position, idBlock): any{
    this.storage.store(AuthConstants.BLOCK_POSITION, position);
    this.storage.store(AuthConstants.MEASURE_POSITION, '1');
    this.storage.store(AuthConstants.ID_BOCK, idBlock);
    this.router.navigateByUrl('/tool/content/organization', { skipLocationChange: true }).then(() => {
      this.router.navigate(['content/organization']);
    });
  }
  navigationMeasure(position, idBlock, blockPosition): any {
    this.storage.store(AuthConstants.ID_BOCK, idBlock);
    this.storage.store(AuthConstants.MEASURE_POSITION, position);
    this.storage.store(AuthConstants.BLOCK_POSITION, blockPosition);
    this.router.navigateByUrl('/tool/content/organization', { skipLocationChange: true }).then(() => {
      this.router.navigate(['content/organization']);
    });
  }
  navigation(dBlock: any, dMeasure: [], measurePosition: number): void {
    this.storage.store(AuthConstants.ID_BOCK, dBlock.id);
    if (Array.isArray(dMeasure) === true) {
      this.storage.store(AuthConstants.MEASURE_POSITION, '1');
    } else {
      this.storage.store(AuthConstants.MEASURE_POSITION, measurePosition);
    }
    this.storage.store(AuthConstants.BLOCK_POSITION, dBlock.position);

    const dataBlock = {
      block: dBlock,
      measure: dMeasure,
      measurePos: measurePosition
    };
    this.dataBridge.sendObjectSource(dataBlock);
  }
  remove(opt, idOption): void {
    const dialogRef = this.dialog.open(EditComponent, {
      // padding: '0px',
      width: '40%',
      height: '50%',
      data: {
        id: idOption,
        option: 'remove',
        option2: opt
      }
    });
  }

}
