import { Component, OnInit } from '@angular/core';
import { AuthConstants } from 'src/app/core/config/auth-constanst';
import { User } from 'src/app/core/model/baremo/user.model';
import { AuthService } from 'src/app/core/services/baremo/auth.service';
import { SendDataService } from 'src/app/core/services/baremo/SendData.service';
import { StorageService } from 'src/app/core/services/baremo/storage.service';
import { ToolService } from 'src/app/core/services/baremo/tool.service';
declare var M: any;
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  idUser;
  token;
  hidden = false;
  user: User;
  constructor(
    private storage: StorageService,
    private tool: ToolService,
    private session: AuthService)
  {
    this.storage.get(AuthConstants.USER_ID).then(data => {
      this.idUser = data;
    });
    this.storage.get(AuthConstants.AUTH).then(data => {
      this.token = data;
    });
  }

  ngOnInit(): any {
    this.getUser();
    this.verifyToken();
  }
  getUser(): any {
    console.log(this.token);
    const getUser = new SendDataService();
    getUser.createServiceName('user');
    getUser.addAuthorization(this.token);
    getUser.addParams(this.idUser);
    this.tool.readData(getUser).subscribe((res) => {
      console.log(res);
      this.user = res;
    });
  }
  logout(): any {
    this.session.logout();
  }
  verifyToken(): any {
    this.storage.get(AuthConstants.AUTH).then(data => {
      this.token = data;
      if (this.token != null) {
        this.hidden = true;
      }else{
        this.hidden = false;
      }
    });
  }
}
