import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthConstants } from 'src/app/core/config/auth-constanst';
import { Answer } from '../../../core/model/baremo/answer.model';
import { FormGroup, FormArray, Validators, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';
import { StorageService } from 'src/app/core/services/baremo/storage.service';
import { ToolService } from 'src/app/core/services/baremo/tool.service';
import { SendDataService } from 'src/app/core/services/baremo/SendData.service';
import { Block } from 'src/app/core/model/baremo/block.model';
import { Tool } from 'src/app/core/model/baremo/tool.model';
import { Measure } from 'src/app/core/model/baremo/measure.model';
import { Question } from 'src/app/core/model/baremo/question.model';
import { Crud } from 'src/app/core/model/baremo/crud.model';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  editImg = 'assets/images/edit.svg';
  token: string;
  state: boolean;
  block: Block;
  answer: Answer;
  tool: Tool;
  measure: Measure;
  question: Question;
  uploadPercent: Observable<number>;
  imageSrc: Observable<any>;

  blockForm: FormGroup;
  measureForm: FormGroup;
  questionForm: FormGroup;
  typeAnswerForm: FormGroup;
  toolForm: FormGroup;
  statusUpload: boolean;

  toolImg: string;
  options: any;
  showEmptyMessage: boolean;
  emptyMessage: string;
  makeNewAnswer: boolean;
  contentEditTypeAnswerArray: boolean;
  answersQuestion: any;
  crud: string;
  auxArrayForm: [];
  answerForm: FormGroup;

  messageError  = '';

  title: string = '';
  get nameT(): any {
    return this.toolForm.get('name');
  }
  get descriptionT(): any {
    return this.toolForm.get('description');
  }
  get nameB(): any {
    return this.blockForm.get('name');
  }
  get descriptionB(): any {
    return this.blockForm.get('description');
  }
  get nameM(): any {
    return this.measureForm.get('name');
  }
  get descriptionM(): any {
    return this.measureForm.get('description');
  }
  get nameQ(): any {
    return this.questionForm.get('name');
  }
  get descriptionQ(): any {
    return this.questionForm.get('description');
  }
  get porcentageQ(): any {
    return this.questionForm.get('porcentage');
  }
  get porcentageM(): any {
    return this.measureForm.get('porcentage');
  }
  get porcentageA(): any {
    return this.answerForm.get('porcentage');
  }

  public errorsMessages = {
    name: [
      { type: 'required', message: 'Nombre es requerido' },
      { type: 'minlength', message: '2 caracteres como minimo' }
    ],
    description: [
      { type: 'required', message: 'Description es requerido' },
      { type: 'minlength', message: '2 caracteres como minimo' }
    ],
    porcentage: [
      { type: 'required', message: 'Password es requerido' },
      { type: 'max', message: 'Los caracteres sobrepasan del total' },
      { type: 'min', message: ' caracteres como minimo' }
    ]
  };
  constructor(
    public dialogRef: MatDialogRef<EditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Crud,
    private storage: StorageService,
    private snackBar: MatSnackBar,
    private toolService: ToolService,
    private formBuilder: FormBuilder,
    private fireStorage: AngularFireStorage,
  ) {
    this.dialogRef.disableClose = true;
    this.buildForm();
  }
  buildForm(): any {
    this.toolForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(2)]],
      description: ['', [Validators.required, Validators.minLength(2)]],
      url: [''],
    });
    this.blockForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(2)]],
      description: ['', [Validators.required, Validators.minLength(2)]]
    });
    this.measureForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(2)]],
      description: ['', [Validators.required, Validators.minLength(2)]],
      porcentage: ['', [Validators.max(100), Validators.min(0)]],
    });
    this.questionForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(2)]],
      description: ['', [Validators.required, Validators.minLength(2)]],
      porcentage: ['', [Validators.max(100), Validators.min(0)]],
    });
    this.typeAnswerForm = this.formBuilder.group({
      idAnswer: [''],
      answers: this.formBuilder.array([])
    });
    this.answerForm = this.formBuilder.group({
      id_question: ['', [Validators.required]],
      name: ['Nueva respusta'],
      porcentage: [10, [Validators.min(0)]],
      sub_answer: [''],
      need: [''],
      idGAns: [],
    });
  }

  ngOnInit(): void {
    this.storage.get(AuthConstants.AUTH).then((token: string) => {
      this.token = token;
      switch (this.data.typeData) {
        case 'tool':
          this.getToolData();
          this.title = 'Herramienta';
          break;
          case 'block':
            this.getBlockData();
            this.title = 'Bloque';
            break;
            case 'measure':
              this.getMeasureData();
              this.title = 'Medida';
              break;
              case 'question':
                this.getQuestionData();
                this.title = 'Pregunta';
          break;
        case 'answer':
          this.getAnswerData();
          break;
      }
    });
  }
  getToolData(): void {
    this.state = true;
    const request = new SendDataService();
    request.addAuthorization(this.token);
    request.createServiceName('tool');
    request.addParams(this.data.idData);
    this.toolService.readData(request).subscribe(
      (tool: Tool) => {
        this.tool = tool;
        this.state = false;
        this.toolImg = tool.url;
        this.toolForm.patchValue(tool);
      },
      (error: any) => {
        console.log(error);
      }
    );
  }
  getBlockData(): void {
    this.state = true;
    const request = new SendDataService();
    request.addAuthorization(this.token);
    request.createServiceName('block');
    request.addParams(this.data.idData);
    this.toolService.readData(request).subscribe(
      (block: Block) => {
        this.block = block;
        this.state = false;
        this.blockForm.patchValue(block);
      },
      (error: any) => {
        console.log(error);
      }
    );
  }
  getMeasureData(): void {
    this.state = true;
    const request = new SendDataService();
    request.addAuthorization(this.token);
    request.createServiceName('measure');
    request.addParams(this.data.idData);
    this.toolService.readData(request).subscribe(
      (measure: Measure) => {
        this.measure = measure;
        this.state = false;
        this.measureForm.patchValue(measure);
      },
      (error: any) => {
        console.log(error);
      }
    );
  }
  getQuestionData(): void {
    this.state = true;
    const request = new SendDataService();
    request.addAuthorization(this.token);
    request.createServiceName('question');
    request.addParams(this.data.idData);
    this.toolService.readData(request).subscribe(
      (question: Question) => {
        this.question = question;
        this.state = false;
        this.questionForm.patchValue(question);
        this.verifyAnswerType();
      },
      (error: any) => {
        console.log(error);
      }
    );
  }
  getAnswerData(): void {
    this.state = true;
    const request = new SendDataService();
    request.addAuthorization(this.token);
    request.createServiceName('answer');
    request.addParams(this.data.idData);
    this.toolService.readData(request).subscribe(
      (answer: Answer) => {
        this.answer = answer;
        this.state = false;
      },
      (error: any) => {
        console.log(error);
      }
    );
  }
  update(): void {
    this.state = true;
    const request = new SendDataService();
    request.addAuthorization(this.token);
    request.createServiceName(this.data.typeData);
    request.addParams(this.data.idData);
    switch (this.data.typeData) {
      case 'tool':
        request.addData(this.toolForm.value);
        break;
      case 'block':
        request.addData(this.blockForm.value);
        break;
      case 'measure':
        request.addData(this.measureForm.value);
        break;
      case 'question':
        request.addData(this.questionForm.value);
        break;
      case 'answer':
        request.addData(this.typeAnswerForm.value);
        break;
    }
    this.toolService.updateData(request).subscribe(
      (res: any) => {
        if (res.status_code === 200) {
          switch (this.data.typeData) {
            case 'tool':
              this.openSnackBar('Herramienta actualizado con éxito');
              this.dialogRef.close('200');
              break;
            case 'block':
              this.openSnackBar('Bloque actualizado con éxito');
              const data = {
                status_code: '200',
                position: this.block.position
              };
              this.dialogRef.close(data);
              break;
              case 'measure':
                this.openSnackBar('Medida actualizado con éxito');
                this.dialogRef.close('200');
                break;
              case 'question':
                this.state = true;
                this.answers().clear();
                this.getQuestionData();
                this.openSnackBar('Pregunta actualizado con éxito');
                break;
              case 'answer':
                this.openSnackBar('Respuesta actualizado con éxito');
                this.dialogRef.close('200');
                break;
          }
        }
      },
      (error: any) => {
        console.log(error);
        alert('Ocurrio un error, vuelva a intentarlo');
        this.state = false;
      }
    );
  }

  upload(event: any): void {
    this.statusUpload = true;
    const file = event.target.files[0];
    const dir = 'tool-image-profile/' + this.toolForm.controls.name.value;
    const fileRef = this.fireStorage.ref(dir);
    const task = this.fireStorage.upload(dir, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges()
    .pipe(
      finalize(() => {
        this.statusUpload = false;
        this.imageSrc = fileRef.getDownloadURL();
        this.openSnackBar('Imagen guardada en la nube exitosamente');
        this.imageSrc.subscribe(url => {
          this.toolForm.controls.url.setValue(url);
        });
      })
    ).subscribe();
    // console.log(dir);
  }

  openSnackBar(message: string): void {
    this.snackBar.open(message, 'Done' , {
      duration: 3000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }
  verifyAnswerType(): void {
    this.state = true;
    const reqQ = new SendDataService();
    reqQ.addAuthorization(this.token);
    reqQ.createServiceName('globalAnswersByQuestion');
    reqQ.addParams(this.question.id);
    this.toolService.readData(reqQ).subscribe(
      res => {
        if (Array.isArray(res) === true) {
          console.log(res);
          // this.getAllAnswerByIdQuestion(res.answer_type_id);
          this.generateTypesAnswer(res, true);
          this.crud = 'update';
          this.answersQuestion = res;
          this.showEmptyMessage = false;
          this.state = false;
        } else {
          this.crud = 'create';
          this.emptyMessage = 'Aun no tienes guardado nunguna respuesta';
          this.showEmptyMessage = true;
          this.state = false;
        }
        this.state = false;
      },
      error => {
        console.log(error);
      }
    );
  }
  getTypeAnswers(): void {
    const typeAnswerRequest = new SendDataService();
    typeAnswerRequest.addAuthorization(this.token);
    typeAnswerRequest.createServiceName('answerType');
    this.toolService.readData(typeAnswerRequest).subscribe(
      res => {
        this.options = res;
        // console.log(res);
      }, error => {
        console.log(error);
      }
    );
  }
  // tslint:disable-next-line: typedef
  async modifyTypeAnswerByQuestion(option){
    if (this.crud === 'create') {
      await this.answers().clear();
      await this.getAllAnswerByAnswerByTypeId(option.id);
      this.makeNewAnswer = false;
    }
  }
  // tslint:disable-next-line: typedef
  async makeNewTypeAnswer(){
    if (this.crud === 'create') {
      await this.answers().clear();
      this.addAnswer('Nueva respuesta', 0, '', '', 0);
      this.makeNewAnswer = true;
      if (this.answers().length !== 0){
        this.showEmptyMessage = false;
      } else {
        this.showEmptyMessage = true;
      }
    }
  }
  getAllAnswerByAnswerByTypeId(answerTypeId): void {
    const reqTypeAnswer = new SendDataService();
    reqTypeAnswer.addAuthorization(this.token);
    reqTypeAnswer.createServiceName('getAnswerOfAnswerType');
    reqTypeAnswer.addParams(answerTypeId);
    this.toolService.readData(reqTypeAnswer).subscribe(
      res => {
        console.log(res);
        this.generateTypesAnswer(res, false);
      },
      error => {
        console.log(error);
      }
    );
  }
  // tslint:disable-next-line: typedef
  async generateTypesAnswer(answerList: any, option: boolean){
    this.contentEditTypeAnswerArray =  true;
    if (option === true) {
      answerList.forEach(answer => {
        this.addAnswer (answer.name, answer.porcentage, answer.sub_answer, answer.need, answer.id);
      });
    } else {
      answerList.forEach(answer => {
        this.addAnswer(answer.name, answer.porcentage, '', '', null);
      });
    }
  }

  answers(): FormArray {
    return this.typeAnswerForm.get('answers') as FormArray;
  }
  newAnswer(name: string, porct: number, subAnswer: string, need: string, id: number): FormGroup {
    return this.formBuilder.group({
      id_question: [this.question.id, [Validators.required]],
      name: [name],
      porcentage: [porct, [Validators.min(0)]],
      sub_answer: [subAnswer],
      need: [need],
      idGAns: [id],
    });
  }
  addAnswer(name: string, porct: number, subAnswer: string, need: string, id: number): any{
    this.answers().push(this.newAnswer(name, porct, subAnswer, need, id));
  }
  removeAnswer(index): void{
    this.answers().removeAt(index);
  }
  saveAnswers(): void {
    this.state = true;
    if (this.crud === 'create') {
      const answerArray: [] = this.typeAnswerForm.controls.answers.value;
      answerArray.forEach((answer: any) => {
        const req = new SendDataService();
        req.addAuthorization(this.token);
        req.addData(answer);
        req.createServiceName('globalAnswerQuestion');
        this.toolService.createData(req).subscribe((res: any) => {
          console.log(res);
        }, error => {
          console.log(error);
        });
        // console.log(answer);
      });
      this.openSnackBar('Respuestas actualizadas con éxito');
      this.state = false;
      // this.dialogRef.close('200');
    } else {
      if (this.crud === 'update') {
        const answerArray: [] = this.typeAnswerForm.controls.answers.value;
        answerArray.forEach((answer: any) => {
          const req = new SendDataService();
          req.addAuthorization(this.token);
          req.addData(answer);
          req.addParams(answer.idGAns);
          req.createServiceName('globalAnswerQuestion');
          this.toolService.updateData(req).subscribe((res: any) => {
            // console.log(res);
          }, error => {
            console.log(error);
          });
          // console.log(answer);
        });
        this.openSnackBar('Respuestas actualizadas con éxito');
        this.state = false;
        // this.dialogRef.close('200');
      }
    }
  }
  // tslint:disable-next-line: typedef
  addNewAnswer() {
    this.state = true;
    this.answerForm.controls.id_question.setValue(this.question.id);
    const req = new SendDataService();
    req.addAuthorization(this.token);
    req.addData(this.answerForm.value);
    req.createServiceName('globalAnswerQuestion');
    this.toolService.createData(req).subscribe((res: any) => {
      // console.log(res);
      this.answers().clear();
      this.verifyAnswerType();
    }, error => {
      console.log(error);
    });
  }
  destroyAnswer(idGAns): void{
    this.state = true;
    const req = new SendDataService();
    req.addAuthorization(this.token);
    req.addParams(idGAns);
    req.createServiceName('globalAnswerQuestion');
    this.toolService.deleteData(req).subscribe((res: any) => {
      // console.log(res);
      this.answers().clear();
      this.verifyAnswerType();
    }, error => {
      console.log(error);
    });
  }
}
