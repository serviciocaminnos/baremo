import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthConstants } from 'src/app/core/config/auth-constanst';
import { Answer } from '../../../core/model/baremo/answer.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ToolService } from 'src/app/core/services/baremo/tool.service';
import { StorageService } from 'src/app/core/services/baremo/storage.service';
import { SendDataService } from 'src/app/core/services/baremo/SendData.service';
import { Block } from 'src/app/core/model/baremo/block.model';
import { Measure } from 'src/app/core/model/baremo/measure.model';
import { Question } from 'src/app/core/model/baremo/question.model';
import { Tool } from 'src/app/core/model/baremo/tool.model';
import { Crud } from 'src/app/core/model/baremo/crud.model';

@Component({
  selector: 'app-remove',
  templateUrl: './remove.component.html',
  styleUrls: ['./remove.component.css']
})
export class RemoveComponent implements OnInit {
  token: any;
  block: Block;
  measure: Measure;
  question: Question;
  answer: Answer;
  state: boolean;
  tool: Tool;
  title: string = '';
  constructor(
    public dialogRef: MatDialogRef<RemoveComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Crud,
    private toolService: ToolService,
    private storage: StorageService,
    private snackBar: MatSnackBar,
  ) {
    this.dialogRef.disableClose = true;
  }

  ngOnInit(): void {
    this.storage.get(AuthConstants.AUTH).then((token) => {
      this.token = token;
      switch (this.data.typeData) {
        case 'tool':
          this.getToolData();
          this.title = 'Herramienta';
          break;
          case 'block':
            this.getBlockData();
            this.title = 'Bloque';
            break;
            case 'measure':
              this.getMeasureData();
              this.title = 'Medida';
              break;
              case 'question':
                this.getQuestionData();
                this.title = 'Pregunta';
          break;
        case 'answer':
          this.getAnswerData();
          break;
      }
    });
  }
  getToolData(): void {
    this.state = true;
    const request = new SendDataService();
    request.addAuthorization(this.token);
    request.createServiceName('tool');
    request.addParams(this.data.idData);
    this.toolService.readData(request).subscribe(
      (tool: Tool) => {
        this.tool = tool;
        this.state = false;
      },
      (error: any) => {
        console.log(error);
      }
    );
  }
  getBlockData(): void {
    this.state = true;
    const request = new SendDataService();
    request.addAuthorization(this.token);
    request.createServiceName('block');
    request.addParams(this.data.idData);
    this.toolService.readData(request).subscribe(
      (block: Block) => {
        this.block = block;
        this.state = false;
      },
      (error: any) => {
        console.log(error);
      }
    );
  }
  getMeasureData(): void {
    this.state = true;
    const request = new SendDataService();
    request.addAuthorization(this.token);
    request.createServiceName('measure');
    request.addParams(this.data.idData);
    this.toolService.readData(request).subscribe(
      (measure: Measure) => {
        this.measure = measure;
        this.state = false;
      },
      (error: any) => {
        console.log(error);
      }
    );
  }
  getQuestionData(): void {
    this.state = true;
    const request = new SendDataService();
    request.addAuthorization(this.token);
    request.createServiceName('question');
    request.addParams(this.data.idData);
    this.toolService.readData(request).subscribe(
      (question: Question) => {
        this.question = question;
        this.state = false;
      },
      (error: any) => {
        console.log(error);
      }
    );
  }
  getAnswerData(): void {
    this.state = true;
    const request = new SendDataService();
    request.addAuthorization(this.token);
    request.createServiceName('answer');
    request.addParams(this.data.idData);
    this.toolService.readData(request).subscribe(
      (answer: Answer) => {
        this.answer = answer;
        this.state = false;
      },
      (error: any) => {
        console.log(error);
      }
    );
  }
  remove(): void {
    this.state = true;
    const request = new SendDataService();
    request.addAuthorization(this.token);
    request.createServiceName(this.data.typeData);
    request.addParams(this.data.idData);
    this.toolService.deleteData(request).subscribe(
      (res: any) => {
        if (res.status_code === 200) {
          switch (this.data.typeData) {
            case 'tool':
              this.openSnackBar('Herramienta eliminada con éxito');
              break;
            case 'block':
              this.openSnackBar('Bloque eliminado con éxito');
              break;
            case 'measure':
              this.openSnackBar('Medida eliminada con éxito');
              break;
            case 'question':
              this.openSnackBar('Pregunta eliminada con éxito');
              break;
            case 'answer':
              this.openSnackBar('Respuesta eliminada con éxito');
              break;
          }
          this.dialogRef.close('200');

        } else {
        }
      },
      (error: any) => {
        console.log(error);
        alert('Ocurrio un error, vuelva a intentarlo');
        this.state = false;
      }
    );
  }
  openSnackBar(message: string): void {
    this.snackBar.open(message, 'Done' , {
      duration: 3000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: 'custom-snackbar-container'
    });
  }
}
