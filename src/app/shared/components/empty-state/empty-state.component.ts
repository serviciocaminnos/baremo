import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-empty-state',
  templateUrl: './empty-state.component.html',
  styleUrls: ['./empty-state.component.css']
})
export class EmptyStateComponent implements OnInit {

  @Input() data = {
    empty: 'Contar un breve resumen acerca de tu proyecto ayuda a las personas a saber mas sobre ello',
    assets: 'assets/images/caminnos/no-data-about.png',
  };
  constructor() { }

  ngOnInit(): void {
  }

}
