import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataToolComponent } from './components/data-tool/data-tool.component';

import { RolPipe } from './pipe/rol/rol.pipe';
import { RemoveComponent } from './components/remove/remove.component';
import { AngularMaterialModule } from './material-angular/angular-material.module';
import { EditComponent } from './components/edit/edit.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { LoadComponent } from './components/load/load.component';
import { EmptyStateComponent } from './components/empty-state/empty-state.component';
import { HeaderComponent } from './components/header/header.component';
import { ConstructionComponent } from './components/construction/construction.component';
import { SafePipe } from './pipe/safe/safe.pipe';
import { CategoryPipe } from './pipe/category/category.pipe';



@NgModule({
  declarations: [
    DataToolComponent, 
    RolPipe, 
    RemoveComponent, 
    EditComponent, 
    LoadComponent, 
    EmptyStateComponent,
    HeaderComponent,
    ConstructionComponent,
    SafePipe,
    CategoryPipe
  ],
  imports: [
    CommonModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    FormsModule
  ],
  exports: [
    DataToolComponent,
    RolPipe,
    LoadComponent,
    EmptyStateComponent,
    ConstructionComponent,
    SafePipe,
    CategoryPipe
  ]
})
export class SharedModule { }
