import { Pipe, PipeTransform } from '@angular/core';
import { Blog } from '@core/model/caminnos/blog.model';

@Pipe({
  name: 'category'
})
export class CategoryPipe implements PipeTransform {

  transform(blogs: Blog[], category: string): Blog[] {
    category = category.toLowerCase();
    if (category === '0') {
      return blogs
    } else {
      return blogs.filter( blog => {
        if (blog.category) {
          return blog.category.toLowerCase().includes(
            category
          );
        } 
      });
    } 
  }

}
