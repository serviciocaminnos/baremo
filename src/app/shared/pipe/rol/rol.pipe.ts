import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'rol'
})
export class RolPipe implements PipeTransform {

  transform(value: number): string {
    let rolUser = '';
    switch (value) {
      case 0:
        rolUser = 'Consultor';
        break;
        case 1:
          rolUser = 'Administrador';
          break;
          case 2:
            rolUser = 'Cliente Empresa';
            break;
            default:
              break;
    }
    return rolUser;
  }

}
