import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ToolComponent } from './tool.component';

const routes: Routes = [
  { path: '', component: ToolComponent, data: {animation: 'Tool'} },
  {
    path: 'content',
    loadChildren: () => import('./content/content.module').then((m) => m.ContentModule)
  },
  {
    path: 'user-permition',
    loadChildren: () => import('./content/permition/permition.module').then( m => m.PermitionModule )
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ToolRoutingModule {}
