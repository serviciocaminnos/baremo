import { Component, OnInit } from '@angular/core';
import { AuthConstants } from 'src/app/core/config/auth-constanst';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { AddToolComponent } from '../add-tool/add-tool.component';
import { ToolService } from 'src/app/core/services/baremo/tool.service';
import { StorageService } from 'src/app/core/services/baremo/storage.service';
import { SendDataService } from 'src/app/core/services/baremo/SendData.service';

@Component({
  selector: 'app-tool',
  templateUrl: './tool.component.html',
  styleUrls: ['./tool.component.css'],
})
export class ToolComponent implements OnInit {
  token;
  rol;
  idUser;

  hiddenProgressListTool = true;
  progressStatus = false;
  tools: any = [];
  rolHidden = false;
  idBlock: any;
  // tslint:disable-next-line: ban-types
  imageEmpty: Boolean;
  constructor(
    private toolService: ToolService,
    private storage: StorageService,
    private router: Router,
    public dialog: MatDialog
  ) {
    this.storage.get(AuthConstants.AUTH).then((token) => {
      this.token = token;
    });
    this.storage.get(AuthConstants.ROL).then((rol) => {
      this.rol = rol;
    });
  }

  ngOnInit(): void {
    this.listTool();
    // tslint:disable-next-line: triple-equals
    if (this.rol == 1) {
      this.rolHidden = true;
    }
  }
  openDialog(idTool, param): any {
    // tslint:disable-next-line: triple-equals
    if (param == 'add') {
      const dialogRef = this.dialog.open(AddToolComponent, {
        width: '60%',
        data: {
          option: param
        }
      });
    }else {
      const dialogRef = this.dialog.open(AddToolComponent, {
        width: '80%',
        data: {
          id: idTool,
          option: param
        }
      });
    }
  }
  listTool(): any {
    const petition = new SendDataService();
    petition.createServiceName('tool');
    petition.addAuthorization(this.token);
    this.toolService.readData(petition).subscribe(
      (res) => {
        if (res[0] != null){
          this.imageEmpty = false;
          this.hiddenProgressListTool = false;
          this.tools = res;
          // console.log(this.tools);
        }else {
          this.imageEmpty = true;
          this.hiddenProgressListTool = false;
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }
  // tslint:disable-next-line: typedef
  saveIdTool(idTool: string): any {
    this.storage.store(AuthConstants.ID_TOOL, idTool);
    this.storage.store(AuthConstants.NAV_INDEX, '0');
    const requestServer = new SendDataService();
    requestServer.addAuthorization(this.token);
    requestServer.createServiceName('getBlockOfTool');
    requestServer.addParams(idTool);
    this.toolService.readData(requestServer).subscribe(
      res => {
        this.idBlock = res[0];
        this.storage.store(AuthConstants.ID_BOCK, this.idBlock.id);
        this.storage.store(AuthConstants.BLOCK_POSITION, '1');
        this.storage.store(AuthConstants.MEASURE_POSITION, '1');
        switch (this.rol) {
          case 0:
            this.storage.store(AuthConstants.NAV_INDEX, '0');
            this.router.navigate(['/content/evaluation']).then(() => {
              window.location.reload();
            });
            break;
            case 1:
              this.storage.store(AuthConstants.NAV_INDEX, '0');
              this.router.navigate(['/content/organization']).then(() => {
                window.location.reload();
              });
              break;
              case 2:
                break;
        }
      }, error => {
        console.log(error);
      }
      );
    }
    // tslint:disable-next-line: typedef
    goToEvaluation(idTool) {
      this.storage.store(AuthConstants.ID_TOOL, idTool);
      this.storage.store(AuthConstants.NAV_INDEX, '1');
      const requestServer = new SendDataService();
      requestServer.addAuthorization(this.token);
      requestServer.createServiceName('getBlockOfTool');
      requestServer.addParams(idTool);
      this.toolService.readData(requestServer).subscribe(
        (res) => {
          this.idBlock = res[0];
          console.log(this.idBlock.id);
          this.storage.store(AuthConstants.ID_BOCK, this.idBlock.id);
          this.storage.store(AuthConstants.BLOCK_POSITION, '1');
          this.storage.store(AuthConstants.MEASURE_POSITION, '1');
          this.router.navigate(['/content/evaluation']).then(() => {
            window.location.reload();
          });
        }, error => {
          console.log(error);
      }
    );
  }
}
