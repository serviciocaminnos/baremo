import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContentComponent } from './content.component';

const routes: Routes = [
  {
    path: '',
    component: ContentComponent,
    data: {animation: 'Content'},
    children: [
      {
        path: 'organization',
        data: { animation: 'Org' },
        loadChildren: () => import('./organization/organization.module').then( m => m.OrganizationModule )
      },
      {
        path: 'evaluation',
        data: { animation: 'Eva' },
        loadChildren: () => import('./evaluation/evaluation.module').then( m => m.EvaluationModule )
      },
      {
        path: '',
        redirectTo: 'organization',
        pathMatch: 'full'
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContentRoutingModule { }
