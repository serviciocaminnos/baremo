import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AuthConstants } from 'src/app/core/config/auth-constanst';
import { slider } from 'src/app/router-animation';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { EditComponent } from './organization/components/edit/edit.component';
import { OrganizationComponent } from './organization/organization.component';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css'],
  animations: [slider],
})
export class ContentComponent {
  // @ViewChild(OrganizationComponent) organization: OrganizationComponent;
  /*session variables */
//   links = [
//     {
//       label: 'ORGANIZACIÓN',
//       link: './organization',
//       index: 0,
//     },
//     {
//       label: 'EVALUACIÓN',
//       link: './evaluation',
//       index: 1,
//     },
//   ];
//   linksC = [
//     {
//       label: 'EVALUACIÓN',
//       link: './evaluation',
//       index: 0,
//     },
//   ];
//   activeLink;
//   linkIndex;

//   // list variables by path (evaluation,oranzation,permition)
//   hiddenlistOrganization: boolean;
//   hiddenlistEvaluation: boolean;
//   hiddenlistPermition: boolean;
//   // Progress bar para añadir nuevas cosas
//   hiddenProgressListBlock: boolean;
//   hiddenProgressToolContent = true;
//   // loacl storage variables
//   token;
//   idUser;
//   rol;
//   /*params variables */
//   varCss = '';
//   idTool;
//   idBlock;
//   /*tools variables */
//   blocks: Array<any> = [];
//   /*Vistas */
//   viewBlock: any = [];
//   viewMeasure: any = [];
//   viewquestion: any = [];
//   /* */
//   hiddenProgressListTool: boolean;
//   progressStatus = true;
//   panelOpenState = false;
//   titleTool: any;
//   blockForm: FormGroup;
//   measureForm: FormGroup;
//   questionForm: FormGroup;
//   rolhidden = true;
//   constructor(
//     private toolService: ToolService,
//     private storage: StorageService,
//     private router: Router,
//     private formbuilder: FormBuilder,
//     public dialog: MatDialog,
//     public dataBridge: DataBridgeService,
//     private snackBar: MatSnackBar,

//   ) {
//     this.storage.get(AuthConstants.NAV_INDEX).then(index => {
//       const i = + index;
//       this.activeLink = this.links[i];
//       this.linkIndex = i;
//     });
//     this.buildForm();
//     this.storage.get(AuthConstants.AUTH).then((token) => {
//       this.token = token;
//     });
//     this.storage.get(AuthConstants.USER_ID).then((idUser) => {
//       this.idUser = idUser;
//     });
//     this.storage.get(AuthConstants.ROL).then((rol) => {
//       this.rol = rol;
//     });
//     this.storage.get(AuthConstants.ID_TOOL).then((idTool) => {
//       this.idTool = idTool;
//     });
//   }
// }

  // ngOnInit(): void {
  //   // this.rutaActiva.params.subscribe((params: Params) => {
  //   //   this.idTool = params.idTool;
  //   // });
  //   try {
  //     if ( this.rol !== 0) {
  //       this.rolhidden = false;
  //     }
  //     // this.progressStatus = true;
  //     this.VistaHerramienta();
  //     this.verifyLink(this.linkIndex);
  //   } catch (error) {
  //     console.log(error);
  //     window.location.reload();
  //   }
  // }
  //   buildForm(): any{
  //     this.blockForm = this.formbuilder.group({
  //       tool_id: [''],
  //       name: ['Bloque'],
  //       porcentage: ['0'],
  //       description: ['Bloque de la herramienta'],
  //     });
  //     this.measureForm = this.formbuilder.group({
  //       block_id: [''],
  //       name: ['Medida'],
  //       porcentage: ['0'],
  //       description: ['Medida del bloque'],
  //     });
  //     this.questionForm = this.formbuilder.group({
  //       measure_id: [''],
  //       name: ['Pregunta'],
  //       description: ['Pregunta de la Medida'],
  //       porcentage: ['0'],
  //     });
  // }
  // // tslint:disable-next-line: typedef
  // getBlocks() {
  //   return new Promise((resolve, reject) => {
  //     const petition = new SendDataService();
  //     petition.createServiceName('getBlockOfTool');
  //     petition.addAuthorization(this.token);
  //     petition.addParams(this.idTool);
  //     this.toolService.readData(petition).subscribe(
  //       (res) => {
  //         let Resuelto = false;
  //         // if (res.length > 0) {
  //         for (const bloque of res) {
  //           const idBloque: number = bloque.id;
  //           this.getMedidas(idBloque).then((medida) => {
  //             let medi: any = [];
  //             medi = medida;
  //             //  if (medi.length > 0) {
  //             // for (const medicion of medi) {
  //             //   const idMedida: number = medicion.id;
  //             //   this.getQuestion(idMedida).then((preguntas) => {
  //             //     let question: any = [];
  //             //     question = preguntas;
  //             //     if (question.length > 0) {
  //             //       medicion.preguntas = question;
  //             //       resolve((Resuelto = true));
  //             //     }
  //             //   });
  //             // }
  //             resolve((Resuelto = true));
  //             bloque.Medidas = medi;
  //             // }
  //           });
  //         }
  //         const Bloques: any = res;

  //         this.blocks = Bloques;

  //         // }
  //       },
  //       (error) => {
  //         console.log(error);
  //       }
  //     );
  //   });
  // }
  // getMedidas(IdBloque: number): any {
  //   return new Promise((resolve) => {
  //     const petition = new SendDataService();
  //     petition.createServiceName('getMeasureOfBlock');
  //     petition.addAuthorization(this.token);
  //     petition.addParams(IdBloque);
  //     this.toolService.readData(petition).subscribe((res) => {
  //       resolve(res);
  //     });
  //   });
  // }
  // // getQuestion(IdMeasure: number): any {
  // //   return new Promise((resolve) => {
  // //     const petition = new SendDataService();
  // //     petition.createServiceName('getQuestionOfMeasure');
  // //     petition.addAuthorization(this.token);
  // //     petition.addParams(IdMeasure);
  // //     this.toolService.readData(petition).subscribe((res) => {
  // //       resolve(res);
  // //     });
  // //   });
  // // }
  // /*
  // siguiente(
  //   indecebloque: number,
  //   indecemedida: number,
  //   indecepregunta: number
  // ) {
  //   */
  // /*let addBlock: any = this.blocks[indecebloque];
  //   let addMeasure: any = this.blocks[indecebloque].Medidas[indecemedida];
  //   let addQuestion: any = this.blocks[indecebloque].Medidas[indecemedida].preguntas[indecepregunta];
  //   this.viewBlock = addBlock;
  //   this.viewMeasure = addMeasure;
  //   this.viewquestion = addQuestion;*/
  // /*console.log(this.blocks);
  // }
  // */
  // VistaHerramienta(): any {
  //   this.hiddenProgressListBlock = true;
  //   this.getInformationTool();
  //   this.getBlocks().then((resuelto) => {
  //     // tslint:disable-next-line: triple-equals
  //     if (resuelto == true) {
  //       /*falta validar*/
  //       // console.log(this.blocks);
  //       const addBlock: any = this.blocks[0];
  //       const addMeasure: any = this.blocks[0].Medidas[0];
  //       // const addQuestion: any = this.blocks[0].Medidas[0].preguntas[0];
  //       this.viewBlock = addBlock;
  //       this.viewMeasure = addMeasure;
  //       // this.viewquestion = addQuestion;
  //       this.progressStatus = false;
  //       this.hiddenProgressListBlock = false;
  //       this.hiddenProgressToolContent = false;
  //     }
  //   });
  // }

  // verifyLink(linkIndex): any {
  //   this.linkIndex = linkIndex;
  //   switch (this.linkIndex) {
  //     case 0:
  //       this.hiddenlistOrganization = true;
  //       this.varCss = 'organizacion';
  //       this.storage.store(AuthConstants.NAV_INDEX, '0');
  //       break;
  //     case 1:
  //       this.hiddenlistOrganization = false;
  //       this.varCss = 'evaluacion';

  //       break;
  //     case 2:
  //       this.hiddenlistOrganization = false;
  //       this.varCss = 'evaluacion';
  //       break;
  //   }
  // }

  // getInformationTool(): any {
  //   const serverRequest = new SendDataService();
  //   serverRequest.createServiceName('tool');
  //   serverRequest.addAuthorization(this.token);
  //   serverRequest.addParams(this.idTool);
  //   this.toolService.readData(serverRequest).subscribe(
  //     (res) => {
  //       this.titleTool = res.name;
  //     }, error => {{
  //       console.log(error);
  //       window.location.reload();
  //     }}
  //   );
  // }
  // goToListTool(): any {
  //   this.storage.remove(AuthConstants.BLOCK_POSITION);
  //   this.storage.remove(AuthConstants.MEASURE_POSITION);
  //   this.storage.remove(AuthConstants.ID_TOOL);
  //   this.storage.remove(AuthConstants.ID_BOCK);
  //   this.router.navigate(['/tool']).then(() => {
  //   });
  // }
  // addNewBlock(toolId): any{
  //   // console.log(toolId);
  //   // tslint:disable-next-line: no-string-literal
  //   this.blockForm.controls['tool_id'].setValue(this.idTool);
  //   this.hiddenProgressListBlock = true;
  //   console.log(this.blockForm.value);
  //   const blockRequest = new SendDataService();
  //   blockRequest.createServiceName('block');
  //   blockRequest.addAuthorization(this.token);
  //   blockRequest.addResponseType('json');
  //   blockRequest.addData(this.blockForm.value);
  //   this.toolService.createData(blockRequest).subscribe(
  //     (blockData) => {
  //       console.log(blockData);
  //       // tslint:disable-next-line: triple-equals
  //       if (blockData.status_code == '200') {
  //         // tslint:disable-next-line: no-string-literal
  //         this.measureForm.controls['block_id'].setValue(blockData.id);
  //         const measureRequest = new SendDataService();
  //         measureRequest.createServiceName('measure');
  //         measureRequest.addAuthorization(this.token);
  //         measureRequest.addResponseType('json');
  //         measureRequest.addData(this.measureForm.value);
  //         this.toolService.createData(measureRequest).subscribe(
  //           (measureData) => {
  //             // tslint:disable-next-line: triple-equals
  //             if (measureData.status_code == '200') {
  //               // tslint:disable-next-line: no-string-literal
  //               this.questionForm.controls['measure_id'].setValue(measureData.id);
  //               const questionRequest = new SendDataService();
  //               questionRequest.createServiceName('question');
  //               questionRequest.addAuthorization(this.token);
  //               questionRequest.addResponseType('json');
  //               questionRequest.addData(this.questionForm.value);
  //               this.toolService.createData(questionRequest).subscribe(
  //                 (questionData) => {
  //                   // tslint:disable-next-line: triple-equals
  //                   if (questionData.status_code == '200') {
  //                     console.log(questionData);
  //                     this.hiddenProgressListBlock = false;
  //                     this.VistaHerramienta();
  //                   }
  //                 },
  //                 (error) => {
  //                   console.log(error);
  //                 }
  //               );
  //             }
  //           },
  //           (error) => {
  //             console.log(error);
  //           }
  //         );
  //       }
  //     },
  //     (error) => {
  //       console.log(error);
  //     }
  //   );
  // }
  // addNewMeasure(idBlock): any{
  //   this.hiddenProgressListBlock = true;
  //   // tslint:disable-next-line: no-string-literal
  //   this.measureForm.controls['block_id'].setValue(idBlock);
  //   const measureRequest = new SendDataService();
  //   measureRequest.createServiceName('measure');
  //   measureRequest.addAuthorization(this.token);
  //   measureRequest.addResponseType('json');
  //   measureRequest.addData(this.measureForm.value);
  //   this.toolService.createData(measureRequest).subscribe(
  //     (measureData) => {
  //       // tslint:disable-next-line: triple-equals
  //       if (measureData.status_code == '200') {
  //         // tslint:disable-next-line: no-string-literal
  //         this.questionForm.controls['measure_id'].setValue(measureData.id);
  //         const questionRequest = new SendDataService();
  //         questionRequest.createServiceName('question');
  //         questionRequest.addAuthorization(this.token);
  //         questionRequest.addResponseType('json');
  //         questionRequest.addData(this.questionForm.value);
  //         this.toolService.createData(questionRequest).subscribe(
  //           (questionData) => {
  //             // tslint:disable-next-line: triple-equals
  //             if (questionData.status_code == '200') {
  //               // console.log(questionData);
  //               this.hiddenProgressListBlock = false;
  //               this.VistaHerramienta();
  //             }
  //           },
  //           (error) => {
  //             console.log(error);
  //           }
  //         );
  //       }
  //     },
  //     (error) => {
  //       console.log(error);
  //     }
  //   );
  // }
  // addNewQuestion(idMeasure): any {
  //   this.hiddenProgressListBlock = true;
  //   // tslint:disable-next-line: no-string-literal
  //   this.questionForm.controls['measure_id'].setValue(idMeasure);
  //   const questionRequest = new SendDataService();
  //   questionRequest.createServiceName('question');
  //   questionRequest.addAuthorization(this.token);
  //   questionRequest.addResponseType('json');
  //   questionRequest.addData(this.questionForm.value);
  //   this.toolService.createData(questionRequest).subscribe(
  //     (questionData) => {
  //       // tslint:disable-next-line: triple-equals
  //       if (questionData.status_code == '200') {
  //         // console.log(questionData);
  //         this.hiddenProgressListBlock = false;
  //         this.router.navigateByUrl('/tool/content/organization', { skipLocationChange: true }).then(() => {
  //           this.router.navigate(['content/organization']);
  //         });
  //       }
  //     },
  //     (error) => {
  //       console.log(error);
  //     }
  //   );
  // }
  // navigationBlock(position, idBlock): any{
  //   this.storage.store(AuthConstants.BLOCK_POSITION, position);
  //   this.storage.store(AuthConstants.MEASURE_POSITION, '1');
  //   this.storage.store(AuthConstants.ID_BOCK, idBlock);
  //   this.router.navigateByUrl('/tool/content/organization', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['content/organization']);
  //   });
  // }
  // navigationMeasure(position, idBlock, blockPosition): any {
  //   this.storage.store(AuthConstants.ID_BOCK, idBlock);
  //   this.storage.store(AuthConstants.MEASURE_POSITION, position);
  //   this.storage.store(AuthConstants.BLOCK_POSITION, blockPosition);
  //   this.router.navigateByUrl('/tool/content/organization', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['content/organization']);
  //   });
  // }
  // remove(opt, idOption): void {
  //   const dialogRef = this.dialog.open(EditComponent, {
  //     // padding: '0px',
  //     width: '40%',
  //     height: '50%',
  //     data: {
  //       id: idOption,
  //       option: 'remove',
  //       option2: opt
  //     }
  //   });
  //   dialogRef.afterClosed().subscribe(result => {
  //     this.VistaHerramienta();
  //   });
  // }
  // openSnackBar(): void {
  //   this.snackBar.open('Funcionalidad en Progreso', 'Done', {
  //     duration: 2000,
  //     horizontalPosition: 'center',
  //     verticalPosition: 'top',
  //   });
  // }
}
