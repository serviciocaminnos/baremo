import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthConstants } from 'src/app/core/config/auth-constanst';
import { BussinesData } from 'src/app/core/model/baremo/bussines.model';
import { SendDataService } from 'src/app/core/services/baremo/SendData.service';
import { StorageService } from 'src/app/core/services/baremo/storage.service';
import { ToolService } from 'src/app/core/services/baremo/tool.service';

@Component({
  selector: 'app-delete-bussines',
  templateUrl: './delete-bussines.component.html',
  styleUrls: ['./delete-bussines.component.css']
})
export class DeleteBussinesComponent implements OnInit {
  status: boolean;
  title = 'Eliminar';
  bussines: BussinesData;
  token: any;

  constructor(
    public dialogRef: MatDialogRef<DeleteBussinesComponent>,
    @Inject(MAT_DIALOG_DATA) public dataBussines: any,
    private httpService: ToolService,
    private storage: StorageService,
  ) {
    this.storage.get(AuthConstants.AUTH).then((token: any) => {
      this.token = token;
    });
  }

  ngOnInit(): void {
    this.getDataBussines();
  }
  getDataBussines(): void {
    // console.log(idUser);
    this.status = true;
    const petition = new SendDataService();
    petition.createServiceName('company');
    petition.addAuthorization(this.token);
    petition.addParams(this.dataBussines.id);
    this.httpService.readData(petition).subscribe(
      (res) => {
        // this.userForm.patchValue(res);
        this.status = false;
        this.bussines = res;
        // console.log(this.selectedRol);
        // console.log(this.userForm.value);
      },
      (error) => {
        console.log(error);
      }
    );
  }
  destroyBussines(): void  {
    const petition = new SendDataService();
    petition.createServiceName('company');
    petition.addAuthorization(this.token);
    petition.addParams(this.dataBussines.id);
    this.httpService.deleteData(petition).subscribe(
      res => {
        if (res.status_code === 200) {
          this.dialogRef.close();
          // this.router.navigate(['tool/user-permition']);
        }
      },
      error => {
        console.log(error);
      }
    );
  }
}
