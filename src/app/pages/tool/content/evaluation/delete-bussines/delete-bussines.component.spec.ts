import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteBussinesComponent } from './delete-bussines.component';

describe('DeleteBussinesComponent', () => {
  let component: DeleteBussinesComponent;
  let fixture: ComponentFixture<DeleteBussinesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteBussinesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteBussinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
