import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faPlus, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { ListQuestionsService } from '../list-questions.service';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-form-representacion-legal',
  templateUrl: './form-representacion-legal.component.html',
  styleUrls: ['./form-representacion-legal.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class FormRepresentacionLegalComponent implements OnInit {
  //icons
  faPlus = faPlus;
  faTrashAlt =faTrashAlt;
  //forms
  selected:any= '';
  //data table
  dataSource = ELEMENT_DATA;
  columnsToDisplay = ['Nombre', 'Documento Identidad', 'Denominanción del Cargo','delete'];
  expandedElement: PeriodicElement | null;  
  //data subtable
  displayedColumns: string[] = ['tipo', 'cuantia', 'componente','mecanismo', 'actions'];
  dataSubtable = ELEMENT_SUBTABLE;
  constructor(private listQuestionsService: ListQuestionsService,  private router: Router,private _Activatedroute:ActivatedRoute) { }

  ngOnInit(): void {
  }
  siguiente(e) {
    this.listQuestionsService.data(3);
    this.router.navigate(['../Participacion'],{ relativeTo: this._Activatedroute });
    e.stopPropagation();
  }
  anterior(e) {
    this.listQuestionsService.data(1);
    this.router.navigate(['../Socios'],{ relativeTo: this._Activatedroute });
    e.stopPropagation();
  }
  
}
export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
  description: string;
}

const ELEMENT_DATA= [
  {
    "Nombre":'juan perez' ,
    "Documento Identidad": 156262626,
    "Denominanción del Cargo": 'abogado',
    "fecha": '15/10/2020',
    "delete": 'eliminar',
   
  }, {
    "Nombre":'Rafael Montes' ,
    "Documento Identidad": 158122116,
    "Denominanción del Cargo": 'abogado',
    "delete": 'eliminar',
  }];

  const ELEMENT_SUBTABLE= [
    {tipo: 'Venta Activos', cuantia: 100, componente: 'gerente general',mecanismo:6266},
    {tipo: 'Compra Archivos', cuantia: 100, componente: 'gerente general',mecanismo:6266},
  ];