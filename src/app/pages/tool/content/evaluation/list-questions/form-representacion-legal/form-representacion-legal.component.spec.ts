import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormRepresentacionLegalComponent } from './form-representacion-legal.component';

describe('FormRepresentacionLegalComponent', () => {
  let component: FormRepresentacionLegalComponent;
  let fixture: ComponentFixture<FormRepresentacionLegalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormRepresentacionLegalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormRepresentacionLegalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
