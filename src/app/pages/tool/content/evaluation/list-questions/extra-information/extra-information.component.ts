import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AuthConstants } from 'src/app/core/config/auth-constanst';
import { MatDialog } from '@angular/material/dialog';
import { CreateDataComponent } from '../create-data/create-data.component';
import { ExtraDataBussines } from 'src/app/core/model/baremo/extra-data.model';
import { DataBridgeService } from 'src/app/core/services/baremo/data-bridge.service';
import { StorageService } from 'src/app/core/services/baremo/storage.service';
import { ToolService } from 'src/app/core/services/baremo/tool.service';
import { SendDataService } from 'src/app/core/services/baremo/SendData.service';

export interface DialogDataBussines {
  idBussines: string;
  idExtraData: string;
  crud: string;
}

@Component({
  selector: 'app-extra-information',
  templateUrl: './extra-information.component.html',
  styleUrls: ['./extra-information.component.css']
})
export class ExtraInformationComponent implements AfterViewInit, OnInit {
  idBussines = 0;
  token: any;
  displayedColumns: string[] = ['name', 'data', 'url', 'action'];
  dataSource: ExtraDataBussines[] = [];
  showEmpetyList: boolean;
  status: boolean;
  constructor(
    private dataBridgeService: DataBridgeService,
    private storageService: StorageService,
    private toolService: ToolService,
    private dialog: MatDialog,
  ) {
    this.storageService.get(AuthConstants.AUTH).then((token: any) => {
      this.token = token;
    });
    this.storageService.get(AuthConstants.ID_COMPANY).then((idBussines: any) => {
      this.idBussines = idBussines;
    });
   }

  ngOnInit(): void {
  }
  ngAfterViewInit(): void {
    this.dataBridgeService.$getObjectSource.subscribe((data: any) => {
      this.idBussines = data.id;
      this.getExtraInfromation();
    });
  }
  getExtraInfromation(): void {
    // cons]ole.log('Hacer Algo');
    this.status = true;
    const petition = new SendDataService();
    petition.createServiceName('getExtraDataContact');
    petition.addAuthorization(this.token);
    petition.addParams(this.idBussines);
    this.toolService.readData(petition).subscribe((res) => {
      // console.log(res);
      if (res.status_code === 400 ) {
        this.showEmpetyList = true;
      } else {
        this.dataSource = res;
        this.showEmpetyList = false;
      }
      this.status = false;
    }, error => {
      console.log(error);
    });
  }
  dialogExtraData(idExtra, option): void {
    const dialogRef = this.dialog.open(CreateDataComponent, {
      width: '50%',
      data: {
        idBussines: this.idBussines,
        idExtraData: idExtra,
        crud: option,
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getExtraInfromation();
    });
  }
}
