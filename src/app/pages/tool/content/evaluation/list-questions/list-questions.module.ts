import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListQuestionsRoutingModule } from './list-questions-routing.module';
import { FormContactComponent } from './form-contact/form-contact.component';
import { FormNominaSociosComponent } from './form-nomina-socios/form-nomina-socios.component';
import { FormCapitalSocialComponent } from './form-capital-social/form-capital-social.component';
import { ParticipacionComponent } from './participacion/participacion.component';
import { FormRegimenCoorporativoComponent } from './form-regimen-coorporativo/form-regimen-coorporativo.component';
import { FormRepresentacionLegalComponent } from './form-representacion-legal/form-representacion-legal.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { ChartsModule } from 'ng2-charts';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TransferenciaCuotasComponent } from './transferencia-cuotas/transferencia-cuotas.component';
import { AngularMaterialModule } from 'src/app/shared/material-angular/angular-material.module';
import { ListQuestionsComponent } from './list-questions.component';
import { HeaderEvaluationComponent } from '../header-evaluation/header-evaluation.component';
import { ExtraInformationComponent } from './extra-information/extra-information.component';
import { CreateDataComponent } from './create-data/create-data.component';

@NgModule({
  declarations: [
    FormContactComponent,
    FormNominaSociosComponent,
    FormCapitalSocialComponent,
    ParticipacionComponent,
    FormRegimenCoorporativoComponent,
    FormRepresentacionLegalComponent,
    TransferenciaCuotasComponent,
    ListQuestionsComponent,
    HeaderEvaluationComponent,
    ExtraInformationComponent,
    CreateDataComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ListQuestionsRoutingModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    ChartsModule,
    AngularMaterialModule
  ]
})
export class ListQuestionsModule { }
