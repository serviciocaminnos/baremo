import { TestBed } from '@angular/core/testing';

import { ListQuestionsService } from './list-questions.service';

describe('ListQuestionsService', () => {
  let service: ListQuestionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ListQuestionsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
