import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faPlus, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { ListQuestionsService } from '../list-questions.service';

@Component({
  selector: 'app-transferencia-cuotas',
  templateUrl: './transferencia-cuotas.component.html',
  styleUrls: ['./transferencia-cuotas.component.css']
})
export class TransferenciaCuotasComponent implements OnInit {
  //icons
  faPlus=faPlus;
  faTrashAlt=faTrashAlt;
//table I
  tableColumnsI: string[] = ['oferta', 'derecho_preferente', 'plazo'];
  dataTableI =DATA_TABLE_I;
  //table II
  tableColumnsII: string[] = ['oferta', 'data_autorized'];
  dataTableII =DATA_TABLE_II;
  //table III
  tableColumnsIII: string[] = ['asamblea', 'plazo'];
  dataTableIII =DATA_TABLE_III;
  //table IV
  tableColumnsIV: string[] = ['comprobadores', 'opcion_1', 'opcion_2','actions'];
  dataTableIV =DATA_TABLE_IV;
  //table IV
  tableColumnsResolution: string[] = ['instancia', 'metodo', 'reglamento','tipo','sede','actions'];
  dataTableResolution =DATA_TABLE_DISPUTE_RESOLUTION;
  constructor(private listQuestionsService: ListQuestionsService,  private router: Router,private _Activatedroute:ActivatedRoute) { }

  ngOnInit(): void {
  }
  siguiente(e) {
    this.listQuestionsService.data(3);
    this.router.navigate(['../Participacion'],{ relativeTo: this._Activatedroute });
    e.stopPropagation();
  }
  anterior(e) {
    this.listQuestionsService.data(1);
    this.router.navigate(['../Socios'],{ relativeTo: this._Activatedroute });
    e.stopPropagation();
  }
}
export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const DATA_TABLE_I= [
  {oferta: "Medio escrito a Socios Input", derecho_preferente: 'Socios', plazo: 10},
  {oferta:'' , derecho_preferente: '', plazo: ''},
  
];
const DATA_TABLE_II= [
  {oferta: "A terceros", derecho_preferente: 'Socios', plazo: 10},
  {oferta:'' , derecho_preferente: '', plazo: ''},
  
];
const DATA_TABLE_III= [
  {asamblea: "Sociedad debe presentar otro comprador", plazo: 15},
];
const DATA_TABLE_IV= [
  {comprobadores: "", opcion_1: 'Exclusión de Socio previo pago de valor de cuotas.', opcion_2: 'Disolución de Sociedad'},
  {comprobadores: "", opcion_1: 'Valor fijado en peritaje', opcion_2: 'Liquidador nombrado por'},
  {comprobadores: "", opcion_1: 'Períto nombrado por', opcion_2: 'Asamblea Extraordinaria'},
  
];
const DATA_TABLE_DISPUTE_RESOLUTION= [
  {
    instancia: 1, 
    metodo: 'Arbitraje', 
    reglamento: 'Centro de Arbitraje CADECO',
    tipo:'institucional',
    sede:'cochabamba'
}, {
  instancia: 2, 
  metodo: 'Arbitraje', 
  reglamento: 'Centro de Arbitraje CADECO',
  tipo:'institucional',
  sede:'cochabamba'
}
];

