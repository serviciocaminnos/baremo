import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransferenciaCuotasComponent } from './transferencia-cuotas.component';

describe('TransferenciaCuotasComponent', () => {
  let component: TransferenciaCuotasComponent;
  let fixture: ComponentFixture<TransferenciaCuotasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransferenciaCuotasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferenciaCuotasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
