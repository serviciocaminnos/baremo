import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { ListQuestionsService } from '../list-questions.service';

@Component({
  selector: 'app-form-regimen-coorporativo',
  templateUrl: './form-regimen-coorporativo.component.html',
  styleUrls: ['./form-regimen-coorporativo.component.css']
})
export class FormRegimenCoorporativoComponent implements OnInit {
   //icons
   faPlus = faPlus;
//table ordinaria 1
  displayedColumns: string[] = ['organo', 'asamblea', 'ordinaria', 'extraordinaria'];
  dataOrdinaria1 = ELEMENT_ORDINARIA_1;
  //table ordinaria 2
  ColumnsdataOrdinaria2: string[] = ['organo', 'ordinaria', 'extraordinaria'];
  dataOrdinaria2 = ELEMENT_ORDINARIA_2;
  //table ordinaria 3
  ColumnsdataOrdinaria3: string[] = ['convocatoria_asamblea', 'medio', 'plazo_anticipacion'];
  dataOrdinaria3 = ELEMENT_ORDINARIA_3;
   //table ordinaria 3
   ColumnsdataOrdinaria4: string[] = ['asamblea', 'ordinaria', 'extraordinaria'];
   dataOrdinaria4 = ELEMENT_ORDINARIA_4;
  constructor(private listQuestionsService: ListQuestionsService,  private router: Router,private _Activatedroute:ActivatedRoute) { }

  ngOnInit(): void {
  }
  siguiente(e) {
    this.listQuestionsService.data(3);
    this.router.navigate(['../Participacion'],{ relativeTo: this._Activatedroute });
    e.stopPropagation();
  }
  anterior(e) {
    this.listQuestionsService.data(1);
    this.router.navigate(['../Socios'],{ relativeTo: this._Activatedroute });
    e.stopPropagation();
  }
}
  const ELEMENT_ORDINARIA_1= [
  {organo: 'Organo', asamblea: 'Asamblea de Socios', ordinaria: 50,extraordinaria:60},
  ];
  const ELEMENT_ORDINARIA_2= [
    {organo: '#Minimo Asistentes (Quorum)', ordinaria: 50,extraordinaria:60},
    {organo: '# VOTOS PARA RESOLUCION (En función al quorum)', ordinaria: 50,extraordinaria:60}, 
  ];
  const ELEMENT_ORDINARIA_3= [
    {convocatoria_asamblea: '#Minimo Asistentes (Quorum)', medio: 50,plazo_anticipacion:60},
   
  ];
  const ELEMENT_ORDINARIA_4= [
    {
      asamblea: 1, 
      ordinaria: 'Aprobar la gestión del Representante Legal (Gerente)',
      extraordinaria:'Modificar la escritura social'
    },
    {
      asamblea: 2, 
      ordinaria: 'Aprobar la gestión del Representante Legal (Gerente)',
      extraordinaria:'Modificar la escritura social'
    },
    {
      asamblea: 3, 
      ordinaria: 'Aprobar la gestión del Representante Legal (Gerente)',
      extraordinaria:'Modificar la escritura social'
    },
    {
      asamblea: 4, 
      ordinaria: 'Aprobar la gestión del Representante Legal (Gerente)',
      extraordinaria:'Modificar la escritura social'
    },
    
    {
      asamblea: 5, 
      ordinaria: 'Aprobar la gestión del Representante Legal (Gerente)',
      extraordinaria:'Modificar la escritura social'
    },
  ];