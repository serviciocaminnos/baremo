import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormRegimenCoorporativoComponent } from './form-regimen-coorporativo.component';

describe('FormRegimenCoorporativoComponent', () => {
  let component: FormRegimenCoorporativoComponent;
  let fixture: ComponentFixture<FormRegimenCoorporativoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormRegimenCoorporativoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormRegimenCoorporativoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
