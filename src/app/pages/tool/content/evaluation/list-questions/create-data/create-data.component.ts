import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogDataBussines } from '../extra-information/extra-information.component';
import { FormBuilder } from '@angular/forms';
import { AuthConstants } from 'src/app/core/config/auth-constanst';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ToolService } from 'src/app/core/services/baremo/tool.service';
import { StorageService } from 'src/app/core/services/baremo/storage.service';
import { SendDataService } from 'src/app/core/services/baremo/SendData.service';
interface Type {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-create-data',
  templateUrl: './create-data.component.html',
  styleUrls: ['./create-data.component.css']
})
export class CreateDataComponent implements OnInit {
  token: any;
  showOptionEdit: boolean;
  showOptionDelete: boolean;
  showOptionCreate: boolean;
  extraDataForm: any;
  uploadPercent: any;
  statusUpload: boolean;
  dataSrc: any;
  typeData: string;

  types: Type[] = [
    {value: 'image/x-png,image/gif,image/jpeg,image/png', viewValue: 'Imagenes (jpg, png, jpeg)'},
    {value: 'application/msexcel,application/msword,application/pdf,application/rtf', viewValue: 'Archivos de texto (word, excel, pdf)'},
  ];
  constructor(
    public dialogRef: MatDialogRef<CreateDataComponent>,
    @Inject(MAT_DIALOG_DATA) public extraData: DialogDataBussines,
    private httpService: ToolService,
    private storageService: StorageService,
    private formBuilder: FormBuilder,
    private storage: AngularFireStorage,
    private snackBar: MatSnackBar,
    private toolService: ToolService,
  ) {
    this.storageService.get(AuthConstants.AUTH).then( token => {
      this.token = token;
    });
    this.buildForm();
   }

  ngOnInit(): void {
    switch (this.extraData.crud) {
      case 'edit':
        this.showOptionEdit = true;
        this.showOptionDelete = false;
        this.showOptionCreate = false;
        this.editUser(this.extraData.idExtraData);
        break;
        case 'delete':
          this.showOptionEdit = false;
          this.showOptionDelete = true;
          this.showOptionCreate = false;
          // this.showOptionCrud = true;
          this.deleteUser(this.extraData.idExtraData);
          break;
          case 'create':
            this.showOptionEdit = false;
            this.showOptionDelete = false;
            this.showOptionCreate = true;
            // this.showOptionCrud = true;
            break;
    }
  }
  buildForm(): void{
    this.extraDataForm = this.formBuilder.group({
      contact_id: [''],
      data_name: [''],
      data: [''],
      type: [''],
      url	: [''],
    });
  }
  editUser(idExtraData: string): void {
    throw new Error('Method not implemented.');
  }
  deleteUser(idExtraData: any): void {
    throw new Error('Method not implemented.');
  }
  saveExtraData(): void {
    this.extraDataForm.controls.contact_id.setValue(this.extraData.idBussines);
    console.log(this.extraDataForm.value);
    const petition = new SendDataService();
    petition.createServiceName('extradataCompany');
    petition.addAuthorization(this.token);
    petition.addData(this.extraDataForm.value);
    this.toolService.createData(petition).subscribe((res) => {
      console.log(res);
      this.dialogRef.close();
    }, error => {
      console.log(error);
    });
  }
  upload(event: any): void {
    this.statusUpload = true;
    const file = event.target.files[0];
    const dir = 'contact-documents/aditional/' + this.extraDataForm.controls.data_name.value + ' ' + this.extraDataForm.controls.data.value;
    const fileRef = this.storage.ref(dir);
    const task = this.storage.upload(dir, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges()
    .pipe(
      finalize(() => {
        this.statusUpload = false;
        this.dataSrc = fileRef.getDownloadURL();
        this.openSnackBar();
        this.dataSrc.subscribe(url => {
          this.extraDataForm.controls.url.setValue(url);
          console.log(this.extraDataForm.value);
        });
      })
    ).subscribe();
    // console.log(dir);
  }
  openSnackBar(): void {
    this.snackBar.open('Guardado Correctamente', 'Done', {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
    });
  }
  changeTypeData(value): void {
    this.typeData = value;
  }
}
