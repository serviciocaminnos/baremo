import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormCapitalSocialComponent } from './form-capital-social/form-capital-social.component';
import { FormContactComponent } from './form-contact/form-contact.component';
import { FormNominaSociosComponent } from './form-nomina-socios/form-nomina-socios.component';
import { FormRegimenCoorporativoComponent } from './form-regimen-coorporativo/form-regimen-coorporativo.component';
import { FormRepresentacionLegalComponent } from './form-representacion-legal/form-representacion-legal.component';
import { ListQuestionsComponent } from './list-questions.component';
import { ParticipacionComponent } from './participacion/participacion.component';
import { TransferenciaCuotasComponent } from './transferencia-cuotas/transferencia-cuotas.component';

const routes: Routes = [
  {
    path: '' , component: ListQuestionsComponent,
    // children: [
    //   { path: '', pathMatch: 'full', redirectTo: 'Contact' },
    //   {
    //     path: 'Contact',
    //     component: FormContactComponent
    //   },
    //   {
    //     path: 'Socios',
    //     component: FormNominaSociosComponent
    //   },
    //   {
    //     path: 'Capital_Social',
    //     component: FormCapitalSocialComponent
    //   },
    //   {
    //     path: 'Participacion',
    //     component: ParticipacionComponent
    //   },
    //   {
    //     path: 'Regimen_Gob_Coorporativo',
    //     component: FormRegimenCoorporativoComponent
    //   },
    //   {
    //     path: 'Representacion_Legal',
    //     component: FormRepresentacionLegalComponent
    //   },
    //   {
    //     path: 'transferencia_cuota',
    //     component: TransferenciaCuotasComponent
    //   },
    // ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListQuestionsRoutingModule { }
