import { Component, OnInit, AfterViewInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { AuthConstants } from 'src/app/core/config/auth-constanst';
import { MatSnackBar } from '@angular/material/snack-bar';
import { StorageService } from 'src/app/core/services/baremo/storage.service';
import { ToolService } from 'src/app/core/services/baremo/tool.service';
import { DataBridgeService } from 'src/app/core/services/baremo/data-bridge.service';
import { SendDataService } from 'src/app/core/services/baremo/SendData.service';

@Component({
  selector: 'app-form-contact',
  templateUrl: './form-contact.component.html',
  styleUrls: ['./form-contact.component.css']
})
export class FormContactComponent implements OnInit, AfterViewInit {

  /* font icons*/
  faPlus = faPlus;
  /* variables of inputs*/
 // options:FormGroup;
  hideRequiredControl = new FormControl(false);
  selected: any = '';
  option = '';
  /* var for add new elements*/
  elementsNews: any = [];
  token: any;
  date = new FormControl('');
  contactForm: FormGroup;
  idTool: any;
  idBussines: any;
  status: boolean;
  constructor(
    private formBuilder: FormBuilder,
    private storage: StorageService,
    private toolService: ToolService,
    private dataBridgeService: DataBridgeService,
    private snackBar: MatSnackBar,
    private dataBridge: DataBridgeService,
    )
  {
    this.buildForm();
    this.storage.get(AuthConstants.AUTH).then((token) => {
      this.token = token;
    });
    this.storage.get(AuthConstants.ID_TOOL).then((idTool) => {
      this.idTool = idTool;
    });

  }
  buildForm(): void {
    this.contactForm = this.formBuilder.group(
    {
      id_tool: [''],
      kind_of_society: ['', [Validators.required]],
      business_name: ['', [Validators.required]],
      trade_registration: ['', [Validators.required]],
      nit: [''],
      type_of_item: [''],
      date: [''],
      legal_address: [''],
      direction: [''],
      contact_person: ['', [Validators.required]],
      contact_number: ['', [Validators.required]],
      contact_email: ['', [Validators.required, Validators.email]],
    }
    );
  }
  onKey(event: any): any { // without type info
    const dataInput = event.target.value.replace(/-/g, '');
    if (dataInput.length % 3 === 0){
    // this.values = event.target.value + '-' ;
    }
    console.log(event.target.value);
  }
  ngOnInit(): void {

  }
  ngAfterViewInit(): void {
    this.dataBridge.$getObjectSource.subscribe((data: any) => {
      this.idBussines = data.id;
      this.option = data.crud;
      if (this.option === 'edit' ) {
        this.getInformationBussines(this.idBussines);
      }
    });
    this.storage.get(AuthConstants.ID_COMPANY).then((idBussines: any) => {
      this.idBussines = idBussines;
    });
  }
  getInformationBussines(idBussines: number): void {
    this.status = true;
    const petition = new SendDataService();
    petition.createServiceName('company');
    petition.addAuthorization(this.token);
    petition.addParams(idBussines);
    this.toolService.readData(petition).subscribe((res) => {
      // console.log(res);
      this.contactForm.patchValue(res);
      this.status = false;
    }, error => {
      console.log(error);
    });
  }
  saveContact(): void{
    this.contactForm.controls.id_tool.setValue(this.idTool);
    console.log(this.contactForm.value);
    const petition = new SendDataService();
    petition.createServiceName('company');
    petition.addAuthorization(this.token);
    petition.addData(this.contactForm.value);
    if (this.option === 'edit') {
      this.toolService.updateData(petition).subscribe((res) => {
        // console.log(res);
        this.openSnackBar();
      }, error => {
        console.log(error);
      });
    } else {
      this.toolService.createData(petition).subscribe((res) => {
        // console.log(res);
        this.openSnackBar();
        this.idBussines = res.idcompany;
        const dataCompany = {
          id: this.idBussines
        };
        this.dataBridgeService.sendObjectSource(dataCompany);
      }, error => {
        console.log(error);
      });
    }
  }
  openSnackBar(): void {
    this.snackBar.open('Guardado Correctamente', 'Done', {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
    });
  }
}
