import { Component, HostBinding, Input, OnInit, ViewChild } from '@angular/core';
import { MatStepper } from '@angular/material/stepper';
import { ActivatedRoute, Router } from '@angular/router';
import { ListQuestionsService } from './list-questions.service';

@Component({
  selector: 'app-list-questions',
  templateUrl: './list-questions.component.html',
  styleUrls: ['./list-questions.component.css']
})


export class ListQuestionsComponent implements OnInit {
  isEditable = true;
  state = true;
  selectedIndex = 0;
  @ViewChild('stepper') stepper;
  constructor(
    private dataComponentes: ListQuestionsService,
    private router: Router,
    private activatedRoute: ActivatedRoute) {

  }

  ngOnInit(): void{
    this.dataComponentes.getData.subscribe(res => {
        this.selectedIndex = res;
    });
  }
  redirection(): any{
    switch (this.stepper.selectedIndex) {
      case 0:
      this.router.navigate(['Contact'], { relativeTo: this.activatedRoute });
      break;
      case 1:
      this.router.navigate(['Socios'], { relativeTo: this.activatedRoute });
      break;
      case 2:
      this.router.navigate(['Capital_Social'], { relativeTo: this.activatedRoute });
      break;
      case 3:
      this.router.navigate(['Participacion'], { relativeTo: this.activatedRoute });
      break;
      case 4:
      this.router.navigate(['Regimen_Gob_Coorporativo'], { relativeTo: this.activatedRoute });
      break;
      case 5:
      this.router.navigate(['Representacion_Legal'], { relativeTo: this.activatedRoute });
      break;
      case 6:
      this.router.navigate(['transferencia_cuota'], { relativeTo: this.activatedRoute });
      break;
   }
  }
}
