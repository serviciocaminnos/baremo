import { Injectable, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ListQuestionsService {

  constructor() { }

  @Output() getData: EventEmitter<number> = new EventEmitter();

  data(msj: number): any {
    this.getData.emit(msj);
  }
}
