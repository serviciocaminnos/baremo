import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faPlus, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { ListQuestionsService } from '../list-questions.service';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { ChartOptions, ChartType } from 'chart.js';

import {
  SingleDataSet,
  Label,
  monkeyPatchChartJsLegend,
  monkeyPatchChartJsTooltip,
  ChartsModule,
} from 'ng2-charts';
import * as Chart from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
@Component({
  selector: 'app-participacion',
  templateUrl: './participacion.component.html',
  styleUrls: ['./participacion.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class ParticipacionComponent implements OnInit {
  //graphics Pie
  width:number=400;
  heigth:number=300;
  public pieChartOptions: ChartOptions = {
    responsive: true,
  };
  public pieChartLabels: Label[] = [
    ['Download', 'Sales'],
    ['In', 'Store', 'Sales'],
    'Mail Sales',
  ];
  public pieChartData: SingleDataSet = [300, 500, 100];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];

  //graphics bar
  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public barChartLabels: Label[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];

  public barChartData: Chart.ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
  ];
//icons
faPlus = faPlus;
faTrashAlt =faTrashAlt;
//forms
selected:any= '';
//data table
dataSource = ELEMENT_DATA;
columnsToDisplay = [
  'Capital Social',
  'Bolivianos (Bs)',
  'Dolares ($us)',
  'Valor p/cuota:',
  'Fecha',
];
expandedElement: PeriodicElement | null;
//data subtable
displayedColumns: string[] = [
  'nombre',
  'participacion',
  'aporte',
  'cuotas_capital',
];
dataSubtable = ELEMENT_SUBTABLE;
  constructor(private listQuestionsService: ListQuestionsService,  private router: Router,private _Activatedroute:ActivatedRoute) {
    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
  }

  ngOnInit(): void {}
  siguiente(e) {
    this.listQuestionsService.data(3);
    this.router.navigate(['../Participacion'], {
      relativeTo: this._Activatedroute,
    });
    e.stopPropagation();
  }
  anterior(e) {
    this.listQuestionsService.data(1);
    this.router.navigate(['../Socios'], { relativeTo: this._Activatedroute });
    e.stopPropagation();
  }
}
export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
  description: string;
}

const ELEMENT_DATA = [
  {
    'Capital Social': 'Cs1',
    'Dolares ($us)': 100,
    'Bolivianos (Bs)': 700,
    'Valor p/cuota:': 'abogado',
    Fecha: '15/10/2020',
  },
  {
    'Capital Social': 'Cs2',
    'Dolares ($us)': 100,
    'Bolivianos (Bs)': 700,
    'Valor p/cuota:': 'abogado',
    Fecha: '15/10/2020',
  },
];

const ELEMENT_SUBTABLE = [
  {
    nombre: 'jose perez',
    participacion: 100,
    aporte: 500,
    cuotas_capital: 6266,
  },
  {
    nombre: 'Alberto Archivos',
    participacion: 100,
    aporte: 500,
    cuotas_capital: 6266,
  },
];
