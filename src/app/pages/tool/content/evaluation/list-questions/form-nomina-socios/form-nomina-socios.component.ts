import { Component, OnInit } from '@angular/core';
import { faPlus, faSortDown } from '@fortawesome/free-solid-svg-icons';
import { faTrashAlt} from '@fortawesome/free-regular-svg-icons';
import { ListQuestionsService } from '../list-questions.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-form-nomina-socios',
  templateUrl: './form-nomina-socios.component.html',
  styleUrls: ['./form-nomina-socios.component.css']
})
export class FormNominaSociosComponent implements OnInit {
  /* fonts awasome */
  faPlus = faPlus;
  faSortDown=faSortDown;
  faTrashAlt = faTrashAlt;
  /* var acordions */
  panelOpenState = false;
  /* var inputs */
  selected:any= '';
   /* var for add new elements*/
   elementsNews:any=['nomina0'];
  constructor(
    private listQuestionsService: ListQuestionsService,  
    private router: Router,
    private _Activatedroute:ActivatedRoute) { }

  ngOnInit(): void {
  }
  addSocio(){
    this.elementsNews.push('nomina'+this.elementsNews.length);
  }
  delete(elemento:string){
    let indice=this.elementsNews.indexOf( elemento );
    this.elementsNews.splice( indice, 1 );
  }



  siguiente(e) {
    this.listQuestionsService.data(2);
    this.router.navigate(['../Capital_Social'],{ relativeTo: this._Activatedroute });
    e.stopPropagation();
  }
  anterior(e) {
    this.listQuestionsService.data(0);
    this.router.navigate(['../Contact'],{ relativeTo: this._Activatedroute });
    e.stopPropagation();
  }

}
