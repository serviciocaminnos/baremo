import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormNominaSociosComponent } from './form-nomina-socios.component';

describe('FormNominaSociosComponent', () => {
  let component: FormNominaSociosComponent;
  let fixture: ComponentFixture<FormNominaSociosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormNominaSociosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormNominaSociosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
