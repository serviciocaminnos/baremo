import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { ListQuestionsService } from '../list-questions.service';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-form-capital-social',
  templateUrl: './form-capital-social.component.html',
  styleUrls: ['./form-capital-social.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class FormCapitalSocialComponent implements OnInit {
  //icons
  faPlus = faPlus;
  faTrashAlt=faTrashAlt;
//data table
  dataSource = ELEMENT_DATA;
  columnsToDisplay = ['Tipo de Cambio', 'Bolivianos (Bs)', 'Dolares ($us)', 'fecha','delete'];
  expandedElement: PeriodicElement | null;  
  //data subtable
  displayedColumns: string[] = ['name', 'Bs', 'Sus', 'actions'];
  dataSubtable = ELEMENT_SUBTABLE;
  
  constructor(private listQuestionsService: ListQuestionsService,  private router: Router,private _Activatedroute:ActivatedRoute) { }

  ngOnInit(): void {
  }
  siguiente(e) {
    this.listQuestionsService.data(3);
    this.router.navigate(['../Participacion'],{ relativeTo: this._Activatedroute });
    e.stopPropagation();
  }
  anterior(e) {
    this.listQuestionsService.data(1);
    this.router.navigate(['../Socios'],{ relativeTo: this._Activatedroute });
    e.stopPropagation();
  }
  getTotalCost() {
    return this.dataSubtable.map(t => t.Bs).reduce((acc, value) => acc + value, 0);
  }
}
export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
  description: string;
}

const ELEMENT_DATA= [
  {
    "Tipo de Cambio":111 ,
    "Bolivianos (Bs)": 156,
    "Dolares ($us)": 1.0079,
    "fecha": '15/10/2020',
    "delete": 'eliminar',
    description: `Hydrogen is a chemical element with symbol H and atomic number 1. With a standard
        atomic weight of 1.008, hydrogen is the lightest element on the periodic table.`
  }, {
    "Tipo de Cambio":10 ,
    "Bolivianos (Bs)": 158,
    "Dolares ($us)": 1.0079,
    "fecha": '15/10/2020',
    description: `Hydrogen is a chemical element with symbol H and atomic number 1. With a standard
        atomic weight of 1.008, hydrogen is the lightest element on the periodic table.`
  }];

  const ELEMENT_SUBTABLE= [
    {name: 'Efectivo', Bs: 100, Sus: 700},
    {name: 'Moviliario', Bs: 100, Sus: 700},
  ];