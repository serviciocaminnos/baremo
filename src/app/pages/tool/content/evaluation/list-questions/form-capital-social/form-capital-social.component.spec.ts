import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormCapitalSocialComponent } from './form-capital-social.component';

describe('FormCapitalSocialComponent', () => {
  let component: FormCapitalSocialComponent;
  let fixture: ComponentFixture<FormCapitalSocialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormCapitalSocialComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormCapitalSocialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
