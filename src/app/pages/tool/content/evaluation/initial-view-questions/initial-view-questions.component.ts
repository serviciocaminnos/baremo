import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthConstants } from 'src/app/core/config/auth-constanst';
import { MatDialog } from '@angular/material/dialog';
import { DeleteBussinesComponent } from '../delete-bussines/delete-bussines.component';
import { StorageService } from 'src/app/core/services/baremo/storage.service';
import { ToolService } from 'src/app/core/services/baremo/tool.service';
import { DataBridgeService } from 'src/app/core/services/baremo/data-bridge.service';
import { SendDataService } from 'src/app/core/services/baremo/SendData.service';


@Component({
  selector: 'app-initial-view-questions',
  templateUrl: './initial-view-questions.component.html',
  styleUrls: ['./initial-view-questions.component.css']
})
export class InitialViewQuestionsComponent implements OnInit {
  displayedColumnsAux: string[] = ['name', 'state', 'registerDate', 'action'];
  dataSourceAux = [];
  emptyUser: boolean;
  token: any;
  status: boolean;
  constructor(
    private router: Router,
    private storage: StorageService,
    private toolService: ToolService,
    private dataBridge: DataBridgeService,
    public dialog: MatDialog
    ) {
      this.storage.get(AuthConstants.AUTH).then((token: any) => {
        this.token = token;
      });
    }

  ngOnInit(): void {
    this.getBussines();
  }
  getBussines(): void {
    this.status = true;
    const petition = new SendDataService();
    petition.createServiceName('company');
    petition.addAuthorization(this.token);
    this.toolService.readData(petition).subscribe((res) => {
      // console.log(res);
      if (res.status_code === 400 ) {
        this.emptyUser = true;
      } else {
        this.dataSourceAux = res;
        this.emptyUser = false;
        this.status = false;
      }
    }, error => {
      console.log(error);
    });
  }

  // tslint:disable-next-line: typedef
  async editCompany(idBussines: number) {
    const company = {
      id: idBussines,
      crud: 'edit'
    };
    await this.dataBridge.sendObjectSource(company);
    await this.storage.store(AuthConstants.ID_COMPANY, idBussines);
    await this.router.navigate(['content/evaluation/question']);
  }
  removeCompany(idBussines: number): void {
    const dialogRef = this.dialog.open(DeleteBussinesComponent, {
      width: '50%',
      data: {id: idBussines}
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getBussines();
    });
  }
  goToRegister(): void {
    this.router.navigate(['content/evaluation/question']);
  }
  goToResult(idCompany: number): void {
    console.log('El id de la emresa es: ' + idCompany);
    this.storage.store(AuthConstants.ID_COMPANY, idCompany);
    this.router.navigate(['content/evaluation/evaluation-list']);
  }
  goToStartEvaluation(): void {
    this.router.navigateByUrl('/content/evaluation/evaluation-list', { skipLocationChange: true }).then(() => {
      this.router.navigate(['content/evaluation/start']);
    });
  }
}
