import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InitialViewQuestionsComponent } from './initial-view-questions.component';

describe('InitialViewQuestionsComponent', () => {
  let component: InitialViewQuestionsComponent;
  let fixture: ComponentFixture<InitialViewQuestionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InitialViewQuestionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InitialViewQuestionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
