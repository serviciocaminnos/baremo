import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header-evaluation',
  templateUrl: './header-evaluation.component.html',
  styleUrls: ['./header-evaluation.component.css']
})
export class HeaderEvaluationComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  goToListComany(): void {
    this.router.navigate(['content/evaluation']);
  }
}
