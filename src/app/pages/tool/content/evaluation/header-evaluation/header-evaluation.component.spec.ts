import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderEvaluationComponent } from './header-evaluation.component';

describe('HeaderEvaluationComponent', () => {
  let component: HeaderEvaluationComponent;
  let fixture: ComponentFixture<HeaderEvaluationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderEvaluationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderEvaluationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
