import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EvaluationRoutingModule } from './evaluation-routing.module';
import { EvaluationComponent } from './evaluation.component';
import { ListQuestionsComponent } from './list-questions/list-questions.component';
import { InitialViewQuestionsComponent } from './initial-view-questions/initial-view-questions.component';
import { HeaderEvaluationComponent } from './header-evaluation/header-evaluation.component';
import { AngularMaterialModule } from 'src/app/shared/material-angular/angular-material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DeleteBussinesComponent } from './delete-bussines/delete-bussines.component';
@NgModule({
  declarations: [
    EvaluationComponent,
    InitialViewQuestionsComponent,
    DeleteBussinesComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    EvaluationRoutingModule,
    AngularMaterialModule
  ],
  exports: [
  ]
})
export class EvaluationModule {}
