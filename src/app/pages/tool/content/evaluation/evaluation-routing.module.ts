import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EvaluationComponent } from './evaluation.component';
import { InitialViewQuestionsComponent } from './initial-view-questions/initial-view-questions.component';

const routes: Routes = [
  {
    path: '',
    component: EvaluationComponent ,
    children: [
      {
        path: '',
        component: InitialViewQuestionsComponent,
        // tslint:disable-next-line: max-line-length
        // loadChildren: () => import('./initial-view-questions/initial-view-questions.component').then( m => m.InitialViewQuestionsComponent )
      },
      {
        path: 'question',
        // component: ListQuestionsComponent,
        loadChildren: () => import('./list-questions/list-questions.module').then( m => m.ListQuestionsModule )
      },
    ]
  },
  {
    path: 'start',
    // component: StartComponent,
    loadChildren: () => import('../organization/organization.module').then( m => m.OrganizationModule )
  },
  {
    path: 'evaluation-list',
    // component: StartComponent,
    loadChildren: () => import('../result/result.module').then( m => m.ResultModule )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EvaluationRoutingModule { }
