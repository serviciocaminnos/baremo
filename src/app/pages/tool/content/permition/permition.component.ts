import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { AuthConstants } from 'src/app/core/config/auth-constanst';
import { MatDialog } from '@angular/material/dialog';
import { EditComponent } from '../permition/edit/edit.component';
import { User } from 'src/app/core/model/baremo/user.model';
import { StorageService } from 'src/app/core/services/baremo/storage.service';
import { ToolService } from 'src/app/core/services/baremo/tool.service';
import { SendDataService } from 'src/app/core/services/baremo/SendData.service';

export interface DialogDataUser {
  idUser: string;
  option: string;
}
const USER_DATA: User[] = [ ];
@Component({
  selector: 'app-permition',
  templateUrl: './permition.component.html',
  styleUrls: ['./permition.component.css']
})
export class PermitionComponent implements OnInit {
  displayedColumnsAux: string[] = ['name', 'cargo', 'rol', 'lastLogin', 'action'];
  dataSourceAux: User[] = [];
  emptyUser: boolean;
  token: any;
  status: boolean;
  constructor(
    private router: Router,
    private storageService: StorageService,
    private httpService: ToolService,
    public dialog: MatDialog
    ) {
      this.storageService.get(AuthConstants.AUTH).then( token => {
        this.token = token;
      });
     }

  ngOnInit(): void {
    this.getAllUsers();
  }
  getAllUsers(): void {
    this.status = true;
    const userReq = new SendDataService();
    userReq.addAuthorization(this.token);
    userReq.createServiceName('user');
    this.httpService.readData(userReq).subscribe(
      res => {
        this.dataSourceAux = res;
        // console.log(this.dataSourceAux);
        if (this.dataSourceAux.length !== 0) {
          this.status = false;
          this.emptyUser = false;
        } else {
          this.emptyUser = true;
        }
      }, error => {
        console.log(error);
      }
    );
}

  addUssers(): void {
    this.router.navigate(['tool/user-permition/add-ussers']);
  }
  goToTool(): void {
    this.router.navigate(['tool']);
  }
  editUser(id: number, crud): void {
    const dialogRef = this.dialog.open(EditComponent, {
      width: '50%',
      data: {idUser: id, option: crud}
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getAllUsers();
    });
  }
  removeUser(id: number): void {
    console.log(id);
  }

}
