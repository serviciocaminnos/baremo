import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthConstants } from 'src/app/core/config/auth-constanst';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { StorageService } from 'src/app/core/services/baremo/storage.service';
import { ToolService } from 'src/app/core/services/baremo/tool.service';
import { SendDataService } from 'src/app/core/services/baremo/SendData.service';
export interface Rol {
  value: number;
  viewValue: string;
}

@Component({
  selector: 'app-add-ussers',
  templateUrl: './add-ussers.component.html',
  styleUrls: ['./add-ussers.component.css']
})
export class AddUssersComponent implements OnInit {

  // imageSrc: any;
  isLinear = true;
  firstFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  name: string;
  lastName: string;
  email: string;

  rols: Rol[] = [
    {value: 0, viewValue: 'Consultor'},
    {value: 1, viewValue: 'Administrador'},
    {value: 2, viewValue: 'Cliente Empresa'}
  ];
  token: any;
  idTool: any;
  statusUpload: boolean;
  imageSrc: Observable<any>;
  uploadPercent: Observable<number>;
  isAdmin: boolean;
  tools: any;
  emptyListTool: boolean;
  accesToolForm: FormGroup;
  idNewUser: number;
  stateAccess = false;
  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private storageService: StorageService,
              private httpService: ToolService,
              private snackBar: MatSnackBar,
              private storage: AngularFireStorage)
  {
    this.buildForm();
    this.storageService.get(AuthConstants.AUTH).then( token => {
      this.token = token;
    });
    this.storageService.get(AuthConstants.ID_TOOL).then( idTool => {
      this.idTool = idTool;
      // tslint:disable-next-line: no-string-literal
      // this.firstFormGroup.controls['company_id'].setValue(idTool);
    });
  }

  ngOnInit(): void {

  }
  buildForm(): void {
    this.firstFormGroup = this.formBuilder.group({
      name: ['', Validators.required],
      // company_id: ['', Validators.required],
      lastname: ['', Validators.required],
      rol: ['', Validators.required],
      position: ['', Validators.required],
      // phone: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required],
      email: ['', Validators.required],
      user_image: ['https://moonvillageassociation.org/wp-content/uploads/2018/06/default-profile-picture1.jpg']

    });
    this.thirdFormGroup = this.formBuilder.group({
     user_image: ['']
    });
    this.accesToolForm = this.formBuilder.group({
     idUser: ['', Validators.required],
     idTool: ['', Validators.required],
    });
  }
  goToBack(): void {
    this.router.navigate(['tool/user-permition']);
  }
  openSnackBar(): void {
    this.snackBar.open('Guardado Correctamente', 'Done', {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
    });
  }
  openSnackBarAccessPermition(): void {
    this.snackBar.open('Permisos Añadidos Correctamente', 'Done', {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }
  openSnackBarDelete(): void {
    this.snackBar.open('Permisos Eliminados Correctamente', 'Done', {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }
  // readURL(event: any): any {
  //   if (event.target.files && event.target.files[0]) {
  //       const file = event.target.files[0];
  //       const reader = new FileReader();
  //       reader.onload = e => this.imageSrc = reader.result;
  //       reader.readAsDataURL(file);
  //   }
  // }
  saveData(option: string): void {
    switch (option) {
      case 'first':
        console.log(this.firstFormGroup.value);
        // this.openSnackBar();
        this.listTool();
        break;
        case 'second':
          const saveUserRequest = new SendDataService();
          saveUserRequest.addAuthorization(this.token);
          saveUserRequest.createServiceName('register');
          saveUserRequest.addData(this.firstFormGroup.value);
          this.httpService.createData(saveUserRequest).subscribe(
            res => {
              console.log(res);
              if (res.status_code === 200) {
                this.openSnackBar();
                this.idNewUser = res.id_user;
                console.log(this.idNewUser);
              }
            }, error => {
              console.log(error);
            }
          );
          break;
          case 'third':
            // this.openSnackBar();
            console.log(this.idNewUser);
            const updateImg = new SendDataService();
            updateImg.addAuthorization(this.token);
            updateImg.createServiceName('user');
            updateImg.addData(this.thirdFormGroup.value);
            updateImg.addParams(this.idNewUser);
            console.log(this.thirdFormGroup.value);
            this.httpService.updateData(updateImg).subscribe(
              res => {
                if (res.status_code === 200) {
                  this.openSnackBar();
                  this.router.navigate(['tool/user-permition']);
                }
              }, error => {
                console.log(error);
              }
            );
            break;
    }
  }
  saveDataTemporaly(): void {
    this.name = this.firstFormGroup.controls.name.value;
    this.lastName = this.firstFormGroup.controls.lastname.value;
    this.email = this.firstFormGroup.controls.email.value;
  }
  upload(event: any): void {
    this.statusUpload = true;
    const file = event.target.files[0];
    const dir = 'image-profile/' + this.firstFormGroup.controls.name.value + ' ' + this.firstFormGroup.controls.lastname.value;
    const fileRef = this.storage.ref(dir);
    const task = this.storage.upload(dir, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges()
    .pipe(
      finalize(() => {
        this.statusUpload = false;
        this.imageSrc = fileRef.getDownloadURL();
        this.openSnackBar();
        this.imageSrc.subscribe(url => {
          this.thirdFormGroup.controls.user_image.setValue(url);
          console.log(this.thirdFormGroup.value);
        });
      })
    ).subscribe();
    // console.log(dir);
  }
  addUserToTool(idTool): void {
    // console.log(this.stateAccess);
    const accesPermitionForm: FormGroup = this.formBuilder.group({
      id_tool: [idTool],
      id_user: [this.idNewUser]
    });
    if (this.stateAccess === false) {
      console.log(accesPermitionForm.value);
      const petition = new SendDataService();
      petition.createServiceName('userPermits');
      petition.addAuthorization(this.token);
      petition.addData(accesPermitionForm.value);
      this.httpService.createData(petition).subscribe(
        (res) => {
          if (res.status_code === 200) {
            this.openSnackBarAccessPermition();
          }
        },
        (error) => {
          console.log(error);
        }
      );
    } else {
      this.openSnackBarDelete();
      console.log('Dar de baja a la herramienta');
    }
  }
  verifyRol(value): void {
    switch (value) {
      case 0:
        this.isAdmin = true;
        break;
        case 1:
          this.isAdmin = false;
          break;
          case 2:
            this.isAdmin = true;
            break;
    }
  }
  listTool(): any {
    const petition = new SendDataService();
    petition.createServiceName('tool');
    petition.addAuthorization(this.token);
    this.httpService.readData(petition).subscribe(
      (res) => {
        if (res[0] != null){
          this.tools = res;
          // console.log(this.tools);
          this.emptyListTool = false;
        }else {
          this.emptyListTool = true;
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
