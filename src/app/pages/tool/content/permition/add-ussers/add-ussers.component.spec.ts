import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUssersComponent } from './add-ussers.component';

describe('AddUssersComponent', () => {
  let component: AddUssersComponent;
  let fixture: ComponentFixture<AddUssersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddUssersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUssersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
