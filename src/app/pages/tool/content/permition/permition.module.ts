import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PermitionRoutingModule } from './permition-routing.module';
import { PermitionComponent } from './permition.component';
import { AddUssersComponent } from './add-ussers/add-ussers.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
// import { ComponentsModule } from 'src/app/components/components.module';
import { HomeComponent } from './home/home.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { EditComponent } from './edit/edit.component';
import { AngularMaterialModule } from 'src/app/shared/material-angular/angular-material.module';

@NgModule({
  declarations: [PermitionComponent, HomeComponent, AddUssersComponent, EditComponent],
  imports: [
    CommonModule,
    PermitionRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    // ComponentsModule,
    SharedModule,
    AngularMaterialModule
  ]
})
export class PermitionModule { }
