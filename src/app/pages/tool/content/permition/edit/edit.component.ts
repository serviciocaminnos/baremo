import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogDataUser } from '../../permition/permition.component';
import { AuthConstants } from 'src/app/core/config/auth-constanst';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Rol } from '../add-ussers/add-ussers.component';
import { Router } from '@angular/router';
import { ToolService } from 'src/app/core/services/baremo/tool.service';
import { StorageService } from 'src/app/core/services/baremo/storage.service';
import { SendDataService } from 'src/app/core/services/baremo/SendData.service';
import { User } from 'src/app/core/model/baremo/user.model';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  title: string;
  showOptionEdit: boolean;
  showOptionDelete: boolean;
  token: any;
  tools: any;
  emptyListTool: boolean;
  status: boolean;
  userForm: FormGroup;
  rols: Rol[] = [
    {value: 0, viewValue: 'Consultor'},
    {value: 1, viewValue: 'Administrador'},
    {value: 2, viewValue: 'Cliente Empresa'}
  ];
  user: User;
  constructor(
    public dialogRef: MatDialogRef<EditComponent>,
    @Inject(MAT_DIALOG_DATA) public dataUser: DialogDataUser,
    private httpService: ToolService,
    private storageService: StorageService,
    private formBuilder: FormBuilder,
    private router: Router,
  ) {
    this.storageService.get(AuthConstants.AUTH).then( token => {
      this.token = token;
    });
    this.buildForm();
  }

  ngOnInit(): void {
    switch (this.dataUser.option) {
      case 'edit':
        this.showOptionEdit = true;
        this.showOptionDelete = false;
        this.editUser(this.dataUser.idUser);
        break;
        case 'delete':
          this.showOptionEdit = false;
          this.showOptionDelete = true;
          // this.showOptionCrud = true;
          this.deleteUser(this.dataUser.idUser);
          break;
    }
  }
  buildForm(): void{
    this.userForm = this.formBuilder.group({
      name: [''],
      lastname: [''],
      rol: [],
      position: [''],
      username: [''],
      email: [''],
    });

  }
  deleteUser(idUser: string): void {
    // console.log(idUser);
    this.status = true;
    this.title = 'Eliminar';
    const petition = new SendDataService();
    petition.createServiceName('user');
    petition.addAuthorization(this.token);
    petition.addParams(idUser);
    this.httpService.readData(petition).subscribe(
      (res) => {
        // this.userForm.patchValue(res);
        this.status = false;
        this.user = res;
        // console.log(this.selectedRol);
        // console.log(this.userForm.value);
      },
      (error) => {
        console.log(error);
      }
    );
  }
  editUser(idUser: string): void {
    this.title = 'Editar';
    this.status = true;
    console.log(idUser);
    this.listTool();
    const petition = new SendDataService();
    petition.createServiceName('user');
    petition.addAuthorization(this.token);
    petition.addParams(idUser);
    this.httpService.readData(petition).subscribe(
      (res) => {
        this.userForm.patchValue(res);
        this.status = false;
        // console.log(this.selectedRol);
        // console.log(this.userForm.value);
      },
      (error) => {
        console.log(error);
      }
    );
  }
  addUserToTool(idTool): void {
    console.log(idTool);
  }
  listTool(): any {
    const petition = new SendDataService();
    petition.createServiceName('tool');
    petition.addAuthorization(this.token);
    this.httpService.readData(petition).subscribe(
      (res) => {
        if (res[0] != null){
          this.tools = res;
          // console.log(this.tools);
          this.emptyListTool = false;
        }else {
          this.emptyListTool = true;
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }
  saveChanges(): void {
    const user = this.userForm.value;
    const petition = new SendDataService();
    petition.createServiceName('user');
    petition.addAuthorization(this.token);
    petition.addParams(this.dataUser.idUser);
    petition.addData(user);
    this.httpService.updateData(petition).subscribe(
      res => {
        if (res.status_code === 200) {
          this.dialogRef.close();
          // this.router.navigate(['tool/user-permition']);
        }
      },
      error => {
        console.log(error);
      }
    );
  }
  destroyUser(): void  {
    const petition = new SendDataService();
    petition.createServiceName('user');
    petition.addAuthorization(this.token);
    petition.addParams(this.dataUser.idUser);
    this.httpService.deleteData(petition).subscribe(
      res => {
        if (res.status_code === 200) {
          this.dialogRef.close();
          // this.router.navigate(['tool/user-permition']);
        }
      },
      error => {
        console.log(error);
      }
    );
  }
}
