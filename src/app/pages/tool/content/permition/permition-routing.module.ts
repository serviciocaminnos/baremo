import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AddUssersComponent } from './add-ussers/add-ussers.component';
import { PermitionComponent } from './permition.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      { path: '', redirectTo: 'permition'},
      {
        path: 'permition',
        component: PermitionComponent,
      },
    ]
  },
  {path: 'add-ussers', component: AddUssersComponent},
  // {path: 'add-ussers', component: AddUssersComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PermitionRoutingModule { }
