import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResultRoutingModule } from './result-routing.module';
import { ResultComponent } from './result.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { EvaluationResultComponent } from './evaluation-result/evaluation-result.component';
import { ChartsModule } from 'ng2-charts';
import { BlockResultComponent } from './block-result/block-result.component';
import { AngularMaterialModule } from 'src/app/shared/material-angular/angular-material.module';
@NgModule({
  declarations: [ResultComponent, EvaluationResultComponent, BlockResultComponent],
  imports: [
    CommonModule,
    ResultRoutingModule,
    MatProgressBarModule,
    ChartsModule,
    AngularMaterialModule
  ],
  entryComponents: [
    EvaluationResultComponent
  ]
})
export class ResultModule { }
