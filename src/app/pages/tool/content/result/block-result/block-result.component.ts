import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { trigger, state, transition, style, animate } from '@angular/animations';

export interface Question {
  id: number;
  position: number;
  name: string;
  answer: string;
  subAnswer: string;
  necesity: string;
  commit: string;
}
export interface Measure {
  id: number;
  name: string;
  position: number;
  question: Question[];
}
const DATA: Measure[] = [
  {
    id: 1,
    position: 1,
    name: 'Una acción un voto',
    question: [
      {
        id: 1,
        name: '¿Ariel...?',
        position: 1,
        answer: 'Parcial',
        subAnswer: 'Nombre de la sub respuesta significativa',
        necesity: 'Imprecindible',
        commit: 'Comentario sobre la pregunta',
      },
      {
        id: 2,
        name: '¿Nombre de la pregunta...?',
        position: 2,
        answer: 'Parcial',
        subAnswer: 'Nombre de la sub respuesta significativa',
        necesity: 'Imprecindible',
                commit: 'Comentario sobre la pregunta',
      },
      {
        id: 3,
        name: '¿Nombre de la pregunta...?',
        position: 3,
        answer: 'Parcial',
        subAnswer: 'Nombre de la sub respuesta significativa',
        necesity: 'Imprecindible',
                commit: 'Comentario sobre la pregunta',
      },
      {
        id: 4,
        name: '¿Nombre de la pregunta...?',
        position: 4,
        answer: 'Parcial',
        subAnswer: 'Nombre de la sub respuesta significativa',
        necesity: 'Imprecindible',
                commit: 'Comentario sobre la pregunta',
      },
      {
        id: 5,
        name: '¿Nombre de la pregunta...?',
        position: 5,
        answer: 'Parcial',
        subAnswer: 'Nombre de la sub respuesta significativa',
        necesity: 'Imprecindible',
                commit: 'Comentario sobre la pregunta',
      },
    ]
  },
  {
    id: 2,
    position: 2,
    name: 'Elaboración y difusión suficiente a los accionistas sobre operaciones extraordinarias que puedan afectar sus intereses',
    question: [
      {
        id: 1,
        name: '¿Nombre de la pregunta...?',
        position: 1,
        answer: 'Parcial',
        subAnswer: 'Nombre de la sub respuesta significativa',
        necesity: 'Imprecindible',
                commit: 'Comentario sobre la pregunta',
      },
      {
        id: 2,
        name: '¿Nombre de la pregunta...?',
        position: 2,
        answer: 'Parcial',
        subAnswer: 'Nombre de la sub respuesta significativa',
        necesity: 'Imprecindible',
                commit: 'Comentario sobre la pregunta',
      },
      {
        id: 3,
        name: '¿Nombre de la pregunta...?',
        position: 3,
        answer: 'Parcial',
        subAnswer: 'Nombre de la sub respuesta significativa',
        necesity: 'Imprecindible',
                commit: 'Comentario sobre la pregunta',
      },
      {
        id: 4,
        name: '¿Nombre de la pregunta...?',
        position: 4,
        answer: 'Parcial',
        subAnswer: 'Nombre de la sub respuesta significativa',
        necesity: 'Imprecindible',
                commit: 'Comentario sobre la pregunta',
      },
      {
        id: 5,
        name: '¿Nombre de la pregunta...?',
        position: 5,
        answer: 'Parcial',
        subAnswer: 'Nombre de la sub respuesta significativa',
        necesity: 'Imprecindible',
                commit: 'Comentario sobre la pregunta',
      },
      {
        id: 6,
        name: '¿Nombre de la pregunta...?',
        position: 6,
        answer: 'Parcial',
        subAnswer: 'Nombre de la sub respuesta significativa',
        necesity: 'Imprecindible',
                commit: 'Comentario sobre la pregunta',
      },
      {
        id: 7,
        name: '¿Nombre de la pregunta...?',
        position: 7,
        answer: 'Parcial',
        subAnswer: 'Nombre de la sub respuesta significativa',
        necesity: 'Imprecindible',
                commit: 'Comentario sobre la pregunta',
      },
    ]
  },
  {
    id: 3,
    position: 3,
    name: 'El Quorum y las Mayorias exigibles',
    question: [
      {
        id: 1,
        name: '¿Nombre de la pregunta...?',
        position: 1,
        answer: 'Parcial',
        subAnswer: 'Nombre de la sub respuesta significativa',
        necesity: 'Imprecindible',
                commit: 'Comentario sobre la pregunta',
      }
    ]
  },
  {
    id: 4,
    position: 4,
    name: 'Derecho a la no difusión de la participación en el capital de la sociedad',
    question: [
      {
        id: 1,
        name: '¿Nombre de la pregunta...?',
        position: 1,
        answer: 'Parcial',
        subAnswer: 'Nombre de la sub respuesta significativa',
        necesity: 'Imprecindible',
                commit: 'Comentario sobre la pregunta',
      }
    ]
  },
  {
    id: 5,
    position: 5,
    name: 'Regimen de transmisión de las participaciones del capital social',
    question: [
      {
        id: 1,
        name: '¿Nombre de la pregunta...?',
        position: 1,
        answer: 'Parcial',
        subAnswer: 'Nombre de la sub respuesta significativa',
        necesity: 'Imprecindible',
                commit: 'Comentario sobre la pregunta',
      }
    ]
  },
  {
    id: 6,
    position: 6,
    name: 'Fomento de la participación y entrega continua de información a los accionistas',
    question: [
      {
        id: 1,
        name: '¿Nombre de la pregunta...?',
        position: 1,
        answer: 'Parcial',
        subAnswer: 'Nombre de la sub respuesta significativa',
        necesity: 'Imprecindible',
                commit: 'Comentario sobre la pregunta',
      }
    ]
  },
  {
    id: 7,
    position: 7,
    name: 'Nombre de la medida...',
    question: [
      {
        id: 1,
        name: '¿Nombre de la pregunta...?',
        position: 1,
        answer: 'Parcial',
        subAnswer: 'Nombre de la sub respuesta significativa',
        necesity: 'Imprecindible',
                commit: 'Comentario sobre la pregunta',
      }
    ]
  },
  {
    id: 8,
    position: 8,
    name: 'Nombre de la medida...',
    question: [
      {
        id: 1,
        name: '¿Nombre de la pregunta...?',
        position: 1,
        answer: 'Parcial',
        subAnswer: 'Nombre de la sub respuesta significativa',
        necesity: 'Imprecindible',
                commit: 'Comentario sobre la pregunta',
      },
      {
        id: 2,
        name: '¿Nombre de la pregunta...?',
        position: 2,
        answer: 'Parcial',
        subAnswer: 'Nombre de la sub respuesta significativa',
        necesity: 'Imprecindible',
                commit: 'Comentario sobre la pregunta',
      },
      {
        id: 3,
        name: '¿Nombre de la pregunta...?',
        position: 3,
        answer: 'Parcial',
        subAnswer: 'Nombre de la sub respuesta significativa',
        necesity: 'Imprecindible',
                commit: 'Comentario sobre la pregunta',
      },
      {
        id: 4,
        name: '¿Nombre de la pregunta...?',
        position: 4,
        answer: 'Parcial',
        subAnswer: 'Nombre de la sub respuesta significativa',
        necesity: 'Imprecindible',
                commit: 'Comentario sobre la pregunta',
      },
    ]
  },
];
@Component({
  selector: 'app-block-result',
  templateUrl: './block-result.component.html',
  styleUrls: ['./block-result.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})

export class BlockResultComponent implements OnInit {
  @Input() idBlock: number;
  nameBlock = 'Derecho y trato equitativo de accionistas';
  panelOpenState = false;
  dataMeasure = DATA;
  columnsToDisplay: string[] = ['position', 'name', 'answer', 'necesity'];
  expandedElement: Measure | null;
  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  goToBlockList(): void {
    console.log('Go to list');
    this.router.navigateByUrl('/content/evaluation/evaluation-list', { skipLocationChange: true }).then(() => {
      this.router.navigate(['content/evaluation/evaluation-list/result']);
    });
  }

}
