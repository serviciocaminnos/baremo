import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ResultComponent } from './result.component';
import { EvaluationResultComponent } from './evaluation-result/evaluation-result.component';

const routes: Routes = [
  {
    path: '',
    component: ResultComponent,
  },
  {path: 'result', component: EvaluationResultComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResultRoutingModule { }
