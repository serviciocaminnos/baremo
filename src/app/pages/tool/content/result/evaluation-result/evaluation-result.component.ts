import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { Label, MultiDataSet, Color} from 'ng2-charts';
import { ChartType, ChartDataSets, ChartOptions } from 'chart.js';

export interface Block {
  id: number;
  name: string;
  countMeasure: number;
  countQuestion: number;
  result: number;
  color: string;
}
const BLOCK_DATA: Block[] = [
  {
    id: 1,
    name: 'Derecho y trato equitativo de accionistas',
    countMeasure: 10,
    countQuestion: 20,
    result: 15,
    color: 'warn',
  },
  {
    id: 2,
    name: 'Asamblea General de Accionistas',
    countMeasure: 10,
    countQuestion: 20,
    result: 30,
    color: 'warn',

  },
  {
    id: 3,
    name: 'El Directorio',
    countMeasure: 10,
    countQuestion: 20,
    result: 45,
    color: 'warn',

  },
  {
    id: 4,
    name: 'Alta Gerencia',
    countMeasure: 10,
    countQuestion: 20,
    result: 50,
    color: 'accent',

  },
  {
    id: 5,
    name: 'Información financiera y no financiera',
    countMeasure: 10,
    countQuestion: 20,
    result: 65,
    color: 'accent',

  },
  {
    id: 6,
    name: 'Administración de Riesgos',
    countMeasure: 10,
    countQuestion: 20,
    result: 85,
    color: 'primary',

  },
  {
    id: 7,
    name: 'Empresa de Familia',
    countMeasure: 10,
    countQuestion: 20,
    result: 100,
    color: 'primary',

  },
];
@Component({
  selector: 'app-evaluation-result',
  templateUrl: './evaluation-result.component.html',
  styleUrls: ['./evaluation-result.component.css']
})
export class EvaluationResultComponent implements OnInit {
  nameBussines = 'Toyosa';
  idBlock: number;
  // Doughnut
  public doughnutChartLabels: Label[] = ['Si', 'No'];
  public doughnutChartData: MultiDataSet = [
    [70, 30],
  ];
  public doughnutChartType: ChartType = 'doughnut';
  public donutColors = [
    {
      backgroundColor: [
        '#00E676',
        '#FF1744',
    ]
    }
  ];
  public dinamycColors: Color[] = [
    {
      backgroundColor: [
        '#00E676',
        '#00E676',
        '#00E676',
        '#00E676',
        '#00E676',
        '#00E676',
        '#00E676',
    ]
    },
    {
      backgroundColor: [
        '#FF1744',
        '#FF1744',
        '#FF1744',
        '#FF1744',
        '#FF1744',
        '#FF1744',
        '#FF1744',
    ]
    }
  ];
  dataSource = BLOCK_DATA;
  displayedColumns: string[] = ['block', 'measure', 'question', 'result'];
  hidden = true;
  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
  };
  public barChartLabels: Label[] = ['2016', '2017', '2018', '2019', '2020', '2021', '2022'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;

  public barChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 70, 55, 40], label: 'Si' },
    { data: [28, 48, 40, 19, 30, 27, 90], label: 'No' }
  ];
  dinamycDonutColors: Color[];
  count = 0;
  constructor(private router: Router) { }
  ngOnInit(): void {
    this.dinamycDonutColors = this.dinamycColors;
  }
  backListEvaluation(): void {
    this.router.navigate(['content/evaluation/evaluation-list']);
  }
  goBlockResult(): void {
    this.router.navigate(['content/evaluation/evaluation-list']);
  }
  sendData(idBlock: number): void {
    this.idBlock = idBlock;
    this.hidden = false;
  }
  // events
  chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }
  chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }
  randomize(): void {
    if (this.count === 0) {
      this.count += 1;
      const lineChartColors: Color[] = [
        { // grey
          backgroundColor: 'rgba(0, 230, 118,0.2)',
          borderColor: 'rgba(0, 230, 118,0.5)',
          pointBackgroundColor: '#00E676',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: '#00E676'
        },
        { // red
          backgroundColor: 'rgba(255, 23, 68,0.2)',
          borderColor: 'red',
          pointBackgroundColor: '#FF1744',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#FF1744',
          pointHoverBorderColor: '#FF1744'
        }
      ];
      this.dinamycDonutColors = lineChartColors;
    }else {
      this.count = 0;
      this.dinamycDonutColors = this.dinamycColors;
    }
    this.barChartType = this.barChartType === 'bar' ? 'line' : 'bar';
  }
}
