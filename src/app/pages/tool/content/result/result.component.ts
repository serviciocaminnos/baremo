import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

export interface Evaluation {
  id: number;
  urlImgProfile: string;
  nameEvaluator: string;
  lastNameEvaluator: string;
  emailEvaluator: string;
  evaluationDate: string;
  state: number;
  complete: number;
}
const EVALUATION_DATA: Evaluation[] = [
  {
    id: 1,
    urlImgProfile: 'https://happytravel.viajes/wp-content/uploads/2020/04/146-1468479_my-profile-icon-blank-profile-picture-circle-hd.png',
    nameEvaluator: 'Ava',
    lastNameEvaluator: 'Moore',
    emailEvaluator: 'avamoore@gmail.com',
    evaluationDate: '28/05/2020',
    state: 25,
    complete: 100
  },
  {
    id: 2,
    urlImgProfile: 'https://happytravel.viajes/wp-content/uploads/2020/04/146-1468479_my-profile-icon-blank-profile-picture-circle-hd.png',
    nameEvaluator: 'Beth',
    lastNameEvaluator: 'Petterson',
    emailEvaluator: 'beth@gmail.com',
    evaluationDate: '28/05/2020',
    state: 50,
    complete: 100
  },
  {
    id: 3,
    nameEvaluator: 'James',
    lastNameEvaluator: 'Smith',
    urlImgProfile: 'https://happytravel.viajes/wp-content/uploads/2020/04/146-1468479_my-profile-icon-blank-profile-picture-circle-hd.png',
    emailEvaluator: 'avamoore@gmail.com',
    evaluationDate: '28/05/2020',
    state: 75,
    complete: 100
  },
  {
    id: 5,
    nameEvaluator: 'Rachel',
    lastNameEvaluator: 'Jones',
    urlImgProfile: 'https://happytravel.viajes/wp-content/uploads/2020/04/146-1468479_my-profile-icon-blank-profile-picture-circle-hd.png',
    emailEvaluator: 'avamoore@gmail.com',
    evaluationDate: '28/05/2020',
    state: 100,
    complete: 100
  }
];

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {
  nameBussines = 'Toyosa';
  emptyResult: boolean;
  dataSource = EVALUATION_DATA;
  displayedColumns: string[] = ['name', 'evaluationDate', 'state'];

  constructor(private router: Router) { }

  ngOnInit(): void {
    if (this.dataSource.length !== 0) {
      this.emptyResult = false;
    } else {
      this.emptyResult = true;
    }
  }
  goToEvaluation(): void {
    this.router.navigate(['content/evaluation']);
  }
  goToResult(idCompany): void {
    this.router.navigateByUrl('/content/evaluation/evaluation-list', { skipLocationChange: true }).then(() => {
      console.log(idCompany);
      this.router.navigate(['content/evaluation/evaluation-list/result']);
    });
  }
  goToStartEvaluation(): void {
    this.router.navigateByUrl('/content/evaluation/evaluation-list', { skipLocationChange: true }).then(() => {
      this.router.navigate(['content/evaluation/start']);
    });
  }
}
