import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrganizationRoutingModule } from './organization-routing.module';
import { OrganizationComponent } from './organization.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { EditComponent } from './components/edit/edit.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { AngularMaterialModule } from 'src/app/shared/material-angular/angular-material.module';
import { BlockComponent } from './components/block/block.component';
import { MeasureComponent } from './components/measure/measure.component';
import { QuestionComponent } from './components/question/question.component';

@NgModule({
  declarations: [
    OrganizationComponent,
    BlockComponent,
    MeasureComponent,
    QuestionComponent,
    EditComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    OrganizationRoutingModule,
    ReactiveFormsModule,
    SharedModule,
    AngularMaterialModule
  ],
  entryComponents: [
    OrganizationComponent,
    BlockComponent,
    MeasureComponent,
    QuestionComponent,
    EditComponent
  ],
  exports: [
    OrganizationComponent,
    BlockComponent,
    MeasureComponent,
    QuestionComponent
  ]
})
export class OrganizationModule { }
