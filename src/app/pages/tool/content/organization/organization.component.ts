import { ToolService } from './../../../../core/services/baremo/tool.service';
import { Component, OnInit, DoCheck, AfterViewInit, AfterViewChecked, Output, EventEmitter } from '@angular/core';
import { AuthConstants } from 'src/app/core/config/auth-constanst';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/core/services/baremo/storage.service';
import { DataBridgeService } from 'src/app/core/services/baremo/data-bridge.service';

@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.css']
})
export class OrganizationComponent implements OnInit, DoCheck {
  // @Output() sendIdMeasure = new EventEmitter<number>();
  dataBlock: any = {
    name: ''
  };
  dataMeasure: any = {
    name: ''
  };
  idMeasuere = 1;
  dataQuestion: any;
  rol: any;
  rolHidden: boolean;
  title: string;
  titleTest: string;
  token: any;
  idUser: any;
  idTool: any;
  blocks: any;
  viewBlock: any;
  viewMeasure: any;
  viewquestion: any;
  progressStatus: boolean;
  hiddenProgressListTool: boolean;
  hiddenProgressToolContent: boolean;
  constructor(private toolService: ToolService,
              private storage: StorageService,
              private dataBridge: DataBridgeService,
              private router: Router) {
    this.storage.get(AuthConstants.AUTH).then((token) => {
      this.token = token;
    });
    this.storage.get(AuthConstants.USER_ID).then((idUser) => {
      this.idUser = idUser;
    });
    this.storage.get(AuthConstants.ROL).then((rol) => {
      this.rol = rol;
    });
    this.storage.get(AuthConstants.ID_TOOL).then((idTool) => {
      this.idTool = idTool;
    });
  }

  ngOnInit(): void {
    if (this.rol !== 0) {
      this.rolHidden = false;
      this.title = 'ORGANIZACIÓN';
    } else {
      this.rolHidden = true;
      this.title = 'EVALUACIÓN';
    }
    // this.getDataBlock();
    // this.VistaHerramienta();
  }
  ngDoCheck(): void {
    // console.log('ReciveData');
    this.getDataBlock();
  }
  getDataBlock(): void {
    this.dataBridge.$getObjectSource.subscribe( (data: any) => {
      // console.log(data);
      if (Object.keys(data).length === 0 && data.constructor === Object) {
        // console.log('no hay datos iniciales');
      } else {
        const pos = data.measurePos;
        this.dataBlock = data.block;
        const objectMeasure = data.measure;
        // console.log(this.dataBlock);
        // console.log(data);
        // console.log(objectMeasure);

        if (Array.isArray(objectMeasure) === true) {
          // m = measure
          objectMeasure.forEach((m: any) => {
            if (m.position === pos){
              this.dataMeasure = m;
              this.idMeasuere = m.id;
              // this.sendIdMeasure.emit(this.idMeasuere);
            }
          });
        } else {
          this.dataMeasure = objectMeasure;
          this.idMeasuere = objectMeasure.id;
          // this.sendIdMeasure.emit(this.idMeasuere);
        }
    }
    }).unsubscribe();
  }
  goToResult(idCompany): void {
    this.router.navigateByUrl('/content/evaluation/evaluation-list', { skipLocationChange: true }).then(() => {
      console.log(idCompany);
      this.router.navigate(['content/evaluation/evaluation-list']);
    });
  }
}
