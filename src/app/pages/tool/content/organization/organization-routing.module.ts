import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrganizationComponent } from './organization.component';
import { EditComponent } from './components/edit/edit.component';
import { ResultComponent } from '../result/result.component';

const routes: Routes = [
  {path: '', component: OrganizationComponent, data: {animation: 'Organization'}},
  {path: 'edit', component: EditComponent, data: {animation: 'Edit'}},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrganizationRoutingModule { }
