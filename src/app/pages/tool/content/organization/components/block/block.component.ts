import { Component, OnInit, Output, Input, ViewChild, EventEmitter, DoCheck } from '@angular/core';
import { AuthConstants } from 'src/app/core/config/auth-constanst';
import { iconStateBlock } from 'src/app/router-animation';
import { MatDialog } from '@angular/material/dialog';
import { EditComponent } from '../edit/edit.component';
import { ToolService } from 'src/app/core/services/baremo/tool.service';
import { StorageService } from 'src/app/core/services/baremo/storage.service';
import { DataBridgeService } from 'src/app/core/services/baremo/data-bridge.service';
import { SendDataService } from 'src/app/core/services/baremo/SendData.service';

export interface DialogData {
  id: string;
  option: string;
}
@Component({
  selector: 'app-block',
  templateUrl: './block.component.html',
  styleUrls: ['./block.component.css'],
  animations: [
    iconStateBlock
  ]
})
export class BlockComponent implements OnInit{
  @Input() dataBlock;
  isOpen = true;
  hiddenProgressListBlock = false;
  blockList: [];
  idTool: any;
  token: string;
  blockPosition: any;
  titleBlock: any;
  idBlock: any;
  rol: any;
  rolHidden: boolean;
  constructor(private toolService: ToolService,
              private storageService: StorageService,
              private dataBridge: DataBridgeService,
              public dialog: MatDialog)
  {
    this.storageService.get(AuthConstants.AUTH).then(token => {
      this.token = token;
    });
    this.storageService.get(AuthConstants.BLOCK_POSITION).then(blockPosition => {
      this.blockPosition = blockPosition;
    });
    this.storageService.get(AuthConstants.ID_TOOL).then(idtool => {
      this.idTool = idtool;
    });
    this.storageService.get(AuthConstants.ROL).then((rol) => {
      this.rol = rol;
    });

    // this.buildForm();
  }

  ngOnInit(): void {
    // this.getBlockList();
    if (this.rol !== 0) {
      this.rolHidden = true;
    } else {
      this.rolHidden = false;
    }
    this.getInformationBlock();
  }
  getBlockData(): void {
    this.dataBridge.$getObjectSource.subscribe( (data: any) => {
      // console.log(data);
      this.titleBlock = data.name;
      this.idBlock = data.id;
    }).unsubscribe();
  }
  getInformationBlock(): any {
    this.storageService.get(AuthConstants.BLOCK_POSITION).then(blockPosition => {
      this.blockPosition = blockPosition;
      const serverRequest = new SendDataService();
      serverRequest.createServiceName('getBlockWithPosition');
      serverRequest.addAuthorization(this.token);
      serverRequest.addParams(this.blockPosition + '/' + this.idTool);
      this.toolService.readData(serverRequest).subscribe(
        res => {
          this.dataBlock = res;
          // this.titleBlock = res.name;
          // this.idBlock = res.id;
        }, error => {
          console.log(error);
        }
        );
      });
  }
  blockInformationEdit(): any {
    console.log(this.idBlock);
  }
  openDialogEdit(idBlock): void {
    const dialogRef = this.dialog.open(EditComponent, {
      // padding: '0px',
      width: '40%',
      height: '60%',
      data: {
        id: idBlock,
        option: 'block'
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getInformationBlock();
    });
  }
}
