import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { AuthConstants } from 'src/app/core/config/auth-constanst';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { ToolService } from 'src/app/core/services/baremo/tool.service';
import { StorageService } from 'src/app/core/services/baremo/storage.service';
import { SendDataService } from 'src/app/core/services/baremo/SendData.service';
export interface DialogData {
  id: string;
  option: string;
  option2: string;
}

export interface BlockData {
  id: string;
  tool_id: string;
  name: string;
  description: string;
  porcentage: number;
  position: number;
  created_at: string;
  updated_at: string;
}
@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  animal: string;
  name: string;
  token: any;
  contentEditBlockForm = false;
  contentEditMeasureForm = false;
  contentEditQuestionForm = false;
  contentEditTypeQuestionForm = false;
  contentEditTypeAnswerArray = false;
  contentRemove = false;

  dataServer: any;

  blockForm: FormGroup;
  measureForm: FormGroup;
  questionForm: FormGroup;
  typeAnswerForm: FormGroup;

  title: string;
  subTitle: string;

  dataName: string;
  dataDescription: string;
  dataPorcentage: number;
  hiddenProgress = true;

  options: any = [
  ];
  typeAnswerList: FormArray;
  emptyMessage: string;
  showEmptyMessage: boolean;
  makeNewAnswer: boolean;

  constructor(public dialogRef: MatDialogRef<EditComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData,
              private toolService: ToolService,
              private storageService: StorageService,
              private formBuilder: FormBuilder,
              private router: Router
              )
  {
    this.buildForm();
    this.storageService.get(AuthConstants.AUTH).then(token => {
      this.token = token;
    });
  }

  ngOnInit(): void {
    this.getAllInformation(this.data.option);
  }
  buildForm(): any {
    this.blockForm = this.formBuilder.group({
      name: [''],
      description: ['']
    });
    this.measureForm = this.formBuilder.group({
      name: [''],
      description: [''],
      porcentage: [],
    });
    this.questionForm = this.formBuilder.group({
      name: [''],
      description: [''],
      porcentage: [''],
    });
    this.typeAnswerForm = this.formBuilder.group({
      idQuestion: [''],
      answers: this.formBuilder.array([])
    });
  }
  answers(): FormArray {
    return this.typeAnswerForm.get('answers') as FormArray;
  }
  newAnswer(idQuestion: string, name: string, porct: string, subAnswer: string, need: string): FormGroup {
    return this.formBuilder.group({
      id_question: [idQuestion, [Validators.required]],
      name: [name],
      porcentage: [porct],
      sub_answer: [subAnswer],
      need: [need],
    });
  }
  addAnswer(idQuestion: string, name: string, porct: string, subAnswer: string, need: string): any{
    this.answers().push(this.newAnswer(idQuestion, name, porct, subAnswer, need));
  }
  saveAnswers(): void {
    this.hiddenProgress = true;
    const answerArray: [] = this.typeAnswerForm.controls.answers.value;
    answerArray.forEach((answer: any) => {
      const req = new SendDataService();
      req.addAuthorization(this.token);
      req.addData(answer);
      req.createServiceName('globalAnswers');
      this.toolService.createData(req).subscribe((res: any) => {
        console.log(res);
      }, error => {
        console.log(error);
      });
      this.hiddenProgress = false;
      // console.log(answer);
      this.showMessageConfirm();
      this.dialogRef.close();

    });
  }
  getAllInformation(option): any {
    switch (option){
      case 'block':
        this.getBlockInfo(this.data.id);
        break;
        case 'measure':
          this.getMeasureInfo(this.data.id);
          break;
          case 'question':
            this.getQuestionInfo(this.data.id);
            break;
            case 'typeQuestion':
              this.getTypeQuestionInfo(this.data.id);
              break;
              case 'remove':
                this.contentRemove = true;
                this.removeInfo(this.data.id, this.data.option2);
                this.hiddenProgress = false;
                break;
    }
  }
  removeInfo(id: string, option2: string): void {
    switch (option2) {
      case 'block':
        this.title = 'Eliminar Bloque';
        break;
        case 'measure':
          this.title = 'Eliminar Medida';
          break;
          case 'question':
            this.title = 'Eliminar Pregunta';
            break;
    }
  }
  getBlockInfo(id: string): any {
    this.contentEditBlockForm = true;
    this.title = 'Editar Bloque';
    this.subTitle = 'Bloque';
    const requestServer = new SendDataService();
    requestServer.addAuthorization(this.token);
    requestServer.createServiceName('block');
    requestServer.addParams(id);
    this.toolService.readData(requestServer).subscribe(
      res => {
        this.blockForm.patchValue(res);
        // console.log(res);
        this.hiddenProgress = false;
      }, error => {
        console.log(error);
      }
    );
  }

  getMeasureInfo(id: string): any {
    this.contentEditMeasureForm = true;
    this.title = 'Editar Medida';
    this.subTitle = 'Medida';
    const requestServer = new SendDataService();
    requestServer.addAuthorization(this.token);
    requestServer.createServiceName('measure');
    requestServer.addParams(id);
    this.toolService.readData(requestServer).subscribe(
      res => {
        this.measureForm.patchValue(res);
        console.log(res);
        this.hiddenProgress = false;
      }, error => {
        console.log(error);
      }
    );
  }
  getQuestionInfo(id: string): any {
    this.contentEditQuestionForm = true;
    this.title = 'Editar Pregunta';
    this.subTitle = 'Pregunta';
    const requestServer = new SendDataService();
    requestServer.addAuthorization(this.token);
    requestServer.createServiceName('question');
    requestServer.addParams(id);
    this.toolService.readData(requestServer).subscribe(
      res => {
        this.questionForm.patchValue(res);
        console.log(res);
        this.hiddenProgress = false;
      }, error => {
        console.log(error);
      }
    );
  }
  getTypeQuestionInfo(id: string): any {
    // tslint:disable-next-line: no-string-literal
    this.typeAnswerForm.controls['idQuestion'].setValue(id);
    this.contentEditTypeQuestionForm = true;
    this.title = 'Editar Tipo de Respuestas';
    this.subTitle = 'Tipo de Respuesta';
    this.verifyAnswerType(id);
  }
  verifyAnswerType(idQuestion): void {
    const reqQ = new SendDataService();
    reqQ.addAuthorization(this.token);
    reqQ.createServiceName('globalAnswersByQuestion');
    reqQ.addParams(idQuestion);
    this.toolService.readData(reqQ).subscribe(
      res => {
        if (Array.isArray(res) === true) {
          console.log(res);
          // this.getAllAnswerByIdQuestion(res.answer_type_id);
          this.generateTypesAnswer(res, true);
          this.showEmptyMessage = false;
        } else {
          this.emptyMessage = 'Aun no tienes guardado nunguna respuesta';
          this.showEmptyMessage = true;
        }
        this.hiddenProgress = false;
      },
      error => {
        console.log(error);
      }
    );
  }
  getAllAnswerByAnswerByTypeId(answerTypeId): void {
    const reqTypeAnswer = new SendDataService();
    reqTypeAnswer.addAuthorization(this.token);
    reqTypeAnswer.createServiceName('getAnswerOfAnswerType');
    reqTypeAnswer.addParams(answerTypeId);
    this.toolService.readData(reqTypeAnswer).subscribe(
      res => {
        console.log(res);
        this.generateTypesAnswer(res, false);
      },
      error => {
        console.log(error);
      }
    );
  }
  // tslint:disable-next-line: typedef
  async generateTypesAnswer(answerList: any, option: boolean){
    this.contentEditTypeAnswerArray =  true;
    if (option === true) {
      answerList.forEach(answer => {
        this.addAnswer(this.data.id , answer.name, answer.porcentage, answer.sub_answer, answer.need);
      });
    } else {
      answerList.forEach(answer => {
        this.addAnswer(this.data.id , answer.name, answer.porcentage, '', '');
      });
    }
  }
  saveChange(option): any {
    this.hiddenProgress = true;
    switch (option){
      case 'block':
        const requestBlockServer = new SendDataService();
        requestBlockServer.addAuthorization(this.token);
        requestBlockServer.createServiceName(option);
        requestBlockServer.addParams(this.data.id);
        requestBlockServer.addData(this.blockForm.value);
        this.toolService.updateData(requestBlockServer).subscribe(
          res => {
            // console.log(res);
            // this.showMessageConfirm();
            this.dialogRef.close();
          }, error => {
            console.log(error);
          }
        );
        break;
        case 'measure':
          const requestMeasureServer = new SendDataService();
          requestMeasureServer.addAuthorization(this.token);
          requestMeasureServer.createServiceName(option);
          requestMeasureServer.addParams(this.data.id);
          requestMeasureServer.addData(this.measureForm.value);
          this.toolService.updateData(requestMeasureServer).subscribe(
            res => {
              // console.log(res);
              // this.showMessageConfirm();
              this.dialogRef.close();
            }, error => {
              console.log(error);
            }
          );
          break;
          case 'remove':
            const requestQuestionServer = new SendDataService();
            requestQuestionServer.addAuthorization(this.token);
            requestQuestionServer.createServiceName(this.data.option2);
            requestQuestionServer.addParams(this.data.id);
            requestQuestionServer.addData(this.questionForm.value);
            this.toolService.updateData(requestQuestionServer).subscribe(
              res => {
                console.log(res);
                // this.showMessageConfirm();
                this.dialogRef.close();
              }, error => {
                console.log(error);
              }
            );
            break;
    }
  }
  showMessageConfirm(): any {
    this.hiddenProgress = false;
    // window.location.reload();
    this.router.navigateByUrl('/tool/content/organization', { skipLocationChange: true }).then(() => {
      this.router.navigate(['content/organization']);
    });
}
  getTypeAnswers(): void {
    const typeAnswerRequest = new SendDataService();
    typeAnswerRequest.addAuthorization(this.token);
    typeAnswerRequest.createServiceName('answerType');
    this.toolService.readData(typeAnswerRequest).subscribe(
      res => {
        this.options = res;
        // console.log(res);
      }, error => {
        console.log(error);
      }
    );
  }
  // tslint:disable-next-line: typedef
  async modifyTypeAnswerByQuestion(option){
    await this.answers().clear();
    await this.getAllAnswerByAnswerByTypeId(option.id);
    // console.log(option);
    // console.log(this.data.id);
    this.makeNewAnswer = false;
    // const questionForm: FormGroup = this.formBuilder.group({
    //   answer_type_id: []
    // });
    // // tslint:disable-next-line: no-string-literal
    // await questionForm.controls['answer_type_id'].setValue(idTypeAnswer);
    // const typeAnswerByQuestionRequest = new SendDataService();
    // typeAnswerByQuestionRequest.addAuthorization(this.token);
    // typeAnswerByQuestionRequest.createServiceName('question');
    // typeAnswerByQuestionRequest.addParams(this.data.id);
    // await typeAnswerByQuestionRequest.addData(questionForm.value);
    // console.log(questionForm.value);
    // this.toolService.updateData(typeAnswerByQuestionRequest).subscribe(
    //   res => {
    //     // console.log(res);
    //     this.verifyAnswerType(this.data.id);
    //     // this.typeAnswerForm.reset();
    //     this.answers().clear();
    //   },
    //   error => {
    //     console.log(error);
    //   }
    // );
  }
  // tslint:disable-next-line: typedef
  async makeNewTypeAnswer(){
    await this.answers().clear();
    this.addAnswer(this.data.id, '', '', '', '');
    this.makeNewAnswer = true;
    if (this.answers().length !== 0){
      this.showEmptyMessage = false;
    } else {
      this.showEmptyMessage = true;
    }
  }
  removeAnswer(index): void{
    this.answers().removeAt(index);
  }
  destroyData(): void {
    this.hiddenProgress = true;
    console.log(this.data.option);
    console.log(this.data.id);
    console.log(this.data.option2);

    switch (this.data.option2){
      case 'block':
        const requestBlockServer = new SendDataService();
        requestBlockServer.addAuthorization(this.token);
        requestBlockServer.createServiceName(this.data.option2);
        requestBlockServer.addParams(this.data.id);
        this.toolService.deleteData(requestBlockServer).subscribe(
          res => {
            console.log(res);
            // this.showMessageConfirm();
            this.dialogRef.close();
          }, error => {
            console.log(error);
          }
        );
        break;
        case 'measure':
          const requestMeasureServer = new SendDataService();
          requestMeasureServer.addAuthorization(this.token);
          requestMeasureServer.createServiceName(this.data.option2);
          requestMeasureServer.addParams(this.data.id);
          this.toolService.deleteData(requestMeasureServer).subscribe(
            res => {
              console.log(res);
              // this.showMessageConfirm();
              this.dialogRef.close();
            }, error => {
              console.log(error);
            }
          );
          break;
          case 'question':
            const requestQuestionServer = new SendDataService();
            requestQuestionServer.addAuthorization(this.token);
            requestQuestionServer.createServiceName(this.data.option2);
            requestQuestionServer.addParams(this.data.id);
            this.toolService.deleteData(requestQuestionServer).subscribe(
              res => {
                console.log(res);
                // this.showMessageConfirm();
                this.dialogRef.close();
              }, error => {
                console.log(error);
              }
            );
            break;
    }
  }
}
