import { Component, OnInit, Input, EventEmitter, DoCheck } from '@angular/core';
import { AuthConstants } from 'src/app/core/config/auth-constanst';
import { EditComponent } from '../edit/edit.component';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { ToolService } from 'src/app/core/services/baremo/tool.service';
import { StorageService } from 'src/app/core/services/baremo/storage.service';
import { DataBridgeService } from 'src/app/core/services/baremo/data-bridge.service';
import { SendDataService } from 'src/app/core/services/baremo/SendData.service';

@Component({
  selector: 'app-measure',
  templateUrl: './measure.component.html',
  styleUrls: ['./measure.component.css']
})
export class MeasureComponent implements OnInit {
  @Input() dataMeasure;
  blockId;
  token: string;
  idTool: string;
  measureList: [];
  measurePosition: any;
  titleMeasure: any;
  idMeasure: any;
  blockPosition: any;
  questionForm: any;
  hiddenProgressListBlock: boolean;
  rol: any;
  rolHidden: boolean;
  constructor(private toolService: ToolService,
              private storageService: StorageService,
              private dialog: MatDialog,
              private router: Router,
              private dataBridge: DataBridgeService,
              private formbuilder: FormBuilder)
  {
    this.buildForm();
    this.storageService.get(AuthConstants.AUTH).then(token => {
      this.token = token;
    });
    this.storageService.get(AuthConstants.ID_BOCK).then(idBlock => {
      this.blockId = idBlock;
    });
    this.storageService.get(AuthConstants.MEASURE_POSITION).then(measurePosition => {
      this.measurePosition = measurePosition;
    });
    this.storageService.get(AuthConstants.ROL).then((rol) => {
      this.rol = rol;
    });
    this.dataBridge.$getObjectSource.subscribe( (data: any) => {
      // console.log(data);
    }).unsubscribe();

  }

  ngOnInit(): void {
    if (this.rol !== 0) {
      this.rolHidden = true;
    } else {
      this.rolHidden = false;
    }
    this.getInformationMeasure();
  }
  getDataMeasure(): void {
    this.dataBridge.$getObjectSource.subscribe( (data: any) => {
      console.log(data);
    }).unsubscribe();
  }
  buildForm(): any{
    this.questionForm = this.formbuilder.group({
      measure_id: [''],
      name: ['Pregunta'],
      description: ['Pregunta de la Medida'],
      porcentage: ['0'],
    });
}
  getInformationMeasure(): any {
    this.storageService.get(AuthConstants.MEASURE_POSITION).then(measurePosition => {
      this.measurePosition = measurePosition;
      const serverRequest = new SendDataService();
      serverRequest.createServiceName('getMeasureWithPosition');
      serverRequest.addAuthorization(this.token);
      serverRequest.addParams(this.measurePosition + '/' + this.blockId);
      this.toolService.readData(serverRequest).subscribe(
        res => {
          this.dataMeasure = res;
          // this.titleMeasure = res.name;
          this.idMeasure = res.id;
          // console.log(res);
        }, error => {
          console.log(error);
        }
        );
      });
  }
  openDialogEdit(idBlock): void {
    const dialogRef = this.dialog.open(EditComponent, {
      // padding: '0px',
      width: '40%',
      height: '60%',
      data: {
        id: idBlock,
        option: 'measure'
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getInformationMeasure();
    });
  }
  addNewQuestion(): any {
    this.hiddenProgressListBlock = true;
    // tslint:disable-next-line: no-string-literal
    this.questionForm.controls['measure_id'].setValue(this.dataMeasure.id);
    const questionRequest = new SendDataService();
    questionRequest.createServiceName('question');
    questionRequest.addAuthorization(this.token);
    questionRequest.addResponseType('json');
    questionRequest.addData(this.questionForm.value);
    this.toolService.createData(questionRequest).subscribe(
      (questionData) => {
        // tslint:disable-next-line: triple-equals
        if (questionData.status_code == '200') {
          // console.log(questionData);
          this.hiddenProgressListBlock = false;
          this.router.navigateByUrl('/tool/content/organization', { skipLocationChange: true }).then(() => {
            this.router.navigate(['content/organization']);
          });
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
