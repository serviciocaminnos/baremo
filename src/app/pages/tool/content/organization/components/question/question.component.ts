import { Component, OnInit, Input, DoCheck, AfterViewInit, OnChanges } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { AuthConstants } from 'src/app/core/config/auth-constanst';
import { EditComponent } from '../edit/edit.component';
import { MatDialog } from '@angular/material/dialog';
import { StorageService } from 'src/app/core/services/baremo/storage.service';
import { ToolService } from 'src/app/core/services/baremo/tool.service';
import { DataBridgeService } from 'src/app/core/services/baremo/data-bridge.service';
import { SendDataService } from 'src/app/core/services/baremo/SendData.service';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit, OnChanges {
  @Input() idMeasure;
  subQuestionForm: FormGroup;
  commitForm: FormGroup;

  measurePosition: any;
  idBlock: any;
  token: any;
  checked = false;
  testMessage: string;
  showTestMessage: boolean;
  rol: any;
  rolHidden: boolean;

  status: boolean;
  showEmptyMessage: boolean;
  emptyMessage: string;
  hiddenProgress: boolean;

  questions: Array<any> = [];

  viewQuestion: any = [];
  viewAnswer: any = [];
  answers: any;
  constructor(private formBuilder: FormBuilder,
              private storageService: StorageService,
              private toolService: ToolService,
              private dataBridge: DataBridgeService,
              private dialog: MatDialog)
  {
    this.buildForm();
    this.storageService.get(AuthConstants.MEASURE_POSITION).then(measurePosition => {
      this.measurePosition = measurePosition;
    });
    this.storageService.get(AuthConstants.ID_BOCK).then(idBlock => {
      this.idBlock = idBlock;
    });
    this.storageService.get(AuthConstants.AUTH).then(token => {
      this.token = token;
    });
    this.storageService.get(AuthConstants.ROL).then((rol) => {
      this.rol = rol;
    });
    this.dataBridge.$getObjectSource.subscribe( (data: any) => {
      // console.log(data);
    }).unsubscribe();
  }
  ngOnInit(): void {
    if (this.rol !== 0) {
      this.rolHidden = true;
    } else {
      this.rolHidden = false;
    }
    this.getMeasureData();
  }
  ngOnChanges(): void {
    // console.log(this.idMeasure);
    this.getQuestions();
  }
  getMeasureData(): any {
    const measureRequest = new SendDataService();
    measureRequest.addAuthorization(this.token);
    measureRequest.createServiceName('getMeasureWithPosition');
    measureRequest.addParams(this.measurePosition + '/' + this.idBlock);
    this.toolService.readData(measureRequest).subscribe(
      res => {
        this.idMeasure = res.id;
        this.getQuestions();
      }, error => {
        console.log(error);
      }
    );
  }
  getQuestions(): any {
    this.status = true;
    const questionServer = new SendDataService();
    questionServer.addAuthorization(this.token);
    questionServer.createServiceName('getQuestionOfMeasure');
    questionServer.addParams(this.idMeasure);
    this.toolService.readData(questionServer).subscribe(
      questions => {
        this.status = false;
        this.questions = questions;
        questions.forEach(element => {
          if (element.position === 1) {
            this.getAnswers(element.id);
          }
        });
        // console.log(questions);
      }, error => {
        console.log(error);
        // window.location.reload();
      }
    );
  }
  buildForm(): any {
    this.subQuestionForm = this.formBuilder.group({
      idQuestion: [''],
      questions: this.formBuilder.array([])
    });
    this.commitForm = this.formBuilder.group({
      commit: [''],
    });
  }
  subQuestions(): FormArray {
    return this.subQuestionForm.get('questions') as FormArray;
  }
  newSubQuestion(): FormGroup {
    return this.formBuilder.group({
      name: [''],
    });
  }
  addAnswer(): any{
    this.subQuestions().push(this.newSubQuestion());
  }
  getAnswers(idQuestion): any {
    this.status = true;
    console.log('get Answer by question id:');
    console.log(idQuestion);
    const reqQ = new SendDataService();
    reqQ.addAuthorization(this.token);
    reqQ.createServiceName('globalAnswersByQuestion');
    reqQ.addParams(idQuestion);
    this.toolService.readData(reqQ).subscribe(
      answers => {
        // console.log(answers);
        if (Array.isArray(answers) === true) {
          console.log(answers);
          this.answers = answers;
          // this.getAllAnswerByIdQuestion(res.answer_type_id);
          this.showEmptyMessage = false;
        } else {
          this.emptyMessage = 'Aun no tienes guardado nunguna respuesta';
          this.showEmptyMessage = true;
          console.log('No hay datos');
        }
        this.status = false;
      },
      error => {
        console.log(error);
      }
    );
  }
  openDialogEdit(idQuestion, opt, opt2): void {
    const dialogRef = this.dialog.open(EditComponent, {
      // padding: '0px',
      width: '60%',
      height: '80%',
      data: {
        id: idQuestion,
        option: opt,
        option2: opt2
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getQuestions();
    });
  }
  makeSubQuestions(idQuestion): void{
    // this.addAnswer();
    console.log(idQuestion);
    // tslint:disable-next-line: triple-equals
    if (this.checked == false) {
      this.showTestMessage = true;
      this.testMessage = 'Sub Preguntas';
    } else {
      this.showTestMessage = false;
      // this.testMessage = 'Se desclickeo el checkbox'
    }
  }
  showAnswers(): void {
    console.log(this.subQuestionForm.value);
  }
  removeAnswer(index): void{
    this.subQuestions().removeAt(index);
  }
  createView(idQuestion): void {
    console.log('Click Step with id:');
    console.log(idQuestion);
  }
}
