import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContentRoutingModule } from './content-routing.module';
import { ContentComponent } from './content.component';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { OrganizationModule } from './organization/organization.module';
// import { ComponentsModule } from 'src/app/components/components.module';
import { BlockComponent } from './organization/components/block/block.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { AngularMaterialModule } from 'src/app/shared/material-angular/angular-material.module';

@NgModule({
  declarations: [ContentComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ContentRoutingModule,
    OrganizationModule,
    // ComponentsModule,
    SharedModule,
    AngularMaterialModule
  ],
  entryComponents: [
    ContentComponent
  ],
  providers: [
    BlockComponent
  ]
})
export class ContentModule {}
