import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ToolRoutingModule } from './tool-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ToolComponent } from './tool.component';
// import { ComponentsModule } from 'src/app/components/components.module';
import { AngularMaterialModule } from 'src/app/shared/material-angular/angular-material.module';


@NgModule({
  declarations: [
    ToolComponent,
  ],
  imports: [
    CommonModule,
    ToolRoutingModule,
    ReactiveFormsModule,
    // ComponentsModule,
    AngularMaterialModule
  ],
  entryComponents: [
    ToolComponent
  ]
})
export class ToolModule { }
