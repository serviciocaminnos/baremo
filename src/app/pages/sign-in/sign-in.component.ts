import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthConstants } from 'src/app/core/config/auth-constanst';
import { SendDataService } from 'src/app/core/services/baremo/SendData.service';
import { StorageService } from 'src/app/core/services/baremo/storage.service';
import { ToolService } from 'src/app/core/services/baremo/tool.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  loginForm: FormGroup;
  userNoRegisterMessage = false;
  progressBar = false;
  logo = 'Baremo';
  constructor(private formBuilder: FormBuilder,
              private auth: ToolService,
              private router: Router,
              private storage: StorageService)
  {
    this.buildForm();
  }
  ngOnInit(): void {
  }

  get userName(): any {
    return this.loginForm.get('email_or_username');
  }

  get password(): any {
    return this.loginForm.get('password');
  }

  buildForm(): any {
    this.loginForm = this.formBuilder.group({
      email_or_username: ['', { validators: [Validators.required, Validators.minLength(5), Validators.maxLength(100)]}],
      password: ['', { validators : [Validators.required, Validators.minLength(8), Validators.maxLength(20)]}]
    });
  }

  login(): any {
    this.progressBar = true;
    const DataLogin: any = new SendDataService();
    DataLogin.createServiceName('login');
    DataLogin.addData(this.loginForm.value);
    this.auth.checkData(DataLogin).subscribe(
      res => {
        // tslint:disable-next-line: triple-equals
        if (res.status_code == '400'){
          this.progressBar = false;
        }else{
          // tslint:disable-next-line: triple-equals
          if (res.status_code == '500' ){
            this.userNoRegisterMessage = true;
            this.progressBar = false;
            this.loginForm.reset();
          }
          // tslint:disable-next-line: triple-equals
          if (res.status_code == '200'){
            this.loginForm.reset();
            this.progressBar = false;
            /* Guardar Datos del login en el Loca Storage*/
            this.storage.store(AuthConstants.AUTH, res.token);
            this.storage.store(AuthConstants.USER_ID, res.idUsuario);
            this.storage.store(AuthConstants.ROL, res.rol);
            localStorage.setItem('title', 'Herramientas');
            this.router.navigate(['/admin']).then(() => {
            });
          }
        }
      }, error => {
        console.log(error);
      }
    );
  }

}
