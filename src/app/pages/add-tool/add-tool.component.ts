import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthConstants } from 'src/app/core/config/auth-constanst';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from '../tool/content/organization/components/edit/edit.component';
import { ToolService } from 'src/app/core/services/baremo/tool.service';
import { StorageService } from 'src/app/core/services/baremo/storage.service';
import { SendDataService } from 'src/app/core/services/baremo/SendData.service';

@Component({
  selector: 'app-add-tool',
  templateUrl: './add-tool.component.html',
  styleUrls: ['./add-tool.component.css']
})
export class AddToolComponent implements OnInit {
  token;
  rol;
  idUser;
  toolForm: FormGroup;
  blockForm: FormGroup;
  measureForm: FormGroup;
  questionForm: FormGroup;
  progressStatus = false;
  alertMessage = false;
  addToolContent = false;
  deleteToolContent = false;
  constructor(private toolService: ToolService,
              private storage: StorageService,
              private formbuilder: FormBuilder,
              private router: Router,
              @Inject(MAT_DIALOG_DATA) public data: DialogData,
              private dialogRef: MatDialogRef<AddToolComponent>
              )
  {
    this.storage.get(AuthConstants.AUTH).then((token) => {
      this.token = token;
    });
    this.storage.get(AuthConstants.USER_ID).then((idUser) => {
      this.idUser = idUser;
      // tslint:disable-next-line: no-string-literal
      this.toolForm.controls['user_id'].setValue(this.idUser);
    });
    this.storage.get(AuthConstants.ROL).then((rol) => {
      this.rol = rol;
    });
    this.buildForm();
  }
  get name(): any {
    return this.toolForm.get('name');
  }
  ngOnInit(): void {
    this.verifyParam(this.data.option);
  }
  buildForm(): any {
    this.toolForm = this.formbuilder.group({
      user_id: [''],
      name: ['', [Validators.required, Validators.minLength(3)]],
      description: ['Herramienta de evaluación de empresas'],
    });
    this.blockForm = this.formbuilder.group({
      tool_id: [''],
      name: ['Bloque', [Validators.required, Validators.minLength(3)]],
      porcentage: ['0'],
      description: ['Bloque de la herramienta'],
    });
    this.measureForm = this.formbuilder.group({
      block_id: [''],
      name: ['Medida', [Validators.required, Validators.minLength(3)]],
      porcentage: ['0'],
      description: ['Medida del bloque'],
    });
    this.questionForm = this.formbuilder.group({
      measure_id: [''],
      name: ['Pregunta', [Validators.required, Validators.minLength(3)]],
      description: ['Pregunta de la Medida'],
      porcentage: ['0'],
    });
  }
  verifyParam(param): any {
    // tslint:disable-next-line: triple-equals
    if (param == 'add') {
      this.addToolContent = true;
    }else {
      this.deleteToolContent = true;
    }
  }
  addTool(): any {
    this.progressStatus = true;
    const toolRequest = new SendDataService();
    toolRequest.createServiceName('tool');
    toolRequest.addAuthorization(this.token);
    toolRequest.addResponseType('json');
    toolRequest.addData(this.toolForm.value);
    this.toolService.createData(toolRequest).subscribe(
      (toolData) => {
        // tslint:disable-next-line: triple-equals
        if (toolData.status_code == '200') {
          // tslint:disable-next-line: no-string-literal
          this.blockForm.controls['tool_id'].setValue(toolData.idTool);
          const blockRequest = new SendDataService();
          blockRequest.createServiceName('block');
          blockRequest.addAuthorization(this.token);
          blockRequest.addResponseType('json');
          blockRequest.addData(this.blockForm.value);
          this.toolService.createData(blockRequest).subscribe(
            (blockData) => {
              console.log(blockData);
              // tslint:disable-next-line: triple-equals
              if (blockData.status_code == '200') {
                // tslint:disable-next-line: no-string-literal
                this.measureForm.controls['block_id'].setValue(blockData.id);
                const measureRequest = new SendDataService();
                measureRequest.createServiceName('measure');
                measureRequest.addAuthorization(this.token);
                measureRequest.addResponseType('json');
                measureRequest.addData(this.measureForm.value);
                this.toolService.createData(measureRequest).subscribe(
                  (measureData) => {
                    // tslint:disable-next-line: triple-equals
                    if (measureData.status_code == '200') {
                      // tslint:disable-next-line: no-string-literal
                      this.questionForm.controls['measure_id'].setValue(measureData.id);
                      const questionRequest = new SendDataService();
                      questionRequest.createServiceName('question');
                      questionRequest.addAuthorization(this.token);
                      questionRequest.addResponseType('json');
                      questionRequest.addData(this.questionForm.value);
                      this.toolService.createData(questionRequest).subscribe(
                        (questionData) => {
                          // tslint:disable-next-line: triple-equals
                          if (questionData.status_code == '200') {
                            console.log(questionData);
                            this.alertMessage = true;
                            this.dialogRef.close();
                            window.location.reload();
                          }
                        },
                        (error) => {
                          console.log(error);
                        }
                      );
                    }
                  },
                  (error) => {
                    console.log(error);
                  }
                );
              }
            },
            (error) => {
              console.log(error);
            }
          );
        } else {
          console.log(toolData);
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }
  deleteTool(): any {
    this.progressStatus = true;
    console.log(this.data.id);
    const idTool = this.data.id;
    const serverRequestDelete = new SendDataService();
    serverRequestDelete.addAuthorization(this.token);
    serverRequestDelete.createServiceName('tool');
    serverRequestDelete.addParams(idTool);
    this.toolService.deleteData(serverRequestDelete).subscribe(
      res => {
        console.log(res);
        this.dialogRef.close();
        window.location.reload();
      }, error => {
        console.log(error);
      }

    );
  }
}
