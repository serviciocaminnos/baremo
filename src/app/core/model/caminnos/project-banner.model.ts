export interface ProjectBanner{
    id: string;
    image: string;
    title: string;
    description: string;
}