export interface Indicator {
  id: string;
  indicator: number;
  description: string;
  image: string;
  date: Date;
}