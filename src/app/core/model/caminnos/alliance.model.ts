export interface Alliance {
  id: string;
  title: string;
  description: string;
  image: string;
  date: Date;
}