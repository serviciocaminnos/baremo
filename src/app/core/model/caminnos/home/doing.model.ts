export interface Doing {
  id: string;
  title: string;
  subtitle: string;
  description: string;
}