export interface Ods {
  id: string;
  path_image: string;
  title: string;
  list: string[];
  images: string[];
}