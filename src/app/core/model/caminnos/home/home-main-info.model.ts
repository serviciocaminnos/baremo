export interface MainInfoHome {
  id: string;
  title: string;
  subtitle: string;
  description: string;
}
