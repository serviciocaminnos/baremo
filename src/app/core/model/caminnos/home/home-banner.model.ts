export interface BannerHome {
  id: string;
  image_path: string;
  title: string;
}
