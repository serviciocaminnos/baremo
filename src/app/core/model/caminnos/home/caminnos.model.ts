export interface Caminno {
  id: string;
  title: string;
  subtitle: string;
  description: string;
  image: string;
  order: number;
  components: string[];
}