export interface MainInfo {
  id: string;
  image: string;
  order: number;
}