export interface MoreInfo {
  id: string;
  url: string;
  title: string;
}