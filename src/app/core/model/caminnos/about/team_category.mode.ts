export interface TeamCategory {
  id: string;
  title: string;
}