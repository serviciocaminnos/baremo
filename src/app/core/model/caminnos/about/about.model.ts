export interface About {
  id: string;
  title: string;
  description: string;
  type: string;
}