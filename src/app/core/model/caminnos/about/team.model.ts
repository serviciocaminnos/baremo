export interface TeamProfile {
  id: string;
  name: string;
  work_position: string;
  image: string;
  biograpy: string;
}