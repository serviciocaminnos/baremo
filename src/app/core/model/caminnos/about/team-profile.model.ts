export interface AboutTeamProfile {
  id: string;
  work_position: string;
  name: string;
  image: string;
  biography: string;
  date: Date;
}