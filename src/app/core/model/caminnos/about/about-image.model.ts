export interface AboutImage {
  id: string;
  image: string;
}