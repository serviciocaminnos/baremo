export interface HomeOds {
    id: string;
    title: string;
    path_image: string;
    information: string[];
}
