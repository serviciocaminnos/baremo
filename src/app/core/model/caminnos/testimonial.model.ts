export interface Testimonial {
  id: string;
  name: string;
  description: string;
  user_image: string;
  main_image: string;
  date: Date | any;
}