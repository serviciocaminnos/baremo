export interface InformationHome {
  id: string;
  count: string;
  description: string;
}
