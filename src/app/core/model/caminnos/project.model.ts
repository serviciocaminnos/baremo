export interface Project {
  id: string;
  title: string;
  subtitle: string;
  description: string;
  organization: string;
  image: string;
  country: string;
  date: Date | any;
  icon_country: string[];
}
