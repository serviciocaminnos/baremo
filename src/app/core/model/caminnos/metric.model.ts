export interface Metric {
  id: string;
  title: string;
  image: string;
  description: string;
  date: Date;
}