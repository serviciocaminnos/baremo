export interface Blog {
  id: string;
  title: string;
  organization: string;
  image: string;
  description: string;
  category: string;
  url: string;
  date: Date | any;
  type: boolean;
}