export interface Countrie {
  id: string;
  name: string;
  alpha2: string;
  alpha3: string;
}