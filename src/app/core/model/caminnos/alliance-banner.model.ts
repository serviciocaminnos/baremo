export interface AllianceBanner{
    id: string;
    image: string;
    description: string;
}