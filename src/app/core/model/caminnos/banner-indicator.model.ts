export interface BannerIndicator {
  id: string;
  image: string;
}