export interface Award {
  id: string;
  title: string;
  image: string;
  description: string;
  date: Date;
}