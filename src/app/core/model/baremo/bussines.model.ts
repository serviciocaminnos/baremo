export interface BussinesData {
    id: number;
    business_name: string;
    kind_of_society: string;
    trade_registration: string;
    nit: string;
    date: string;
    legal_address: string;
    direction: string;
    contact_person: string;
    contact_number: string;
    contact_email: string;
    created_at: string;
    updated_at: string;
}
