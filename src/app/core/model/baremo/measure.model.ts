export interface Measure {
    id: number;
    block_id: number;
    name: string;
    description: string;
    position: number;
    porcentage: number;
    created_at: string;
    updated_at: string;
}
