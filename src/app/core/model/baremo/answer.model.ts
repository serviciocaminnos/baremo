export interface Answer {
    id: number;
    id_question: number;
    porcentage: number;
    name: string;
    need: string;
    sub_answer: string;
    created_at: string;
    updated_at: string;
}
