export interface Question {
    id: number;
    measure_id: number;
    answer_type_id: number;
    name: string;
    description: string;
    position: number;
    porcentage: number;
    created_at: string;
    updated_at: string;
}
