export interface User {
    id: number;
    company_id: string;
    name: string;
    lastname: string;
    username: string;
    email: string;
    email_verified_at: string;
    // lastLogin: string;
    position: string;
    user_image: string;
    rol: number;
    created_at: string;
    updated_at: string;
}
