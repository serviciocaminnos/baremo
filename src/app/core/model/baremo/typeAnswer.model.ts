import { Answer } from './answer.model';

export interface TypeAnswer {
    name: number;
    answer: Answer;
}
