export interface Block {
    id: number;
    tool_id: number;
    name: string;
    porcentage: number;
    description: string;
    position: string;
    created_at: string;
    updated_at: string;
}
