export interface Crud {
    idData: number;
    typeData: string;
    crud: string;
    porcentage: number;
}
