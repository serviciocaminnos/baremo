export interface Tool {
    id: number;
    name: string;
    description: string;
    url: string;
    created_at: string;
    updated_at: string;
}
