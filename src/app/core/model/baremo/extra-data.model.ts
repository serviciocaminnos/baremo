export interface ExtraDataBussines {
    id: number;
    contact_id: number;
    data_name: string;
    data: string;
    type: string;
    url: string;
    created_at: string;
    updated_at: string;
}
