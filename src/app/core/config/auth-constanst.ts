export class AuthConstants {
  public static readonly AUTH = 'authToken';
  public static readonly USER_ID = 'userId';
  public static readonly ROL = 'rol';
  public static readonly ID_TOOL = 'tool';
  public static readonly ID_BOCK = 'block';
  public static readonly MEASURE_POSITION = 'measure_position';
  public static readonly BLOCK_POSITION = 'block_position';
  public static readonly NAV_INDEX = 'nav_index';
  public static readonly ID_COMPANY = 'company';
}
