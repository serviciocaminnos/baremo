import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SendDataService {
Authorization:String='';
params:any='';
data:any='';
responseType:any='json';
ServiceName:String='';
  constructor() { 
    
  }
  createServiceName(Url:String){
    this.ServiceName=Url;
  }
  addAuthorization(token:String){
    return this.Authorization=token;
  }
  addParams(Params:any){
    return this.params=Params;
  }
  addData(Data:any){
    return this.data=Data;
  }
  addResponseType(Type:String){
    this.responseType=Type;
  }
  

}
