import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  constructor(private httpClient: HttpClient) {}
  get(data: any): any {
    let addurl = '';
    if (data.params != '') {
      addurl = `/${data.params}`;
    }
    const headers = new HttpHeaders({
      Authorization: 'Bearer ' + data.Authorization,
    });
    const options = { headers: headers, withCredintials: false };
    const url = environment.apiUrl + data.ServiceName + addurl;
    return this.httpClient.get(url, options);
  }
  put(data: any): any {
    let addurl = '';
    if (data.params != '') {
      addurl = `/${data.params}`;
    }
    let tipo: string = data.responseType;
    let token = data.Authorization;
    let TypeResponse;
    /*  Definition of data Type Response */
    if (tipo == 'text') {
      TypeResponse = 'text' as const;
    } else if (tipo == 'json') {
      TypeResponse = 'json' as const;
    } else {
      TypeResponse = 'json' as const;
    }
    /*ejecution of Http Client */
    const headers = new HttpHeaders({
      Authorization: 'Bearer ' + token,
      'Content-Type': 'application/json',
    });
    const options = {
      headers: headers,
      withCredintials: false,
      responseType: TypeResponse,
    };
    const url = environment.apiUrl + data.ServiceName + addurl;
    return this.httpClient.put(url, JSON.stringify(data.data), options);
  }

  post(data: any): any {
    let tipo: string = data.responseType;
    let TypeResponse;
    /*  Definition of data Type Response */
    if (tipo == 'text') {
      TypeResponse = 'text' as const;
    } else if (tipo == 'json') {
      TypeResponse = 'json' as const;
    } else {
      TypeResponse = 'json' as const;
    }
    /*ejecution of Http Client */
    const headers = new HttpHeaders({
      Authorization: 'Bearer ' + data.Authorization,
      'Content-Type': 'application/json',
    });
    const options = {
      headers: headers,
      withCredintials: false,
      responseType: TypeResponse,
    };
    const url = environment.apiUrl + data.ServiceName;
    return this.httpClient.post(url, JSON.stringify(data.data), options);
  }
  delete(data: any): any {
    let addurl = '';
    if (data.params != '') {
      addurl = `/${data.params}`;
    }
    let tipo: string = data.responseType;
    let token = data.Authorization;
    let TypeResponse;
    /*  Definition of data Type Response */
    if (tipo == 'text') {
      TypeResponse = 'text' as const;
    } else if (tipo == 'json') {
      TypeResponse = 'json' as const;
    } else {
      TypeResponse = 'json' as const;
    }
    /*ejecution of Http Client */
    const headers = new HttpHeaders({
      Authorization: 'Bearer ' + token,
      'Content-Type': 'application/json',
    });
    const options = {
      headers: headers,
      withCredintials: false,
      responseType: TypeResponse,
    };
    const url = environment.apiUrl + data.ServiceName + addurl;
    return this.httpClient.delete(url, options);
  }
}
