import { Injectable } from '@angular/core';

import { SendDataService } from './SendData.service';
import { StorageService } from './storage.service';
import { ToolService } from './tool.service';
import { Router } from '@angular/router';
import { AuthConstants } from '../../config/auth-constanst';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  token;
  constructor(
    private localStore: StorageService,
    private tool: ToolService,
    private router: Router
  ) {
    this.localStore.get(AuthConstants.AUTH).then((value) => {
      this.token = value;
    });
  }
  logout(): any {
    console.log(this.token);
    const deleteToken = new SendDataService();
    deleteToken.createServiceName('logout');
    deleteToken.addAuthorization(this.token);
    deleteToken.addData({ token: this.token });
    this.tool.checkData(deleteToken).subscribe((res) => {
      console.log(res);
      this.localStore.clear();
      this.router.navigate(['/login']).then(() => {
        window.location.reload();
      });
    });
  }
}
