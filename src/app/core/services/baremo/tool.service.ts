import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
@Injectable({
  providedIn: 'root',
})
export class ToolService {
  constructor(private http: HttpService, private httpClient: HttpClient) {}
  /* Function To read the data from the Database */
  readData(data: any): Observable<any> {
    return this.http.get(data);
  }
  /*Function To Verify Database Data */
  checkData(data: any): Observable<any> {
    return this.http.post(data);
  }
  /*Function To create elements in the Database */
  createData(data: any): Observable<any> {
    return this.http.post(data);
  }
  /*Function To Update elements in the Database */
  updateData(data: any): Observable<any> {
    return this.http.put(data);
  }
  deleteData(data: any): Observable<any> {
    return this.http.delete(data);
  }
}
