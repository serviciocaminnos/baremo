import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataBridgeService {
  private objectSource = new BehaviorSubject<{}>({});
  private secondObjectSource = new BehaviorSubject<{}>({});
  private listSource = new BehaviorSubject<any[]>([]);

  $getObjectSource = this.objectSource.asObservable();
  $getSecondObjectSource = this.secondObjectSource.asObservable();
  $getListSource = this.listSource.asObservable();

  constructor() { }

  sendObjectSource(data: any): any {
    this.objectSource.next(data);
  }
  sendSecondObjectSource(data: any): any {
    this.secondObjectSource.next(data);
  }
  sendListSource(list: any[]): any {
    this.listSource.next(list);
  }
}
