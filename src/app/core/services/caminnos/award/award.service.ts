import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Award } from '@core/model/caminnos/award.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AwardService {

  constructor(
    private fireStoreService: AngularFirestore,
  ) { }


  getAwards(): Observable<Award[]> {
    return this.fireStoreService.collection(
      'award',
      ref => ref.orderBy('date', 'desc')
    )
    .valueChanges({ idField: 'id' })
    .pipe(
      map(
        (res: Award[]) => {
          return res;
        }
      )
    );
  }

  editAward(data: Award, id: string) {
    return this.fireStoreService.collection('award')
    .doc(id)
    .update(data)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }

  deleteAward(data: Award) {
    return this.fireStoreService.collection('award')
    .doc(data.id)
    .delete()
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }

  addAward(data: Award) {
    return this.fireStoreService.collection('award')
    .add(data)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }
}
