import { TestBed } from '@angular/core/testing';

import { ProjectCaminnosService } from './project-caminnos.service';

describe('ProjectCaminnosService', () => {
  let service: ProjectCaminnosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProjectCaminnosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
