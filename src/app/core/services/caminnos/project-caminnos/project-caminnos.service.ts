import { ProjectBanner } from './../../../model/caminnos/project-banner.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Countrie } from '@core/model/caminnos/countrie.model';
import { Project } from '@core/model/caminnos/project.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProjectCaminnosService {

  constructor(
    private fireStoreService: AngularFirestore,
    private httpClient: HttpClient,
  ) { }


  getProjects(): Observable<any> {
    return this.fireStoreService.collection(
      'projects',
      ref => ref.orderBy('date', 'desc')
    )
    .valueChanges({ idField: 'id' })
    .pipe(
      map(
        (res: Project[]) => {
          return res;
        }
      )
    );
  }

  getCountries(): Observable<Countrie[]> {
    return this.httpClient.get<Countrie[]>('assets/json/world.json');
  }

  editProject(data: Project, id: string) {
    return this.fireStoreService.collection('projects')
    .doc(id)
    .update(data)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }

  deleteProject(data: Project) {
    return this.fireStoreService.collection('projects')
    .doc(data.id)
    .delete()
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }

  addProject(data: Project) {
    return this.fireStoreService.collection('projects')
    .add(data)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }

  getInformationBanner(): Observable<ProjectBanner[]>
  {
    return this.fireStoreService.collection<ProjectBanner>('projectsBanner').valueChanges({ idField: 'id' })
    .pipe(
      map(
        (res: ProjectBanner[])=>{
          return res;
        }
      )
    )
  }

  editProjectBanner(data: ProjectBanner, id: string) {
    return this.fireStoreService.collection('projectsBanner')
    .doc(id)
    .update(data)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }
}
