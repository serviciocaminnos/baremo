import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Caminno } from '@core/model/caminnos/home/caminnos.model';
import { Doing } from '@core/model/caminnos/home/doing.model';
import { MainInfoHome } from '@core/model/caminnos/home/home-main-info.model';
import { MainInfo } from '@core/model/caminnos/home/main-info.model';
import { MoreInfo } from '@core/model/caminnos/home/more-info.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BannerHome } from '@core/model/caminnos/home/home-banner.model';
import { InformationHome } from 'src/app/core/model/caminnos/home-information.model';
import { HomeOds } from 'src/app/core/model/caminnos/home-ods.model';
import { Ods } from '@core/model/caminnos/home/ods.model';

@Injectable({
  providedIn: 'root'
})
export class HomeCaminnosService {

  constructor(
    private fireStoreService: AngularFirestore
    ) {
  }

  getBanners(): Observable<any> {
    return this.fireStoreService.collection('home').valueChanges({ idField: 'id' })
    .pipe(
      map(
        (res: BannerHome[]) => {
          return res;
        }
      )
    );
  }
  getInformations(): Observable<any> {
    return this.fireStoreService.collection('imapact_home').valueChanges({ idField: 'id' })
    .pipe(
      map(
        (res: InformationHome[]) => {
          // console.log(res);
          return res;
        }
      )
    );
  }
  // tslint:disable-next-line: typedef
  editBanner(dataForm: BannerHome, id: string) {
    return this.fireStoreService.collection('home').doc(id).update(
      {
        title: dataForm.title,
        image_path: dataForm.image_path
      }
    )
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }
  // tslint:disable-next-line: typedef
  editInformationCaminno(dataForm: Caminno, id: string) {
    // console.log(dataForm);
    
    return this.fireStoreService.collection('home-info').doc('caminnos')
    .collection('caminnos-list')
    .doc(id)
    .update(dataForm)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }
  
  // tslint:disable-next-line: typedef
  editInformationOds(dataForm: Ods, id: string) {
    // console.log(dataForm);
    
    return this.fireStoreService
    .collection('ods')
    .doc(id)
    .update({
      title: dataForm.title,
      path_image: dataForm.path_image
    })
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }

  addOds(dataForm: Ods) {
    return this.fireStoreService
    .collection('ods')
    .add(dataForm)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }

  editInformationDoing(dataForm: Doing, id: string) {
    // console.log(dataForm);
    
    return this.fireStoreService.collection('home-info').doc('doing')
    .collection('doing-list')
    .doc(id)
    .update(dataForm)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }

  editInformationMainInfo(dataForm: MainInfo, id: string) {
    // console.log(dataForm);
    
    return this.fireStoreService.collection('home-info').doc('main')
    .collection('banner-list')
    .doc(id)
    .update(dataForm)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }
  
  
  editInformation(dataForm: InformationHome, id: string) {
    return this.fireStoreService.collection('home-info').doc(id)
    .update(dataForm)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }

  getOds(): Observable<Ods[]> {
    return this.fireStoreService
    .collection('ods', ref => ref.orderBy('date', 'desc'))
    .valueChanges({ idField: 'id' })
    .pipe(
      map(
        (res: Ods[]) => {
          // console.log(res);
          return res;
        }
      )
    );
  }
  
  // tslint:disable-next-line: typedef
  editOds(dataForm: HomeOds, id: string) {
    return this.fireStoreService.collection('ods').doc(id).update(
      {
        title: dataForm.title,
        path_image: dataForm.path_image
      }
      )
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
      )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
      );
  }

  deleteOds(data: Ods) {
    return this.fireStoreService
    .collection('ods')
    .doc(data.id)
    .delete()
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
      )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
      );
  }
  
  getSectionHome(): Observable<any> {
    return this.fireStoreService.collection('home-info').valueChanges({ idField: 'id' })
    .pipe(
      map(
        (res: MainInfoHome[]) => {
          // console.log(res);
          return res;
        }
      )
    );
  }

  getSubCollection(id: string): Observable<any> {
    switch (id) {
      case 'main':
        return this.fireStoreService.collection('home-info').doc(id).collection('banner-list').valueChanges({ idField: 'id' })
        .pipe(
          map(
            (res: MainInfo[]) => {
              // console.log(res);
              return res;
            }
          )
        );
        break;
      case 'doing':
        return this.fireStoreService.collection('home-info').doc(id).collection('doing-list').valueChanges({ idField: 'id' })
        .pipe(
          map(
            (res: Doing[]) => {
              // console.log(res);
              return res;
            }
          )
        );
      break;
      case 'caminnos':
        return this.fireStoreService.collection('home-info').doc(id).collection('caminnos-list').valueChanges({ idField: 'id' })
        .pipe(
          map(
            (res: Caminno[]) => {
              // console.log(res);
              return res;
            }
          )
        );
        break;
      case 'more-info':
        return this.fireStoreService.collection('home-info').doc(id).collection('more-info-list').valueChanges({ idField: 'id' })
        .pipe(
          map(
            (res: MoreInfo[]) => {
              // console.log(res);
              return res;
            }
          )
        );
        break;
      case 'ods':
        return this.fireStoreService.collection('home-info').doc(id).collection('ods-list').valueChanges({ idField: 'id' })
        .pipe(
          map(
            (res: HomeOds[]) => {
              // console.log(res);
              return res;
            }
          )
        );
        break;
    
      default:
        break;
    }
  }

  getDocumentHome(id: string): Observable<any> {
    switch (id) {
      case 'main':
        return this.fireStoreService.collection('home-info').doc(id).valueChanges({ idField: 'id' })
        .pipe(
          map(
            (res: MainInfoHome) => {
              // console.log(res);
              return res;
            }
          )
        );
        break;
      case 'doing':
        return this.fireStoreService.collection('home-info').doc(id).valueChanges({ idField: 'id' })
        .pipe(
          map(
            (res: MainInfoHome) => {
              // console.log(res);
              return res;
            }
          )
        );
      break;
      case 'caminnos':
        // console.log('caminnos');
        
        return this.fireStoreService.collection('home-info').doc(id).valueChanges({ idField: 'id' })
        .pipe(
          map(
            (res: MainInfoHome) => {
              // console.log(res);
              return res;
            }
          )
        );
        break;
      case 'more-info':
        return this.fireStoreService.collection('home-info').doc(id).valueChanges({ idField: 'id' })
        .pipe(
          map(
            (res: MainInfoHome) => {
              // console.log(res);
              return res;
            }
          )
        );
        break;
      case 'ods':
        return this.fireStoreService.collection('home-info').doc(id).valueChanges({ idField: 'id' })
        .pipe(
          map(
            (res: MainInfoHome) => {
              // console.log(res);
              return res;
            }
          )
        );
        break;
    
      default:
        break;
    }
  }

  addVideo(data: MoreInfo): Promise<any> {
    return this.fireStoreService.collection('home-info').doc('more-info')
    .collection('more-info-list')
    .add(data)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }
  deleteVideo(data: MoreInfo): Promise<any> {
    return this.fireStoreService.collection('home-info').doc('more-info')
    .collection('more-info-list')
    .doc(data.id)
    .delete()
    .then(
      (res: any) => {
        return 'Eliminado Correctamente';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }

  editVideo(data: MoreInfo, id: string): Promise<any> {
    console.log(id);
    return this.fireStoreService.collection('home-info').doc('more-info')
    .collection('more-info-list')
    .doc(id)
    .update(data)
    .then(
      (res: any) => {
        return 'Editado Correctamente';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }
}
