import { TestBed } from '@angular/core/testing';

import { HomeCaminnosService } from './home-caminnos.service';

describe('HomeCaminnosService', () => {
  let service: HomeCaminnosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HomeCaminnosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
