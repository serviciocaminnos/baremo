import { TestBed } from '@angular/core/testing';

import { ImpactCaminnosService } from './impact-caminnos.service';

describe('ImpactCaminnosService', () => {
  let service: ImpactCaminnosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ImpactCaminnosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
