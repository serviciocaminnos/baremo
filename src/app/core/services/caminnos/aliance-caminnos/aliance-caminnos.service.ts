import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AllianceBanner } from '@core/model/caminnos/alliance-banner.model';
import { Alliance } from '@core/model/caminnos/alliance.model';
import { Organization } from '@core/model/caminnos/organization.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AlianceCaminnosService {

  constructor(
    private fireStoreService: AngularFirestore,
    private httpClient: HttpClient,
  ) { }


  getAlliances(): Observable<any> {
    return this.fireStoreService.collection(
      'alliance',
      ref => ref.orderBy('date', 'desc')
    )
    .valueChanges({ idField: 'id' })
    .pipe(
      map(
        (res: Alliance[]) => {
          return res;
        }
      )
    );
  }

  editAlliance(data: Alliance, id: string) {
    return this.fireStoreService.collection('alliance')
    .doc(id)
    .update(data)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }

  deleteAlliance(data: Alliance) {
    return this.fireStoreService.collection('alliance')
    .doc(data.id)
    .delete()
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }

  addAlliance(data: Alliance) {
    return this.fireStoreService.collection('alliance')
    .add(data)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }

  getAllianceBanner(): Observable<AllianceBanner[]>{
    return this.fireStoreService.collection<AllianceBanner>('allianceBanner').valueChanges({ idField: 'id'})
    .pipe(
      map(
        (res: AllianceBanner[])=>
        {
          return res;
        }
      )
    );
  }

  editAllianceBanner(data: AllianceBanner, id: string) {
    return this.fireStoreService.collection('allianceBanner')
    .doc(id)
    .update(data)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }

  getAllSponsors(): Observable<Organization[]>{
    return this.fireStoreService.collection<Organization>('allianceSponsors').valueChanges({idField: 'id'})
    .pipe(
      map(
        (res: Organization[]) => res as Organization[]
      )
    );
  }

  addOrganization(data: Organization) {
    return this.fireStoreService.collection('allianceSponsors')
    .add(data)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }

  editOrganization(data: Organization, id: string) {
    return this.fireStoreService.collection('allianceSponsors')
    .doc(id)
    .update(data)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }

  deleteOrganization(data: Organization) {
    return this.fireStoreService.collection('allianceSponsors')
    .doc(data.id)
    .delete()
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }
}
