import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Testimonial } from '@core/model/caminnos/testimonial.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TestimonialService {

  constructor(
    private fireStoreService: AngularFirestore,
    private httpClient: HttpClient,
  ) { }


  getTestimonial(): Observable<any> {
    return this.fireStoreService.collection(
      'testimonial',
      ref => ref.orderBy('date', 'desc')
    )
    .valueChanges({ idField: 'id' })
    .pipe(
      map(
        (res: Testimonial[]) => {
          return res;
        }
      )
    );
  }

  editTestimonial(data: Testimonial, id: string) {
    return this.fireStoreService.collection('testimonial')
    .doc(id)
    .update(data)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }

  deleteTestimonial(data: Testimonial) {
    return this.fireStoreService.collection('testimonial')
    .doc(data.id)
    .delete()
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }

  addTestimonial(data: Testimonial) {
    return this.fireStoreService.collection('testimonial')
    .add(data)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }
}
