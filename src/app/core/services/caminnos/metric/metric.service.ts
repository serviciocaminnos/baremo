import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { BannerIndicator } from '@core/model/caminnos/banner-indicator.model';
import { Indicator } from '@core/model/caminnos/indicator.model';
import { Metric } from '@core/model/caminnos/metric.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MetricService {

  constructor(
    private fireStoreService: AngularFirestore,
  ) { }


  getMetrics(): Observable<Metric[]> {
    return this.fireStoreService.collection(
      'metric',
      ref => ref.orderBy('date', 'desc')
    )
    .valueChanges({ idField: 'id' })
    .pipe(
      map(
        (res: Metric[]) => {
          return res;
        }
      )
    );
  }

  editMetric(data: Metric, id: string) {
    return this.fireStoreService.collection('metric')
    .doc(id)
    .update(data)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }

  deleteMetric(data: Metric) {
    return this.fireStoreService.collection('metric')
    .doc(data.id)
    .delete()
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }

  addMetric(data: Metric) {
    return this.fireStoreService.collection('metric')
    .add(data)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }

  getIndicators(): Observable<Indicator[]> {
    return this.fireStoreService.collection(
      'indicator',
      ref => ref.orderBy('date', 'desc')
    )
    .valueChanges({ idField: 'id' })
    .pipe(
      map(
        (res: Indicator[]) => {
          return res;
        }
      )
    );
  }

  editIndicator(data: Indicator, id: string) {
    return this.fireStoreService.collection('indicator')
    .doc(id)
    .update(data)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }

  deleteIndicator(data: Indicator) {
    return this.fireStoreService.collection('indicator')
    .doc(data.id)
    .delete()
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }

  addIndicator(data: Indicator) {
    return this.fireStoreService.collection('indicator')
    .add(data)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }


  getBanner(): Observable<BannerIndicator[]> {
    return this.fireStoreService.collection(
      'indicator-banner',
    )
    .valueChanges({ idField: 'id' })
    .pipe(
      map(
        (res: BannerIndicator[]) => {
          return res;
        }
      )
    );
  }

  editBanner(data: BannerIndicator, id: string) {
    return this.fireStoreService.collection('indicator-banner')
    .doc(id)
    .update(data)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }
}
