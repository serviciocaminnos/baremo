import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AboutImage } from '@core/model/caminnos/about/about-image.model';
import { AboutTeam } from '@core/model/caminnos/about/about-team.model';
import { About } from '@core/model/caminnos/about/about.model';
import { AboutTeamProfile } from '@core/model/caminnos/about/team-profile.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AboutUsService {
  constructor(
    private fireStoreService: AngularFirestore,
  ) { }


  getAboutUs(): Observable<About[]> {
    return this.fireStoreService.collection(
      'about',
    )
    .valueChanges({ idField: 'id' })
    .pipe(
      map(
        (res: About[]) => {
          return res;
        }
      )
    );
  }

  editAboutUs(data: About, id: string) {
    return this.fireStoreService.collection('about')
    .doc(id)
    .update(data)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }

  deleteAboutUs(data: About) {
    return this.fireStoreService.collection('about')
    .doc(data.id)
    .delete()
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }

  getAboutImage(): Observable<AboutImage[]> {
    return this.fireStoreService.collection(
      'about-image',
    )
    .valueChanges({ idField: 'id' })
    .pipe(
      map(
        (res: AboutImage[]) => {
          return res;
        }
      )
    );
  }

  editAboutImage(data: AboutImage, id: string) {
    return this.fireStoreService.collection('about-image')
    .doc(id)
    .update(data)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }

  getAboutTeam(): Observable<AboutTeam[]> {
    return this.fireStoreService.collection(
      'team',
    )
    .valueChanges({ idField: 'id' })
    .pipe(
      map(
        (res: AboutTeam[]) => {
          return res;
        }
      )
    );
  }

  editAboutTeam(data: AboutTeam, id: string) {
    return this.fireStoreService.collection('team')
    .doc(id)
    .update(data)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }
  addAboutTeam(data: AboutTeam) {
    return this.fireStoreService.collection('team')
    .add(data)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }

  deleteAboutTeam(data: AboutTeam) {
    return this.fireStoreService.collection('team')
    .doc(data.id)
    .delete()
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }

  getAboutTeamProfile(team: AboutTeam): Observable<AboutTeamProfile[]> {
    return this.fireStoreService.collection(
      'team',
    )
    .doc(team.id)
    .collection(
      'team-profile',
      ref => ref.orderBy('date', 'desc')
    )
    .valueChanges({ idField: 'id' })
    .pipe(
      map(
        (res: AboutTeamProfile[]) => {
          return res;
        }
      )
    );
  }

  addProfile(data: AboutTeamProfile, id: string) {
    return this.fireStoreService.collection('team')
    .doc(id)
    .collection('team-profile')
    .add(data)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }
  editProfile(data: AboutTeamProfile, team: AboutTeam) {
    // console.log(data, team);
    return this.fireStoreService.collection('team')
    .doc(team.id)
    .collection('team-profile')
    .doc(data.id)
    .update(data)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }
  deleteProfile(data: AboutTeamProfile, team: AboutTeam) {
    return this.fireStoreService.collection('team')
    .doc(team.id)
    .collection('team-profile')
    .doc(data.id)
    .delete()
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }
}
