import { TestBed } from '@angular/core/testing';

import { BlogCaminnosService } from './blog-caminnos.service';

describe('BlogCaminnosService', () => {
  let service: BlogCaminnosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BlogCaminnosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
