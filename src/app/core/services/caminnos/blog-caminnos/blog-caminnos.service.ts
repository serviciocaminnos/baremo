import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Blog } from '@core/model/caminnos/blog.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BlogCaminnosService {

  constructor(
    private fireStoreService: AngularFirestore,
  ) { }


  getBlogs(): Observable<any> {
    return this.fireStoreService.collection(
      'blog',
      ref => ref.orderBy('date', 'desc')
    )
    .valueChanges({ idField: 'id' })
    .pipe(
      map(
        (res: Blog[]) => {
          return res;
        }
      )
    );
  }

  editBlog(data: Blog, id: string) {
    return this.fireStoreService.collection('blog')
    .doc(id)
    .update(data)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }

  deleteBlog(data: Blog) {
    return this.fireStoreService.collection('blog')
    .doc(data.id)
    .delete()
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }

  addBlog(data: Blog) {
    return this.fireStoreService.collection('blog')
    .add(data)
    .then(
      (res: any) => {
        return 'Cambios guardados';
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
        return 'Sucedio un error';
      }
    );
  }
}
