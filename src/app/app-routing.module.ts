import { SignInComponent } from './pages/sign-in/sign-in.component';
import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@core/guard/auth/auth.guard';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';

const routes: Routes = [

  { path: '', pathMatch: 'full', redirectTo: 'login' },

  { path: 'login', component: SignInComponent },
  {
    path: 'tool',
    loadChildren: () => import('./pages/tool/tool.module').then( m => m.ToolModule)
  },
  {
    path: 'admin',
    canActivate: [AuthGuard],
    loadChildren: () => import('./module/admin/admin.module').then( m => m.AdminModule)
  },

  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes, {
      useHash: true
    })
  ],
  exports: [ RouterModule ]
})

export class AppRoutingModule { }
