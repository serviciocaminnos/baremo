import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './components/layout/layout.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'organization',
        pathMatch: 'full'
      },
      {
        path: 'organization',
        loadChildren: () => import('./components/organization/organization.module').then((m) => m.OrganizationModule)
      },
      {
        path: 'evaluation',
        loadChildren: () => import('./components/evaluation/evaluation.module').then((m) => m.EvaluationModule)
      },
      {
        path: 'permises',
        loadChildren: () => import('./components/permises/permises.module').then((m) => m.PermisesModule)
      },
      {
        path: 'config',
        loadChildren: () => import('./components/config/config.module').then((m) => m.ConfigModule)
      },
      {
        path: 'business',
        loadChildren: () => import('./components/business/business.module').then((m) => m.BusinessModule)
      },
      {
        path: 'result',
        loadChildren: () => import('./components/result/result.module').then((m) => m.ResultModule)
      },
    ]
  },
  {
    path: 'caminnos',
    loadChildren: () => import('../caminnos/caminnos.module').then((m) => m.CaminnosModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
