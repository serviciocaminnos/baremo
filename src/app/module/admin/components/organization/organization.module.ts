import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrganizationRoutingModule } from './organization-routing.module';
import { ToolComponent } from './components/tool/tool.component';
import { AngularMaterialModule } from 'src/app/shared/material-angular/angular-material.module';
import { ToolContentContainer } from './container/tool-content/tool-content.container';


@NgModule({
  declarations: [
    ToolComponent,
    ToolContentContainer,
  ],
  imports: [
    CommonModule,
    OrganizationRoutingModule,
    AngularMaterialModule
  ],
  exports: [
    ToolContentContainer
  ]
})
export class OrganizationModule { }
