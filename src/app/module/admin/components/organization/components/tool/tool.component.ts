import { Component, OnInit } from '@angular/core';
import { AuthConstants } from 'src/app/core/config/auth-constanst';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RemoveComponent } from 'src/app/shared/components/remove/remove.component';
import { MatDialog } from '@angular/material/dialog';
import { ToolService } from 'src/app/core/services/baremo/tool.service';
import { StorageService } from 'src/app/core/services/baremo/storage.service';
import { SendDataService } from 'src/app/core/services/baremo/SendData.service';

@Component({
  selector: 'app-tool',
  templateUrl: './tool.component.html',
  styleUrls: ['./tool.component.css']
})
export class ToolComponent implements OnInit {
  token: string;
  rol: number;
  rolHidden = false;
  tools: any[];
  status: boolean;
  imageEmpty = false ;
  idUser: number;
  toolForm: FormGroup;
  blockForm: FormGroup;
  measureForm: FormGroup;
  questionForm: FormGroup;
  constructor(
    private toolService: ToolService,
    private storage: StorageService,
    private router: Router,
    private formbuilder: FormBuilder,
    private dialog: MatDialog,
  ) {
    this.storage.get(AuthConstants.AUTH).then((token) => {
      this.token = token;
    });
    this.storage.get(AuthConstants.ROL).then((rol) => {
      this.rol = rol;
    });
    this.storage.get(AuthConstants.USER_ID).then((idUser) => {
      this.idUser = idUser;
      // tslint:disable-next-line: no-string-literal
      this.toolForm.controls['user_id'].setValue(this.idUser);
    });
    this.buildForm();
  }
  buildForm(): any {
    this.toolForm = this.formbuilder.group({
      user_id: [''],
      name: ['Nueva Herramienta'],
      description: ['Herramienta de evaluación de empresas'],
      // tslint:disable-next-line: max-line-length
      url: ['https://static.vecteezy.com/system/resources/previews/000/171/284/original/free-hand-drawn-vector-nightscape-illustration.jpg'],
    });
    this.blockForm = this.formbuilder.group({
      tool_id: [''],
      name: ['Bloque', [Validators.required, Validators.minLength(3)]],
      porcentage: ['0'],
      description: ['Bloque de la herramienta'],
    });
    this.measureForm = this.formbuilder.group({
      block_id: [''],
      name: ['Medida', [Validators.required, Validators.minLength(3)]],
      porcentage: ['0'],
      description: ['Medida del bloque'],
    });
    this.questionForm = this.formbuilder.group({
      measure_id: [''],
      name: ['Pregunta', [Validators.required, Validators.minLength(3)]],
      description: ['Pregunta de la Medida'],
      porcentage: ['0'],
    });
  }
  ngOnInit(): void {

    this.listTool();
    // tslint:disable-next-line: triple-equals
    if (this.rol == 1) {
      this.rolHidden = true;
    }
  }
  listTool(): any {
    this.status = true;
    const petition = new SendDataService();
    petition.createServiceName('tool');
    petition.addAuthorization(this.token);
    this.toolService.readData(petition).subscribe(
      (res) => {
        if (res[0] != null){
          this.status = false;
          this.tools = res;
          this.imageEmpty = false;
          // console.log(this.tools);
        }else {
          this.imageEmpty = true;
          this.status = false;
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }
  addTool(): any {
    this.status = true;
    const toolRequest = new SendDataService();
    toolRequest.createServiceName('tool');
    toolRequest.addAuthorization(this.token);
    toolRequest.addResponseType('json');
    toolRequest.addData(this.toolForm.value);
    this.toolService.createData(toolRequest).subscribe(
      (toolData) => {
        // tslint:disable-next-line: triple-equals
        if (toolData.status_code == '200') {
          // tslint:disable-next-line: no-string-literal
          this.blockForm.controls['tool_id'].setValue(toolData.idTool);
          const blockRequest = new SendDataService();
          blockRequest.createServiceName('block');
          blockRequest.addAuthorization(this.token);
          blockRequest.addResponseType('json');
          blockRequest.addData(this.blockForm.value);
          this.toolService.createData(blockRequest).subscribe(
            (blockData) => {
              // console.log(blockData);
              // tslint:disable-next-line: triple-equals
              if (blockData.status_code == '200') {
                // tslint:disable-next-line: no-string-literal
                this.measureForm.controls['block_id'].setValue(blockData.id);
                const measureRequest = new SendDataService();
                measureRequest.createServiceName('measure');
                measureRequest.addAuthorization(this.token);
                measureRequest.addResponseType('json');
                measureRequest.addData(this.measureForm.value);
                this.toolService.createData(measureRequest).subscribe(
                  (measureData) => {
                    // tslint:disable-next-line: triple-equals
                    if (measureData.status_code == '200') {
                      // tslint:disable-next-line: no-string-literal
                      this.questionForm.controls['measure_id'].setValue(measureData.id);
                      const questionRequest = new SendDataService();
                      questionRequest.createServiceName('question');
                      questionRequest.addAuthorization(this.token);
                      questionRequest.addResponseType('json');
                      questionRequest.addData(this.questionForm.value);
                      this.toolService.createData(questionRequest).subscribe(
                        (questionData) => {
                          // tslint:disable-next-line: triple-equals
                          if (questionData.status_code == '200') {
                            // console.log(questionData);
                            this.status = true;
                            this.listTool();
                          }
                        },
                        (error) => {
                          console.log(error);
                        }
                      );
                    }
                  },
                  (error) => {
                    console.log(error);
                  }
                );
              }
            },
            (error) => {
              console.log(error);
            }
          );
        } else {
          console.log(toolData);
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
