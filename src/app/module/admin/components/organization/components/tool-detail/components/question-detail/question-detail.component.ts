import { Component, OnInit, Input, Output,  EventEmitter } from '@angular/core';
import { Answer } from 'src/app/core/model/baremo/answer.model';
import { RemoveComponent } from 'src/app/shared/components/remove/remove.component';
import { MatDialog } from '@angular/material/dialog';
import { EditComponent } from 'src/app/shared/components/edit/edit.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthConstants } from 'src/app/core/config/auth-constanst';
import { Question } from 'src/app/core/model/baremo/question.model';
import { StorageService } from 'src/app/core/services/baremo/storage.service';
import { ToolService } from 'src/app/core/services/baremo/tool.service';
import { SendDataService } from 'src/app/core/services/baremo/SendData.service';

@Component({
  selector: 'app-question-detail',
  templateUrl: './question-detail.component.html',
  styleUrls: ['./question-detail.component.css']
})
export class QuestionDetailComponent implements OnInit {
  @Input() question: Question;
  @Input() steper: any;
  @Output() newEditEvent = new EventEmitter<string>();
  answers: Answer[];
  token: string;
  emptyMessage: string;
  showEmptyMessage: boolean;
  state: boolean;
  constructor(
    private dialog: MatDialog,
    private storage: StorageService,
    private snackBar: MatSnackBar,
    private toolService: ToolService,

  ) {

  }
  ngOnInit(): void {
    this.storage.get(AuthConstants.AUTH).then((token: string) => {
      this.token = token;
      this.verifyAnswerType();
    });
  }
  deleteQuestion(): void {
    console.log(this.question.id);
    const dialogRef = this.dialog.open(RemoveComponent, {
      width: '550px',
      data: {
        idData: this.question.id,
        crud: 'remove',
        typeData: 'question'
      },
      panelClass: 'custom-dialog-container'

    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result === '200') {
        this.newEditEvent.emit('200');
      }
     });
  }
  editeQuestion(): void {
    console.log(this.question.id);
    const dialogRef = this.dialog.open(EditComponent, {
      width: '1000px',
      height: '500px',
      data: {
        idData: this.question.id,
        crud: 'edit',
        typeData: 'question',
        porcentage: this.question.porcentage
      },
      panelClass: 'custom-dialog-container'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === '200') {
        this.newEditEvent.emit('200');
      }
     });
  }
  verifyAnswerType(): void {
    const reqQ = new SendDataService();
    reqQ.addAuthorization(this.token);
    reqQ.createServiceName('globalAnswersByQuestion');
    reqQ.addParams(this.question.id);
    this.toolService.readData(reqQ).subscribe(
      res => {
        if (Array.isArray(res) === true) {
          // console.log(res);
          this.answers = res;
          // this.getAllAnswerByIdQuestion(res.answer_type_id);
          this.showEmptyMessage = false;
        } else {
          // console.log(res);
          this.emptyMessage = 'Aun no tienes guardado nunguna respuesta';
          this.showEmptyMessage = true;
        }
        this.state = false;
      },
      error => {
        console.log(error);
      }
    );
  }
}
