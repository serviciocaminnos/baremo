import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ToolDatailRoutingModule } from './tool-datail-routing.module';
import { ToolDetailComponent } from './components/detail/tool-detail.component';
import { AngularMaterialModule } from 'src/app/shared/material-angular/angular-material.module';
import { BlockDetailComponent } from './components/block-detail/block-detail.component';
import { MeasureDetailComponent } from './components/measure-detail/measure-detail.component';
import { QuestionDetailComponent } from './components/question-detail/question-detail.component';


@NgModule({
  declarations: [
    ToolDetailComponent,
    BlockDetailComponent,
    MeasureDetailComponent,
    QuestionDetailComponent
  ],
  imports: [
    CommonModule,
    ToolDatailRoutingModule,
    AngularMaterialModule
  ],
  exports: [
    MeasureDetailComponent,
    QuestionDetailComponent
  ]
})
export class ToolDatailModule { }
