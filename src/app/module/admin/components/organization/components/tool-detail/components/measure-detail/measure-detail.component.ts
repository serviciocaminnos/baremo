import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthConstants } from 'src/app/core/config/auth-constanst';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { RemoveComponent } from 'src/app/shared/components/remove/remove.component';
import { EditComponent } from 'src/app/shared/components/edit/edit.component';
import { Measure } from 'src/app/core/model/baremo/measure.model';
import { Question } from 'src/app/core/model/baremo/question.model';
import { StorageService } from 'src/app/core/services/baremo/storage.service';
import { ToolService } from 'src/app/core/services/baremo/tool.service';
import { SendDataService } from 'src/app/core/services/baremo/SendData.service';

@Component({
  selector: 'app-measure-detail',
  templateUrl: './measure-detail.component.html',
  styleUrls: ['./measure-detail.component.css']
})
export class MeasureDetailComponent implements OnInit {
  @Input() measure: Measure;
  @Input() steper: any;
  @Output() newEditEvent = new EventEmitter<string>();
  questions: Question[];
  toolId: number;
  token: string;
  questionForm: FormGroup;
  state: boolean;
  constructor(
    private storage: StorageService,
    private toolService: ToolService,
    private formbuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
  ) {
    this.storage.get(AuthConstants.ID_TOOL).then((toolId: number) => {
      this.toolId = toolId;
      // console.log(this.toolId);
    });
    this.buildForm();
  }
  buildForm(): any{
    this.questionForm = this.formbuilder.group({
      measure_id: [''],
      name: ['Pregunta'],
      description: ['Pregunta de la Medida'],
      porcentage: ['0'],
    });
  }

  ngOnInit(): void {
    this.storage.get(AuthConstants.AUTH).then((token: string) => {
      this.token = token;
      this.getQuestions();
    });
  }
  ngOnChange(): void {
    this.storage.get(AuthConstants.AUTH).then((token: string) => {
      this.token = token;
      this.getQuestions();
    });  }
  getQuestions(): void {
    this.state = true;
    const request = new SendDataService();
    request.addAuthorization(this.token);
    request.createServiceName('getQuestionOfMeasure');
    request.addParams(this.measure.id);
    this.toolService.readData(request).subscribe(
      (question: Question[]) => {
        // console.log(question);
        this.questions = question;
        this.state = false;
        // this.getMeasures(this.block.id);
      },
      (error: any) => {
        console.log(error);
      }
    );
  }
  addNewQuestion(): any {
    this.state = true;
    this.questionForm.controls.measure_id.setValue(this.measure.id);
    const questionRequest = new SendDataService();
    questionRequest.createServiceName('question');
    questionRequest.addAuthorization(this.token);
    questionRequest.addData(this.questionForm.value);
    this.toolService.createData(questionRequest).subscribe(
      (questionData) => {
        // tslint:disable-next-line: triple-equals
        if (questionData.status_code == '200') {
          // console.log(questionData);
          this.state = false;
          this.getQuestions();
          this.openSnackBar('Nueva pregunta añadida con éxito');
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }
  deleteMeasure(): void {
    console.log(this.measure.id);
    const dialogRef = this.dialog.open(RemoveComponent, {
      width: '550px',
      data: {
        idData: this.measure.id,
        crud: 'remove',
        typeData: 'measure'
      },
      panelClass: 'custom-dialog-container'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === '200') {
        this.newEditEvent.emit('200');
      }

     });
  }
  editeMeasure(): void {
    console.log(this.measure.id);
    const dialogRef = this.dialog.open(EditComponent, {
      width: '500px',
      height: '400px',
      data: {
        idData: this.measure.id,
        crud: 'remove',
        typeData: 'measure'
      },
      panelClass: 'custom-dialog-container'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === '200') {
        this.newEditEvent.emit('200');
      }
     });
  }
  openSnackBar(message: string): void {
    this.snackBar.open(message, 'Done' , {
      duration: 3000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }
}
