import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlockDetailComponent } from './components/block-detail/block-detail.component';
import { ToolDetailComponent } from './components/detail/tool-detail.component';

const routes: Routes = [
  {
    path: '',
    component: ToolDetailComponent,
    children: [
      { path: 'block', component: BlockDetailComponent },
      { path: ':position', component: BlockDetailComponent },
      { path: '', redirectTo: 'block'},
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ToolDatailRoutingModule { }
