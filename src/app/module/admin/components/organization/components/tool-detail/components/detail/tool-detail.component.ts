import { Component, OnInit, OnChanges } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { AuthConstants } from 'src/app/core/config/auth-constanst';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RemoveComponent } from 'src/app/shared/components/remove/remove.component';
import { EditComponent } from 'src/app/shared/components/edit/edit.component';
import { MatDialog } from '@angular/material/dialog';
import { Block } from 'src/app/core/model/baremo/block.model';
import { Tool } from 'src/app/core/model/baremo/tool.model';
import { ToolService } from 'src/app/core/services/baremo/tool.service';
import { StorageService } from 'src/app/core/services/baremo/storage.service';
import { SendDataService } from 'src/app/core/services/baremo/SendData.service';

@Component({
  selector: 'app-tool-detail',
  templateUrl: './tool-detail.component.html',
  styleUrls: ['./tool-detail.component.css']
})
export class ToolDetailComponent implements OnInit, OnChanges {
  toolId: number;
  blocks: Block[];
  tool: Tool;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
  token: string;
  blockForm: FormGroup;
  questionForm: FormGroup;
  measureForm: FormGroup;
  status: boolean;
  constructor(
    private breakpointObserver: BreakpointObserver,
    private router: ActivatedRoute,
    private toolService: ToolService,
    private storage: StorageService,
    private formbuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private route: Router,

  ) {
    this.router.params.subscribe((params: Params) => {
      this.toolId = params.idTool;
    });
    this.buildForm();
   }

  ngOnInit(): void {
    this.storage.get(AuthConstants.AUTH).then((token) => {
      this.token = token;
      this.getBlocks();
      this.getInformationTool();
    });
  }
  ngOnChanges(): void {
    this.storage.get(AuthConstants.AUTH).then((token) => {
      this.token = token;
      this.getBlocks();
      this.getInformationTool();
    });
  }
  getBlocks(): void {
    this.status = true;
    const petition = new SendDataService();
    petition.createServiceName('getBlockOfTool');
    petition.addAuthorization(this.token);
    petition.addParams(this.toolId);
    this.toolService.readData(petition).subscribe(
      (res: Block[]) => {
        this.blocks = res;
        this.status = false;
        // console.log(res);
        //
      },
      (error: any) => {
        console.log(error);
      }
    );

  }
  getInformationTool(): any {
    const serverRequest = new SendDataService();
    serverRequest.createServiceName('tool');
    serverRequest.addAuthorization(this.token);
    serverRequest.addParams(this.toolId);
    this.toolService.readData(serverRequest).subscribe(
      (res: Tool) => {
        this.tool = res;
        // console.log(res);
      }, error => {{
        console.log(error);
      }}
    );
  }
  buildForm(): any{
    this.blockForm = this.formbuilder.group({
      tool_id: [''],
      name: ['Bloque'],
      porcentage: ['0'],
      description: ['Bloque de la herramienta'],
    });
    this.measureForm = this.formbuilder.group({
      block_id: [''],
      name: ['Medida'],
      porcentage: ['0'],
      description: ['Medida del bloque'],
    });
    this.questionForm = this.formbuilder.group({
      measure_id: [''],
      name: ['Pregunta'],
      description: ['Pregunta de la Medida'],
      porcentage: ['0'],
    });
  }

  addBlock(): void {
    this.status = true;
    this.blockForm.controls.tool_id.setValue(this.toolId);
    // console.log(this.blockForm.value);
    const blockRequest = new SendDataService();
    blockRequest.createServiceName('block');
    blockRequest.addAuthorization(this.token);
    blockRequest.addData(this.blockForm.value);
    this.toolService.createData(blockRequest).subscribe(
      (blockData) => {
        // console.log(blockData);
        // tslint:disable-next-line: triple-equals
        if (blockData.status_code == '200') {
          // tslint:disable-next-line: no-string-literal
          this.measureForm.controls['block_id'].setValue(blockData.id);
          const measureRequest = new SendDataService();
          measureRequest.createServiceName('measure');
          measureRequest.addAuthorization(this.token);
          measureRequest.addData(this.measureForm.value);
          this.toolService.createData(measureRequest).subscribe(
            (measureData) => {
              // tslint:disable-next-line: triple-equals
              if (measureData.status_code == '200') {
                // tslint:disable-next-line: no-string-literal
                this.questionForm.controls['measure_id'].setValue(measureData.id);
                const questionRequest = new SendDataService();
                questionRequest.createServiceName('question');
                questionRequest.addAuthorization(this.token);
                questionRequest.addData(this.questionForm.value);
                this.toolService.createData(questionRequest).subscribe(
                  (questionData) => {
                    // tslint:disable-next-line: triple-equals
                    if (questionData.status_code == '200') {
                      this.status = false;
                      this.getBlocks();
                      this.openSnackBar('Nuevo bloque creado');
                    }
                  },
                  (error) => {
                    console.log(error);
                  }
                );
              }
            },
            (error) => {
              console.log(error);
            }
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  editBlock(id: string, tData: string): void {
    const dialogRef = this.dialog.open(EditComponent, {
      width: '1000px',
      height: '400px',
      data: {
        idData: id,
        crud: 'edit',
        typeData: tData
      },
      panelClass: 'custom-dialog-container'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.status_code === '200' && result.position === 1) {
        this.getBlocks();
        window.location.reload();
      }
      if (tData === 'block') {
        this.getBlocks();
      } else {
        this.getInformationTool();
      }
    });
  }
  deleteBlock(id: string, request: string, tData: string): void {
    const dialogRef = this.dialog.open(RemoveComponent, {
      width: '550px',
      data: {
        idData: id,
        crud: request,
        typeData: tData
      },
      panelClass: 'custom-dialog-container'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === '200') {
        this.getBlocks();
      }
    });
  }
  openSnackBar(message: string): void {
    this.snackBar.open(message, 'Done' , {
      duration: 3000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }
  back(): void {
    this.route.navigate(['/admin']);
  }
}
