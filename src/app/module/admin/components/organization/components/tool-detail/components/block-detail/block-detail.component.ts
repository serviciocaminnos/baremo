import { Component, OnInit, OnChanges, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AuthConstants } from 'src/app/core/config/auth-constanst';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { RemoveComponent } from 'src/app/shared/components/remove/remove.component';
import { EditComponent } from 'src/app/shared/components/edit/edit.component';
import { Block } from 'src/app/core/model/baremo/block.model';
import { Measure } from 'src/app/core/model/baremo/measure.model';
import { StorageService } from 'src/app/core/services/baremo/storage.service';
import { ToolService } from 'src/app/core/services/baremo/tool.service';
import { SendDataService } from 'src/app/core/services/baremo/SendData.service';

@Component({
  selector: 'app-block-detail',
  templateUrl: './block-detail.component.html',
  styleUrls: ['./block-detail.component.css']
})
export class BlockDetailComponent implements OnInit, OnChanges {
  blockPosition: any;
  toolId: number;
  token: string;
  block: Block;
  measures: Measure[];
  state: boolean;
  questionForm: FormGroup;
  measureForm: FormGroup;

  constructor(
    private router: ActivatedRoute,
    private storage: StorageService,
    private toolService: ToolService,
    private formbuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private route: Router,
  ) {
    // this.blockPostion = 1;
    this.storage.get(AuthConstants.ID_TOOL).then((toolId: number) => {
      this.toolId = toolId;
      // console.log(this.toolId);
    });
    this.storage.get(AuthConstants.AUTH).then((token: string) => {
      this.token = token;
      // console.log(this.toolId);
    });
    this.buildForm();

  }
  buildForm(): any{
    this.measureForm = this.formbuilder.group({
      block_id: [''],
      name: ['Medida'],
      porcentage: ['0'],
      description: ['Medida del bloque'],
    });
    this.questionForm = this.formbuilder.group({
      measure_id: [''],
      name: ['Pregunta'],
      description: ['Pregunta de la Medida'],
      porcentage: ['0'],
    });
  }

  ngOnInit(): void {
    // this.getMeasures();
    this.router.params.subscribe((params: Params) => {
      // console.log(params.position);
      if (!isNaN(params.position)) {
        // console.log(params.position);
        this.blockPosition = params.position;
      }else {
        this.blockPosition = 1;
      }
      this.getBlock();
    });
  }
  ngOnChanges(): void {
    this.router.params.subscribe((params: Params) => {
      // console.log(params.position);
      if (!isNaN(params.position)) {
        // console.log(params.position);
        this.blockPosition = params.position;
      }else {
        this.blockPosition = 1;
      }
      this.getBlock();
    });
  }
  getBlock(): void {
    this.state = true;
    const request = new SendDataService();
    request.addAuthorization(this.token);
    request.createServiceName('getBlockWithPosition');
    request.addParams(this.blockPosition + '/' + this.toolId);
    this.toolService.readData(request).subscribe(
      (block: Block) => {
        // console.log(block);
        this.block = block;
        this.getMeasures();
      },
      (error: any) => {
        console.log(error);
      }
    );
  }
  getMeasures(): void {
    const request = new SendDataService();
    request.addAuthorization(this.token);
    request.createServiceName('getMeasureOfBlock');
    request.addParams(this.block.id);
    this.toolService.readData(request).subscribe(
      (measure: Measure[]) => {
        // console.log(measure);
        this.measures = measure;
        this.state = false;
      },
      (error: any) => {
        console.log(error);
      }
    );
  }
  addNewMeasure(): any{
    this.state = true;
    this.measureForm.controls.block_id.setValue(this.block.id);
    const measureRequest = new SendDataService();
    measureRequest.createServiceName('measure');
    measureRequest.addAuthorization(this.token);
    measureRequest.addData(this.measureForm.value);
    this.toolService.createData(measureRequest).subscribe(
      (measureData) => {
        // tslint:disable-next-line: triple-equals
        if (measureData.status_code == '200') {
          // tslint:disable-next-line: no-string-literal
          this.questionForm.controls['measure_id'].setValue(measureData.id);
          const questionRequest = new SendDataService();
          questionRequest.createServiceName('question');
          questionRequest.addAuthorization(this.token);
          questionRequest.addResponseType('json');
          questionRequest.addData(this.questionForm.value);
          this.toolService.createData(questionRequest).subscribe(
            (questionData) => {
              // tslint:disable-next-line: triple-equals
              if (questionData.status_code == '200') {
                // console.log(questionData);
                this.state = false;
                this.getMeasures();
                this.openSnackBar('Nueva medidad añadida con éxito');
              }
            },
            (error) => {
              console.log(error);
            }
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }
  deleteBlock(): void {
    const dialogRef = this.dialog.open(RemoveComponent, {
      width: '550px',
      data: {
        idData: this.block.id,
        crud: 'remove',
        typeData: 'block'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.route.navigateByUrl('/admin/organization/' + this.toolId, { skipLocationChange: true }).then(() => {
        // window.location.reload();
        this.route.navigate(['/admin/organization/' + this.toolId + '/block']);
      });
    });
  }
  openSnackBar(message: string): void {
    this.snackBar.open(message, 'Done' , {
      duration: 3000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }
  editBlock(id: string, tData: string): void {
    const dialogRef = this.dialog.open(EditComponent, {
      width: '500px',
      height: '300px',
      data: {
        idData: id,
        crud: 'edit',
        typeData: tData
      },
      panelClass: 'custom-dialog-container'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === '200' || this.blockPosition === result.position) {
        window.location.reload();
      }
    });
  }
}
