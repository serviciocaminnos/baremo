import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AuthConstants } from '@core/config/auth-constanst';
import { Tool } from '@core/model/baremo/tool.model';
import { StorageService } from '@core/services/baremo/storage.service';
import { RemoveComponent } from '@shared/components/remove/remove.component';

@Component({
  selector: 'app-tool-content',
  templateUrl: './tool-content.container.html',
  styleUrls: ['./tool-content.container.css']
})
export class ToolContentContainer implements OnInit {
  @Input() tool: Tool;
  @Input() rolHidden: boolean;

  @Output() newEmitterTool: EventEmitter<Boolean> = new EventEmitter();
  constructor(
    private storgeService: StorageService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
  }

  saveToolId(toolId: number): any {
    this.storgeService.store(AuthConstants.ID_TOOL, toolId);
  }

  deleteTool(id: string, request: string, tData: string): void {
    const dialogRef = this.dialog.open(RemoveComponent, {
      width: '550px',
      data: {
        idData: id,
        crud: request,
        typeData: tData
      },
      panelClass: 'custom-dialog-container'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === '200') {
        this.newEmitterTool.emit(true);
      }
    });
  }
}
