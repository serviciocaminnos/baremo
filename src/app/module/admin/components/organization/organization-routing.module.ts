import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ToolComponent } from './components/tool/tool.component';

const routes: Routes = [
  { path: '', component: ToolComponent },
  { path: ':idTool', loadChildren: () => import('./components/tool-detail/tool-datail.module').then(m => m.ToolDatailModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrganizationRoutingModule { }
