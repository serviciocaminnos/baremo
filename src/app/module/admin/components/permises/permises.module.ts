import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PermisesRoutingModule } from './permises-routing.module';
import { HomeComponent } from './components/home/home.component';


@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    PermisesRoutingModule
  ]
})
export class PermisesModule { }
