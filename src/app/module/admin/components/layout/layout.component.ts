import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { AuthConstants } from 'src/app/core/config/auth-constanst';
import { User } from 'src/app/core/model/baremo/user.model';
import { StorageService } from 'src/app/core/services/baremo/storage.service';
import { ToolService } from 'src/app/core/services/baremo/tool.service';
import { AuthService } from 'src/app/core/services/baremo/auth.service';
import { SendDataService } from 'src/app/core/services/baremo/SendData.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
  idUser;
  token;
  hidden = false;
  user: User;
  logo = 'Baremo';
  title: string;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private storage: StorageService,
    private tool: ToolService,
    private session: AuthService
  ) {
    this.storage.get(AuthConstants.USER_ID).then(data => {
      this.idUser = data;
    });
    this.storage.get(AuthConstants.AUTH).then(data => {
      this.token = data;
    });
    if (localStorage.getItem('title')) {
      this.title = localStorage.getItem('title'); 
    }

  }
  ngOnInit(): any {
    this.getUser();
    this.verifyToken();
  }
  getUser(): any {
    console.log(this.token);
    const getUser = new SendDataService();
    getUser.createServiceName('user');
    getUser.addAuthorization(this.token);
    getUser.addParams(this.idUser);
    this.tool.readData(getUser).subscribe((res) => {
      // console.log(res);
      this.user = res;
    });
  }
  logout(): any {
    this.session.logout();
  }
  saveTitle(value: string): void {
    localStorage.setItem('title', value);
    this.title = value;
  }
  verifyToken(): any {
    this.storage.get(AuthConstants.AUTH).then(data => {
      this.token = data;
      if (this.token != null) {
        this.hidden = true;
      }else{
        this.hidden = false;
      }
    });
  }
}
