import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfigRoutingModule } from './config-routing.module';
import { HomeComponent } from './components/home/home.component';
import { SharedModule } from '@shared/shared.module';


@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    ConfigRoutingModule,
    SharedModule
  ]
})
export class ConfigModule { }
