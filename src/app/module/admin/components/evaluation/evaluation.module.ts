import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EvaluationRoutingModule } from './evaluation-routing.module';
import { HomeComponent } from './components/home/home.component';
import { AngularMaterialModule } from 'src/app/shared/material-angular/angular-material.module';
import { SharedModule } from '@shared/shared.module';


@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    EvaluationRoutingModule,
    AngularMaterialModule,
    SharedModule
  ]
})
export class EvaluationModule { }
