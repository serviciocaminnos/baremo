import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from 'src/app/pages/page-not-found/page-not-found.component';
import { LayoutComponent } from './components/layout/layout.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'home-caminnos' },
      {
        path: 'home-caminnos',
        loadChildren: () => import('./components/home/home.module').then(m => m.HomeModule)
      },
      {
        path: 'project-caminnos',
        loadChildren: () => import('./components/projects/projects.module').then(m => m.ProjectsModule)
      },
      {
        path: 'aliance-caminnos',
        loadChildren: () => import('./components/aliance/aliance.module').then(m => m.AlianceModule)
      },
      {
        path: 'impact-caminnos',
        loadChildren: () => import('./components/impact/impact.module').then(m => m.ImpactModule)
      },
      {
        path: 'award-caminnos',
        loadChildren: () => import('./components/award/award.module').then(m => m.AwardModule)
      },
      {
        path: 'blog-caminnos',
        loadChildren: () => import('./components/blog/blog.module').then(m => m.BlogModule)
      },
      { 
        path: 'about',
        loadChildren: () => import('./components/about/about.module').then(m => m.AboutModule)
      },
      { 
        path: 'about',
        loadChildren: () => import('./components/about/about.module').then(m => m.AboutModule)
      },
      { 
        path: 'organization', 
        loadChildren: () => import('./components/organization/organization.module').then(m => m.OrganizationModule)
      },
      { 
        path: 'testimonials', 
        loadChildren: () => import('./components/testimonials/testimonials.module').then(m => m.TestimonialsModule)
      },
     
    ],
  },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CaminnosRoutingModule { }
