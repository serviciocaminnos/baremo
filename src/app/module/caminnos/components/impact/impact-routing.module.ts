import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ImpactListComponent } from './impact-list/impact-list.component';

const routes: Routes = [
  { path: '', component: ImpactListComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ImpactRoutingModule { }
