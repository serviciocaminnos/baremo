import { Component, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BannerIndicator } from '@core/model/caminnos/banner-indicator.model';
import { Indicator } from '@core/model/caminnos/indicator.model';
import { Metric } from '@core/model/caminnos/metric.model';
import { MetricService } from '@core/services/caminnos/metric/metric.service';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-impact-list',
  templateUrl: './impact-list.component.html',
  styleUrls: ['./impact-list.component.css']
})
export class ImpactListComponent implements OnInit {

  state: boolean = false;
  metrics: Metric[];
  indicators: Indicator[];
  banner: BannerIndicator;

  statusUpload: boolean;
  imageSrc: Observable<string | null>;
  bannerForm: FormGroup;
  uploadPercent: Observable<number>;
  constructor(
    private metricService: MetricService,
    private storage: AngularFireStorage,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private formBuilder: FormBuilder,
  ) {
    this.buildForm();
  }

  ngOnInit(): void {
    this.getMetrics();
    this.getIndicators();
    this.getIndicatorsBanner();
  }

  buildForm(): void {
    this.bannerForm = this.formBuilder.group({
      id: [''],
      image: [''],
    });
  }

  getIndicatorsBanner(): void {
    this.state = true;
    this.metricService.getBanner().subscribe(
      (res: BannerIndicator[]) => {
        this.banner = res[0];
        this.state = false;
        this.bannerForm.patchValue(this.banner);
        // console.log(res);
      },
      (error: any) => {
        this.state = false;
        console.log(error);
      },
    );
  }
  

  getMetrics(): void {
    this.state = true;
    this.metricService.getMetrics().subscribe(
      (res: Metric[]) => {
        this.metrics = res;
        this.state = false;
        // console.log(res);
      },
      (error: any) => {
        this.state = false;
        console.log(error);
      },
    );
  }

  getIndicators(): void {
    this.state = true;
    this.metricService.getIndicators().subscribe(
      (res: Indicator[]) => {
        this.indicators = res;
        this.state = false;
        // console.log(res);
      },
      (error: any) => {
        this.state = false;
        console.log(error);
      },
    );
  }

  addMetric(): void {
    const metric: Metric = {
      id: '',
      title: 'Nueva Metrica',
      description: 'Reseña acerca de la metrica',
      image: 'https://scontent.fsrz1-2.fna.fbcdn.net/v/t1.6435-9/80206598_149659809777922_537181438135500800_n.jpg?_nc_cat=104&ccb=1-5&_nc_sid=973b4a&_nc_ohc=AI_9pvtwniYAX8AIj4J&_nc_ht=scontent.fsrz1-2.fna&oh=79e3e148c39c4b5984e1df488bc2e230&oe=61983F4E',
      date: new Date(),
    }
    this.metricService.addMetric(metric)
    .then(
      (res: any) => {
        this.state = false;
        // this.projects = res;
        Swal.fire({
          title: 'Agregado Correctamente',
          text: 'El nuevo premio se agregó correctamente',
          icon: 'success',
          showConfirmButton: false,
          timer: 2000
        })
        console.log(res);
      }
      )
    .catch(
      (error: any) => {
        this.state = false;
        console.log(error);
      },
    );
  }

  
  addIndicator(): void {
    const indicator: Indicator = {
      id: '',
      indicator: 10,
      description: 'Descripción acerca del indicador',
      image: 'https://scontent.fsrz1-2.fna.fbcdn.net/v/t1.6435-9/80206598_149659809777922_537181438135500800_n.jpg?_nc_cat=104&ccb=1-5&_nc_sid=973b4a&_nc_ohc=AI_9pvtwniYAX8AIj4J&_nc_ht=scontent.fsrz1-2.fna&oh=79e3e148c39c4b5984e1df488bc2e230&oe=61983F4E',
      date: new Date(),
    }
    this.metricService.addIndicator(indicator)
    .then(
      (res: any) => {
        this.state = false;
        // this.projects = res;
        Swal.fire({
          title: 'Agregado Correctamente',
          text: 'El nuevo indicador se agregó correctamente',
          icon: 'success',
          showConfirmButton: false,
          timer: 2000
        })
        console.log(res);
      }
      )
    .catch(
      (error: any) => {
        this.state = false;
        console.log(error);
      },
    );
  }

  saveChange(option: string): void {
    this.state = true;
    const banner: BannerIndicator = this.bannerForm.value;
    // console.log(data);
    this.metricService.editBanner(banner, this.banner.id)
    .then(
      (res: any) => {
        this.state = false;
      }
      )
      .catch(
        (error: any) => {
        console.log(error);
      }
    );
  }


  upload(event: any): void {
    this.statusUpload = true;
    const file = event.target.files[0];
    const dir = 'banner-indicator/' + this.banner.id;
    const fileRef = this.storage.ref(dir);
    const task = this.storage.upload(dir, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges()
    .pipe(
      finalize(() => {
        this.statusUpload = false;
        this.imageSrc = fileRef.getDownloadURL();
        this.openSnackBar();
        this.imageSrc.subscribe(url => {
          this.bannerForm.controls.image.setValue(url);
          // this.image = url;
          // console.log(this.image);
          this.saveChange('image');
          // console.log(this.bannerForm.value);
        });
      })
    ).subscribe(() => {
    });
    // console.log(dir);
  }
  
  openSnackBar(): void {
    this.snackBar.open('La imagen se subio correctamente', 'Done', {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }
}
