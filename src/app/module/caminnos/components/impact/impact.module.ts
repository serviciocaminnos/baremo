import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ImpactRoutingModule } from './impact-routing.module';
import { ImpactListComponent } from './impact-list/impact-list.component';
import { MetricContainer } from './container/metric/metric.container';
import { AngularMaterialModule } from '@material/angular-material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { IndicatorContainer } from './container/indicator/indicator.container';


@NgModule({
  declarations: [
    ImpactListComponent,
    MetricContainer,
    IndicatorContainer
  ],
  imports: [
    CommonModule,
    ImpactRoutingModule,
    AngularMaterialModule,
    ReactiveFormsModule
  ]
})
export class ImpactModule { }
