import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndicatorContainer } from './indicator.container';

describe('IndicatorContainer', () => {
  let component: IndicatorContainer;
  let fixture: ComponentFixture<IndicatorContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndicatorContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IndicatorContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
