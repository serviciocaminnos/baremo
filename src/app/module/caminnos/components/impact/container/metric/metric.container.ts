import { Component, Input, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Metric } from '@core/model/caminnos/metric.model';
import { MetricService } from '@core/services/caminnos/metric/metric.service';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-metric',
  templateUrl: './metric.container.html',
  styleUrls: ['./metric.container.css']
})
export class MetricContainer implements OnInit {

  @Input() metric: Metric | any;
  isTitle: boolean;
  isDescription: boolean;
  statusUpload: boolean;
  imageSrc: Observable<string | null>;
  metricForm: FormGroup;
  uploadPercent: Observable<number>;
  state: boolean = false;
  constructor(
    private storage: AngularFireStorage,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private formBuilder: FormBuilder,
    private metricService: MetricService,
  ) {
    this.buildForm();
  }
  
  ngOnInit(): void {
    this.metricForm.patchValue(this.metric);
    this.metricForm.controls.date.setValue(this.metric.date.toDate());
  }

  buildForm(): void {
    this.metricForm = this.formBuilder.group({
      id: [''],
      title: [''],
      description: [''],
      image: [''],
      date: [''],
    });
  }

  saveChange(option: string): void {
    this.state = true;
    this.showInput(option);
    const metric: Metric = this.metricForm.value;
    // console.log(data);
    this.metricService.editMetric(metric, this.metric.id)
    .then(
      (res: any) => {
        this.state = false;
      }
      )
      .catch(
        (error: any) => {
        console.log(error);
      }
    );
  }


  changeDate(): void {
    this.saveChange('date');
  }

  deleteMetric(): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: '¿Estas seguro de eliminar la metrica: ' + this.metric.title + '?',
      text: "No volveras a recuperarlo una vez eliminado",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.metricService.deleteMetric(this.metric)
        .then(
          (res: any) => {
            swalWithBootstrapButtons.fire(
              '¡Eliminado!',
              'La metrica se eliminó correctamente',
              'success'
            )
            this.deleteImage(this.metric.image);
          }
        ).catch(
          (error: any) => {
            swalWithBootstrapButtons.fire(
              'Error',
              'Verifica tu conexión a internet o vuelva a intentarlo',
              'error'
            )
          }
        )

      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'La metrica esta a salvo',
          'error'
        )
      }
    })

  }

  upload(event: any): void {
    this.statusUpload = true;
    const file = event.target.files[0];
    const dir = 'metric/' + this.metric.id;
    const fileRef = this.storage.ref(dir);
    const task = this.storage.upload(dir, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges()
    .pipe(
      finalize(() => {
        this.statusUpload = false;
        this.imageSrc = fileRef.getDownloadURL();
        this.openSnackBar();
        this.imageSrc.subscribe(url => {
          this.metricForm.controls.image.setValue(url);
          // this.image = url;
          // console.log(this.image);
          this.saveChange('image');
          // console.log(this.bannerForm.value);
        });
      })
    ).subscribe(() => {
    });
    // console.log(dir);
  }
  
  openSnackBar(): void {
    this.snackBar.open('La imagen se subio correctamente', 'Done', {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }

  showInput(option: string): void {
    switch (option) {
      case 'title':
        if (this.isTitle) {
          this.isTitle = false;
        } else {
          this.isTitle = true;
        }
        break;
      case 'description':
        if (this.isDescription) {
          this.isDescription = false;
        } else {
          this.isDescription = true;
        }
        break;
    
      default:
        this.isDescription = false;
        this.isTitle = false;
      break;
    }
  }

  deleteImage(url) {
    return this.storage.storage.refFromURL(url).delete();
  }
}
