import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MetricContainer } from './metric.container';

describe('MetricComponent', () => {
  let component: MetricContainer;
  let fixture: ComponentFixture<MetricContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MetricContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MetricContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
