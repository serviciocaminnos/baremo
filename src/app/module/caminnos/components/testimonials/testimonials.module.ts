import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TestimonialsRoutingModule } from './testimonials-routing.module';
import { TestimonialsComponent } from './testimonials.component';
import { SharedModule } from '@shared/shared.module';
import { AngularMaterialModule } from '@material/angular-material.module';
import { TestimonialComponent } from './container/testimonial/testimonial.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    TestimonialsComponent,
    TestimonialComponent
  ],
  imports: [
    CommonModule,
    TestimonialsRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    AngularMaterialModule
  ]
})
export class TestimonialsModule { }
