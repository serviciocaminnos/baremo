import { Component, OnInit } from '@angular/core';
import { Testimonial } from '@core/model/caminnos/testimonial.model';
import { TestimonialService } from '@core/services/caminnos/testimonial/testimonial.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-testimonials',
  templateUrl: './testimonials.component.html',
  styleUrls: ['./testimonials.component.css']
})
export class TestimonialsComponent implements OnInit {
  testimonials: Testimonial[];
  constructor(
    private testimonialService: TestimonialService
  ) { }

  ngOnInit(): void {
    this.getTestimonials();
  }
  
  getTestimonials(): void {
    this.testimonialService.getTestimonial().subscribe(
      (res: Testimonial[]) => {
        this.testimonials = res;
      },
      (error: any) => {
        console.log(error);
        
      }
    )
  }

  addTestimonial(): void {
    const testimonial: Testimonial = {
      id: '',
      name: 'Nombre del encuestado',
      description: 'Descripción del testimonio',
      main_image: 'https://scontent.fsrz1-2.fna.fbcdn.net/v/t1.6435-9/80206598_149659809777922_537181438135500800_n.jpg?_nc_cat=104&ccb=1-5&_nc_sid=973b4a&_nc_ohc=AI_9pvtwniYAX8AIj4J&_nc_ht=scontent.fsrz1-2.fna&oh=79e3e148c39c4b5984e1df488bc2e230&oe=61983F4E',
      user_image: 'https://scontent.fsrz1-2.fna.fbcdn.net/v/t1.6435-9/80206598_149659809777922_537181438135500800_n.jpg?_nc_cat=104&ccb=1-5&_nc_sid=973b4a&_nc_ohc=AI_9pvtwniYAX8AIj4J&_nc_ht=scontent.fsrz1-2.fna&oh=79e3e148c39c4b5984e1df488bc2e230&oe=61983F4E',
      date: new Date()
    };
    this.testimonialService.addTestimonial(testimonial)
    .then(
      (res: any) => {
        Swal.fire({
          icon: 'success',
          title: 'Agregado Correctamente',
          text: 'El testimonio se agrego correctamente',
          showConfirmButton: false,
          timer: 2000
        });
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
      }
    )
  }
  
}
