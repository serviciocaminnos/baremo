import { Component, Input, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Testimonial } from '@core/model/caminnos/testimonial.model';
import { TestimonialService } from '@core/services/caminnos/testimonial/testimonial.service';
import { finalize } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-testimonial',
  templateUrl: './testimonial.component.html',
  styleUrls: ['./testimonial.component.css']
})
export class TestimonialComponent implements OnInit {
  @Input() testimonial: Testimonial;
  isName: boolean;
  isDescription: boolean;

  statusUpload: boolean;
  imageSrc: any;
  testimonialForm: FormGroup;
  uploadPercent: any;
  state: boolean = false;

  constructor(
    private storage: AngularFireStorage,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private testimonialService: TestimonialService,
  ) {
    this.buildForm();
  }
  
  ngOnInit(): void {
    this.testimonialForm.patchValue(this.testimonial);
    this.testimonialForm.controls.date.setValue(this.testimonial.date.toDate());
  }

  buildForm(): void {
    this.testimonialForm = this.formBuilder.group({
      id: [''],
      name: [''],
      description: [''],
      main_image: [''],
      user_image: [''],
      date: [''],
    });
  }

  saveChange(option: string): void {
    this.state = true;
    this.showInput(option);
    const testimonial: Testimonial = this.testimonialForm.value;
    // console.log(profile);
    this.testimonialService.editTestimonial(testimonial, this.testimonial.id)
    .then(
      (res: any) => {
        this.state = false;
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
      }
    );
  }


  changeDate(): void {
    this.saveChange('date');
  }

  deleteAward(): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: '¿Estas seguro de eliminar el testimonio de : ' + this.testimonial.name + '?',
      text: "No volveras a recuperarlo una vez eliminado",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Eliminar!',
      cancelButtonText: 'Cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.testimonialService.deleteTestimonial(this.testimonial)
        .then(
          (res: any) => {
            swalWithBootstrapButtons.fire(
              '¡Eliminado!',
              'El testimonio se eliminó correctamente',
              'success'
            )
            this.deleteImage(this.testimonial.main_image);
            this.deleteImage(this.testimonial.user_image);
          }
        ).catch(
          (error: any) => {
            swalWithBootstrapButtons.fire(
              'Error',
              'Verifica tu conexión a internet o vuelva a intentarlo',
              'error'
            )
          }
        )

      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'El testimonio esta a salvo',
          'error'
        )
      }
    })

  }

  upload(event: any, option: string): void {
    this.statusUpload = true;
    const file = event.target.files[0];
    let dir = '';
    if (option === 'user') {
      dir = 'testimonial-user/' + this.testimonial.id;
    }
    if (option === 'main') {
      dir = 'testimonial-image/' + this.testimonial.id;
    }
    const fileRef = this.storage.ref(dir);
    const task = this.storage.upload(dir, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges()
    .pipe(
      finalize(() => {
        this.statusUpload = false;
        this.imageSrc = fileRef.getDownloadURL();
        this.openSnackBar();
        this.imageSrc.subscribe(url => {
          if (option === 'user') {
            this.testimonialForm.controls.user_image.setValue(url);
          }
          if (option === 'main') {
            this.testimonialForm.controls.main_image.setValue(url);
          }
          // this.image = url;
          // console.log(this.image);
          this.saveChange('image');
          // console.log(this.bannerForm.value);
        });
      })
    ).subscribe(() => {
    });
    // console.log(dir);
  }
  
  openSnackBar(): void {
    this.snackBar.open('La imagen se subio correctamente', 'Done', {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }

  showInput(option: string): void {
    switch (option) {
      case 'name':
        if (this.isName) {
          this.isName = false;
        } else {
          this.isName = true;
        }
        break;
      case 'description':
        if (this.isDescription) {
          this.isDescription = false;
        } else {
          this.isDescription = true;
        }
        break;
      default:
        this.isName = false;
        this.isDescription = false;
      break;
    }
  }
  deleteImage(url) {
    return this.storage.storage.refFromURL(url).delete();
  }

}
