import { Component, OnInit } from '@angular/core';
import { Blog } from '@core/model/caminnos/blog.model';
import { BlogCaminnosService } from '@core/services/caminnos/blog-caminnos/blog-caminnos.service';
import Swal from 'sweetalert2';
export interface Filter {
  value: string;
  label: string;
}
@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.css']
})
export class BlogListComponent implements OnInit {
  state: boolean = false;
  blogs: Blog[];
  filterSelected: Filter = {
    value: '0',
    label: 'Ninguno'
  };

  filters: Filter[] = [
    {
      value: '0',
      label: 'Ninguno'
    },
    {
      value: '1',
      label: 'Articulos'
    },
    {
      value: '2',
      label: 'Webinars'
    },
    {
      value: '3',
      label: 'Entrevistas e Investigaciones'
    },
  ]

  blogFirst: Blog; 
  constructor(
    private blogService: BlogCaminnosService
  ) { }

  ngOnInit(): void {
    this.getBlogs();
  }

  getBlogs(): void {
    this.state = true;
    this.blogService.getBlogs().subscribe(
      (res: Blog[]) => {
        this.blogs = res;
        this.state = false;
        // console.log(res);
        this.blogFirst = this.blogs[0];
      },
      (error: any) => {
        this.state = false;
        console.log(error);
      },
    );
  }
  addBlog(): void {
    const blog: Blog = {
      id: '',
      title: 'Nueva Publicaciòn',
      category: '1',
      organization: 'Organizaciones aliadas',
      description: 'Reseña acerca de la publicaciòn',
      image: 'https://scontent.fsrz1-2.fna.fbcdn.net/v/t1.6435-9/80206598_149659809777922_537181438135500800_n.jpg?_nc_cat=104&ccb=1-5&_nc_sid=973b4a&_nc_ohc=AI_9pvtwniYAX8AIj4J&_nc_ht=scontent.fsrz1-2.fna&oh=79e3e148c39c4b5984e1df488bc2e230&oe=61983F4E',
      url: 'https://www.facebook.com/Caminnos/',
      date: new Date(),
      type: false
    }
    this.blogService.addBlog(blog)
    .then(
      (res: any) => {
        this.state = false;
        // this.projects = res;
        Swal.fire({
          title: 'Agregado Correctamente',
          text: 'La nueva alianza se agregó correctamente',
          icon: 'success',
          showConfirmButton: false,
          timer: 2000
        })
        console.log(res);
      }
      )
    .catch(
      (error: any) => {
        this.state = false;
        console.log(error);
      },
    );
  }

}
