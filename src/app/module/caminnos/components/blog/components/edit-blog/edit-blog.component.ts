import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Blog } from '@core/model/caminnos/blog.model';
import { BlogCaminnosService } from '@core/services/caminnos/blog-caminnos/blog-caminnos.service';

@Component({
  templateUrl: './edit-blog.component.html',
  styleUrls: ['./edit-blog.component.css']
})
export class EditBlogComponent implements OnInit {
  blogForm: FormGroup;

  state: boolean = false;

  constructor(
    private dialogRef: MatDialogRef<EditBlogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Blog,
    private formBuilder: FormBuilder,
    private blogService: BlogCaminnosService,
  ) {
    // console.log(this.data);
    this.buildForm();
  }

  ngOnInit(): void {
    this.blogForm.patchValue(this.data);
  }

  buildForm(): void {
    this.blogForm = this.formBuilder.group({
      id: [''],
      title: [''],
      organization: [''],
      description: [''],
      image: [''],
      url: [''],
      type: [''],
      date: [''],
    });
  }
  
  save(): void {
    this.state = true;
    const blog: Blog = this.blogForm.value;
    this.blogService.editBlog(blog, this.data.id)
    .then(
      (res: any) => {
        // console.log(res);
        this.dialogRef.close();
        this.state = false;
      }
      )
    .catch(
      (error: any) => {
        this.state = false;
        console.log(error);
      }
    );
  }
}
