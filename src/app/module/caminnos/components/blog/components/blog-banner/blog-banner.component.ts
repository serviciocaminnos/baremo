import { Component, Input, OnInit } from '@angular/core';
import { Blog } from '@core/model/caminnos/blog.model';

@Component({
  selector: 'app-blog-banner',
  templateUrl: './blog-banner.component.html',
  styleUrls: ['./blog-banner.component.css']
})
export class BlogBannerComponent implements OnInit {
  @Input() blog: Blog;
  constructor() { }

  ngOnInit(): void {
  }

}
