import { Component, Input, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Blog } from '@core/model/caminnos/blog.model';
import { BlogCaminnosService } from '@core/services/caminnos/blog-caminnos/blog-caminnos.service';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { EditBlogComponent } from '../../components/edit-blog/edit-blog.component';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.container.html',
  styleUrls: ['./blog.container.css']
})
export class BlogContainer implements OnInit {
  @Input() blog: Blog | any;
  isTitle: boolean;
  isDescription: boolean;
  statusUpload: boolean;
  imageSrc: Observable<string | null>;
  blogForm: FormGroup;
  uploadPercent: Observable<number>;
  state: boolean = false;
  constructor(
    private storage: AngularFireStorage,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private formBuilder: FormBuilder,
    private blogService: BlogCaminnosService,
  ) {
    this.buildForm();
  }
  
  ngOnInit(): void {
    this.blogForm.patchValue(this.blog);
    this.blogForm.controls.date.setValue(this.blog.date.toDate());
  }

  buildForm(): void {
    this.blogForm = this.formBuilder.group({
      id: [''],
      title: [''],
      organization: [''],
      description: [''],
      image: [''],
      url: [''],
      type: [''],
      date: [''],
    });
  }

  saveChange(option: string): void {
    this.state = true;
    this.showInput(option);
    const alliance: Blog = this.blogForm.value;
    // console.log(data);
    this.blogService.editBlog(alliance, this.blog.id)
    .then(
      (res: any) => {
        this.state = false;
      }
      )
      .catch(
        (error: any) => {
        console.log(error);
      }
    );
  }


  changeDate(): void {
    this.saveChange('date');
  }

  editBlog(): void {
    const dialogRef = this.dialog.open(EditBlogComponent, {
      width: '500px',
      height: '700px',
      panelClass: 'custom-dialog-container',
      data: this.blog
    });
  }
  
  deleteBlog(): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: '¿Estas seguro de eliminar la publicación: ' + this.blog.title + '?',
      text: "No volveras a recuperarlo una vez eliminado",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.blogService.deleteBlog(this.blog)
        .then(
          (res: any) => {
            swalWithBootstrapButtons.fire(
              '¡Eliminado!',
              'La publicación se eliminó correctamente',
              'success'
            )
            this.deleteImage(this.blog.image);
          }
        ).catch(
          (error: any) => {
            swalWithBootstrapButtons.fire(
              'Error',
              'Verifica tu conexión a internet o vuelva a intentarlo',
              'error'
            )
          }
        )

      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'La publicación esta a salvo',
          'error'
        )
      }
    })

  }

  upload(event: any): void {
    this.statusUpload = true;
    const file = event.target.files[0];
    const dir = 'alliance/' + this.blog.id;
    const fileRef = this.storage.ref(dir);
    const task = this.storage.upload(dir, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges()
    .pipe(
      finalize(() => {
        this.statusUpload = false;
        this.imageSrc = fileRef.getDownloadURL();
        this.openSnackBar();
        this.imageSrc.subscribe(url => {
          this.blogForm.controls.image.setValue(url);
          // this.image = url;
          // console.log(this.image);
          this.saveChange('image');
          // console.log(this.bannerForm.value);
        });
      })
    ).subscribe(() => {
    });
    // console.log(dir);
  }
  
  openSnackBar(): void {
    this.snackBar.open('La imagen se subio correctamente', 'Done', {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }

  showInput(option: string): void {
    switch (option) {
      case 'title':
        if (this.isTitle) {
          this.isTitle = false;
        } else {
          this.isTitle = true;
        }
        break;
      case 'description':
        if (this.isDescription) {
          this.isDescription = false;
        } else {
          this.isDescription = true;
        }
        break;
    
      default:
        this.isDescription = false;
        this.isTitle = false;
      break;
    }
  }

  deleteImage(url) {
    return this.storage.storage.refFromURL(url).delete();
  }
}
