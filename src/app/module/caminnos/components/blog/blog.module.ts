import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlogRoutingModule } from './blog-routing.module';
import { BlogListComponent } from './blog-list/blog-list.component';
import { AngularMaterialModule } from '@material/angular-material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { BlogContainer } from './container/blog/blog.container';
import { EditBlogComponent } from './components/edit-blog/edit-blog.component';
import { SharedModule } from '@shared/shared.module';
import { BlogBannerComponent } from './components/blog-banner/blog-banner.component';


@NgModule({
  declarations: [
    BlogListComponent,
    BlogContainer,
    EditBlogComponent,
    BlogBannerComponent
  ],
  imports: [
    CommonModule,
    BlogRoutingModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class BlogModule { }
