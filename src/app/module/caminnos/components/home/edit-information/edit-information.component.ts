import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Caminno } from '@core/model/caminnos/home/caminnos.model';
import { Doing } from '@core/model/caminnos/home/doing.model';
import { BannerHome } from '@core/model/caminnos/home/home-banner.model';
import { MainInfoHome } from '@core/model/caminnos/home/home-main-info.model';
import { MainInfo } from '@core/model/caminnos/home/main-info.model';
import { MoreInfo } from '@core/model/caminnos/home/more-info.model';
import { Ods } from '@core/model/caminnos/home/ods.model';
import { InformationHome } from 'src/app/core/model/caminnos/home-information.model';
import { HomeCaminnosService } from 'src/app/core/services/caminnos/home-caminnos/home-caminnos.service';

@Component({
  selector: 'app-edit-information',
  templateUrl: './edit-information.component.html',
  styleUrls: ['./edit-information.component.css']
})
export class EditInformationComponent implements OnInit, OnChanges {
  @Input() dataHome: MainInfoHome;
  @Input() dataSubCollection: Caminno[] | Doing[] | MoreInfo[] | MainInfo[] | BannerHome[];
  informationForm: FormGroup;
  information: InformationHome;

  status: boolean;

  isTitle: boolean = false;
  isSubtitle: boolean = false;
  isDescription: boolean = false;

  state: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private homeService: HomeCaminnosService
  ) {
    this.buildForm();
    
  }
  
  ngOnInit(): void {
    // console.log(this.dataSubCollection);
    // this.informationForm.patchValue(this.dataHome);
  }
  
  ngOnChanges(): void {
    this.informationForm.patchValue(this.dataHome);
  }

  buildForm(): void {
    this.informationForm = this.formBuilder.group({
      id: [''],
      title: ['', [Validators.required]],
      subtitle: ['', [Validators.required]],
      description: ['', [Validators.required]]
    });
  }

  saveChange(option: string): void {
    this.state = true;
    this.showInput(option);
    this.homeService.editInformation(this.informationForm.value, this.dataHome.id)
    .then(
      (res: any) => {
        this.state = false;
      }
      )
      .catch(
        (error: any) => {
        console.log(error);
      }
    );
  }


  showInput(option: string): void {
    switch (option) {
      case 'title':
        if (this.isTitle) {
          this.isTitle = false;
        } else {
          this.isTitle = true;
        }
        break;
      case 'subtitle':
        if (this.isSubtitle) {
          this.isSubtitle = false;
        } else {
          this.isSubtitle = true;
        }
        break;
      case 'description':
        if (this.isDescription) {
          this.isDescription = false;
        } else {
          this.isDescription = true;
        }
        break;
    
      default:
        this.isDescription = false;
        this.isSubtitle = false;
        this.isTitle = false;
        break;
    }
  }

  addVideo(): void {
    this.state = true;
    const data: MoreInfo = {
      id: '',
      title: 'Titulo',
      url: 'https://www.youtube.com/embed/bx-YT01rpfA',
    };
    this.homeService.addVideo(data)
    .then(
      (res: any) => {
        this.state = false;
      }
    ).catch(
      (error: any) => {

      }
    )
  }

  addOds(): void {
    this.state = true;
    const data: Ods | any = {
      title: 'Nuevo ODS',
      path_image: 'https://firebasestorage.googleapis.com/v0/b/baremo-640e1.appspot.com/o/ods%2FODS%20CAMINNOS1.jpg?alt=media&token=cae6b75f-f25b-4593-bd4d-f0b75c88376d',
      date: new Date()
    };
    this.homeService.addOds(data)
    .then(
      (res: any) => {
        this.state = false;
      }
    ).catch(
        (error: any) => {
        this.state = false;
      }
    )
  }
}
