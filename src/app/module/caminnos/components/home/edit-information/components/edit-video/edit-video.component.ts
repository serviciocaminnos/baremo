import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MoreInfo } from '@core/model/caminnos/home/more-info.model';
import { HomeCaminnosService } from '@core/services/caminnos/home-caminnos/home-caminnos.service';
import Swal from 'sweetalert2';

@Component({
  templateUrl: './edit-video.component.html',
  styleUrls: ['./edit-video.component.css']
})
export class EditVideoComponent implements OnInit {
  videoForm: FormGroup;
  state: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private homeService: HomeCaminnosService,
    private dialogRef: MatDialogRef<EditVideoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: MoreInfo
  ) {
    this.buildForm();
  }
  
  buildForm(): void {
    this.videoForm = this.formBuilder.group({
      id: [''],
      url: [''],
      title: ['']
    });
  }
  
  ngOnInit(): void {
    this.videoForm.setValue(this.data);
  }

  save(): void {
    this.state = true;
    console.log(this.videoForm.value);
    
    this.homeService.editVideo(this.videoForm.value, this.data.id)
    .then(
      (res: any) => {
        this.state = false;
        Swal.fire(
          'Datos Guardados',
          'La url del video se guardo correctamente',
          'success',
        )
        this.dialogRef.close();
      }
    ).catch(
      (error: any) => {
        Swal.fire(
          'Error',
          'Verifica tu conexión a internet o vuelva a intentarlo',
          'error'
        )
      }
    )
  }

}
