import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Caminno } from '@core/model/caminnos/home/caminnos.model';
import { Doing } from '@core/model/caminnos/home/doing.model';
import { BannerHome } from '@core/model/caminnos/home/home-banner.model';
import { MainInfo } from '@core/model/caminnos/home/main-info.model';
import { MoreInfo } from '@core/model/caminnos/home/more-info.model';
import { HomeCaminnosService } from '@core/services/caminnos/home-caminnos/home-caminnos.service';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-caminno',
  templateUrl: './caminno.container.html',
  styleUrls: ['./caminno.container.css']
})
export class CaminnoContainer implements OnInit, OnChanges {
  @Input() caminno: Caminno;
  isTitle: boolean = false;
  isDescription: boolean = false;
  caminnosForm: FormGroup;

  statusUpload: boolean = false;
  imageSrc: Observable<string | null>;
  uploadPercent: Observable<number>;
  
  image: string;

  componentsList: any[];

  state: boolean = false;
  constructor(
    private homeService: HomeCaminnosService,
    private formBuilder: FormBuilder,
    private storage: AngularFireStorage,
    private snackBar: MatSnackBar,
    private router: Router,
  ) {
    this.buildForm();
  }
  
  ngOnInit(): void {
    this.buildArrayData();
    this.caminnosForm.patchValue(this.caminno);
    this.image = this.caminno.image;

    // this.caminnosForm.controls.components.setValue(this.caminno.components);
    // console.log( 'Form Caminnos:', this.caminnosForm.value);
  }
  
  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    // this.caminnosForm.controls.image.valueChanges.subscribe(url => {
    //   this.image = url;
    // });
    // console.log(this.image);
    
  }

  buildForm(): void {
    this.caminnosForm = this.formBuilder.group({
      id: [''],
      title: [''],
      image: [''],
      order: [''],
      subtitle: [''],
      description: [''],
      components: this.formBuilder.array([])
      // components: [this.caminno.components]
    })
  }

  buildArrayData(): void {
    const array: string[] = this.caminno.components;
    if (this.caminno.id === 'caminnos') {
      array.forEach(element => {
        const form: FormGroup = this.formBuilder.group({
          component: [element]
        });
        this.components.push(form);
      });
    }
  }

  saveChange(option: string): void {
    this.state = true;
    this.showInput(option);
    const caminno = this.caminnosForm.value;
    const data: Caminno = {
      id: caminno.id,
      title: caminno.title,
      subtitle: caminno.subtitle,
      description: caminno.description,
      image: caminno.image,
      components: this.reBuildComponents(),
      order: caminno.order
    } 
    // console.log(data);
    this.homeService.editInformationCaminno(data, this.caminno.id)
    .then(
      (res: any) => {
        this.state = false;
      }
      )
      .catch(
        (error: any) => {
        console.log(error);
      }
    );
  }

  reBuildComponents(): string[] {
    const components = this.components.value;
    let res: string[] = [];
    if (this.caminno.id === 'caminnos') {
      components.forEach(element => {
        res.push(element.component);
      });
    }
    return res;
  }

  upload(event: any): void {
    this.statusUpload = true;
    const file = event.target.files[0];
    const dir = 'caminnos/' + 'caminno' + this.caminno.order;
    const fileRef = this.storage.ref(dir);
    const task = this.storage.upload(dir, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges()
    .pipe(
      finalize(() => {
        this.statusUpload = false;
        this.imageSrc = fileRef.getDownloadURL();
        this.openSnackBar();
        this.imageSrc.subscribe(url => {
          this.caminnosForm.controls.image.setValue(url);
          // this.image = url;
          // console.log(this.image);
          this.saveChange('image');
          // console.log(this.bannerForm.value);
        });
      })
    ).subscribe(() => {
    });
    // console.log(dir);
  }
  
  openSnackBar(): void {
    this.snackBar.open('La imagen se subio correctamente', 'Done', {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }

  showInput(option: string): void {
    switch (option) {
      case 'title':
        if (this.isTitle) {
          this.isTitle = false;
        } else {
          this.isTitle = true;
        }
        break;
      case 'description':
        if (this.isDescription) {
          this.isDescription = false;
        } else {
          this.isDescription = true;
        }
        break;
    
      default:
        this.isDescription = false;
        this.isTitle = false;
      break;
    }
  }

  public get components(): FormArray {
    return this.caminnosForm.get('components') as FormArray;
  }
}
