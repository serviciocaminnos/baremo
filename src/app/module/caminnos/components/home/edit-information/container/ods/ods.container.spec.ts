import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OdsContainer } from './ods.container';

describe('OdsContainer', () => {
  let component: OdsContainer;
  let fixture: ComponentFixture<OdsContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OdsContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OdsContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
