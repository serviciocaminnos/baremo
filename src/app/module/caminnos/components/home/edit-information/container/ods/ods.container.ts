import { Component, Input, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Ods } from '@core/model/caminnos/home/ods.model';
import { HomeCaminnosService } from '@core/services/caminnos/home-caminnos/home-caminnos.service';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-ods',
  templateUrl: './ods.container.html',
  styleUrls: ['./ods.container.css']
})
export class OdsContainer implements OnInit {
  @Input() ods: Ods;
  isTitle: boolean = false;
  isDescription: boolean = false;
  odsForm: FormGroup;

  statusUpload: boolean = false;
  imageSrc: Observable<string | null>;
  uploadPercent: Observable<number>;
  
  image: string;

  componentsList: any[];

  state: boolean = false;
  constructor(
    private homeService: HomeCaminnosService,
    private formBuilder: FormBuilder,
    private storage: AngularFireStorage,
    private snackBar: MatSnackBar,
  ) {
    this.buildForm();
  }
  
  ngOnInit(): void {
    this.odsForm.patchValue({
      title: this.ods.title,
      path_image: this.ods.path_image
    });
    // console.log( 'Form Caminnos:', this.caminnosForm.value);
  }
  
  buildForm(): void {
    this.odsForm = this.formBuilder.group({
      id: [''],
      title: [''],
      path_image: [''],
    })
  }
  saveChange(option: string): void {
    this.state = true;
    this.showInput(option);
    const ods = this.odsForm.value;
    // console.log(data);
    this.homeService.editInformationOds(ods, this.ods.id)
    .then(
      (res: any) => {
        this.state = false;
      }
      )
      .catch(
        (error: any) => {
        console.log(error);
      }
    );
  }
  upload(event: any): void {
    this.statusUpload = true;
    const file = event.target.files[0];
    const dir = 'ods/' + this.ods.id;
    const fileRef = this.storage.ref(dir);
    const task = this.storage.upload(dir, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges()
    .pipe(
      finalize(() => {
        this.statusUpload = false;
        this.imageSrc = fileRef.getDownloadURL();
        this.openSnackBar();
        this.imageSrc.subscribe(url => {
          this.odsForm.controls.path_image.setValue(url);
          // this.image = url;
          // console.log(this.image);
          this.saveChange('image');
          // console.log(this.bannerForm.value);
        });
      })
    ).subscribe(() => {
    });
    // console.log(dir);
  }
  
  openSnackBar(): void {
    this.snackBar.open('La imagen se subio correctamente', 'Done', {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }

  showInput(option: string): void {
    switch (option) {
      case 'title':
        if (this.isTitle) {
          this.isTitle = false;
        } else {
          this.isTitle = true;
        }
        break;
      case 'description':
        if (this.isDescription) {
          this.isDescription = false;
        } else {
          this.isDescription = true;
        }
        break;
    
      default:
        this.isDescription = false;
        this.isTitle = false;
      break;
    }
  }

  
  deleteOds(): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: '¿Estas seguro de eliminar el ods?',
      text: "No volveras a recuperarlo una vez eliminado",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.homeService.deleteOds(this.ods)
        .then(
          (res: any) => {
            swalWithBootstrapButtons.fire(
              '¡Eliminado!',
              'El Ods se elimino correctamente',
              'success'
            )
          }
        ).catch(
          (error: any) => {
            swalWithBootstrapButtons.fire(
              'Error',
              'Verifica tu conexión a internet o vuelva a intentarlo',
              'error'
            )
          }
        )

      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'El Ods esta a salvo',
          'error'
        )
      }
    })

  }
}
