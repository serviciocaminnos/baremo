import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DoingContainer } from './doing.container';

describe('DoingContainer', () => {
  let component: DoingContainer;
  let fixture: ComponentFixture<DoingContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DoingContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DoingContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
