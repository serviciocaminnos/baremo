import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CaminnoContainer } from './caminno.container';

describe('CaminnoComponent', () => {
  let component: CaminnoContainer;
  let fixture: ComponentFixture<CaminnoContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CaminnoContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CaminnoContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
