import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainInfoContainer } from './main-info.container';

describe('MainInfoContainer', () => {
  let component: MainInfoContainer;
  let fixture: ComponentFixture<MainInfoContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainInfoContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainInfoContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
