import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MainInfo } from '@core/model/caminnos/home/main-info.model';
import { HomeCaminnosService } from '@core/services/caminnos/home-caminnos/home-caminnos.service';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-main-info',
  templateUrl: './main-info.container.html',
  styleUrls: ['./main-info.container.css']
})
export class MainInfoContainer implements OnInit {
  @Input() mainInfo: MainInfo;

  statusUpload: boolean = false;
  imageSrc: Observable<string | null>;
  uploadPercent: Observable<number>;
  state: boolean = false;
  
  imageForm: string;
  constructor(
    private formBuilder: FormBuilder,
    private storage: AngularFireStorage,
    private snackBar: MatSnackBar,
    private homeService: HomeCaminnosService,
  ) { }

  ngOnInit(): void {
    // console.log(this.mainInfo);
    // if (true) {
    // }

  }

  saveChange(option: string): void {
    this.state = true;
    // console.log(data);
    const data: MainInfo = {
      id: this.mainInfo.id,
      image: this.imageForm,
      order: this.mainInfo.order
    };
    this.homeService.editInformationMainInfo(data, this.mainInfo.id)
    .then(
      (res: any) => {
        this.state = false;
      }
      )
      .catch(
        (error: any) => {
        console.log(error);
      }
    );
  }

  upload(event: any): void {
    this.statusUpload = true;
    const file = event.target.files[0];
    const dir = 'banners/' + 'banner' + this.mainInfo.order + '.png';
    const fileRef = this.storage.ref(dir);
    const task = this.storage.upload(dir, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges()
    .pipe(
      finalize(() => {
        this.statusUpload = false;
        this.imageSrc = fileRef.getDownloadURL();
        this.openSnackBar();
        this.imageSrc.subscribe(url => {
          this.imageForm = url;
          // this.image = url;
          // console.log(this.image);
          this.saveChange('image');
          // console.log(this.bannerForm.value);
        });
      })
    ).subscribe(() => {
    });
    // console.log(dir);
  }
  
  openSnackBar(): void {
    this.snackBar.open('La imagen se subio correctamente', 'Done', {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }
}
