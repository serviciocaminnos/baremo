import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MoreInfo } from '@core/model/caminnos/home/more-info.model';
import { HomeCaminnosService } from '@core/services/caminnos/home-caminnos/home-caminnos.service';
import Swal from 'sweetalert2';
import { EditVideoComponent } from '../../components/edit-video/edit-video.component';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css']
})
export class VideoComponent implements OnInit {
  @Input() moreInfo: MoreInfo;
  constructor(
    private homeService: HomeCaminnosService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    // console.log(this.moreInfo);
  }
  
  editVideo(): void {
    const dialogRef = this.dialog.open( EditVideoComponent, {
      width: '500px',
      height: '250px',
      panelClass: 'custom-dialog-container',
      data: this.moreInfo
    });
  }

  deleteVideo(): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: '¿Estas seguro de eliminar el video?',
      text: "No volveras a recuperarlo una vez eliminado",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.homeService.deleteVideo(this.moreInfo)
        .then(
          (res: any) => {
            swalWithBootstrapButtons.fire(
              '¡Eliminado!',
              'El video se elimino correctamente',
              'success'
            )
          }
        ).catch(
          (error: any) => {
            swalWithBootstrapButtons.fire(
              'Error',
              'Verifica tu conexión a internet o vuelva a intentarlo',
              'error'
            )
          }
        )

      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'El video esta a salvo',
          'error'
        )
      }
    })

  }

}
