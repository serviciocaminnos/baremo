import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Doing } from '@core/model/caminnos/home/doing.model';
import { HomeCaminnosService } from '@core/services/caminnos/home-caminnos/home-caminnos.service';

@Component({
  selector: 'app-doing',
  templateUrl: './doing.container.html',
  styleUrls: ['./doing.container.css']
})
export class DoingContainer implements OnInit {
  @Input() doing: Doing;
  isTitle: boolean = false;
  isDescription: boolean = false;
  isSubtitle: boolean = false;

  doingForm: FormGroup;


  state: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private homeService: HomeCaminnosService,
  ) {
    this.buildForm();
  }

  ngOnInit(): void {
    this.doingForm.patchValue(this.doing);
  }

  buildForm(): void {
    this.doingForm = this.formBuilder.group({
      title: [''],
      subtitle: [''],
      description: [''],
      // components: [this.caminno.components]
    })
  }

  saveChange(option: string): void {
    this.state = true;
    this.showInput(option);
    const data: Doing = this.doingForm.value;
    this.homeService.editInformationDoing(data, this.doing.id)
    .then(
      (res: any) => {
        this.state = false;
        // this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        //   this.router.navigate(['/admin/caminnos/home-caminnos'])
        // })
      }
      )
      .catch(
        (error: any) => {
        console.log(error);
      }
    );
  }

  showInput(option: string): void {
    switch (option) {
      case 'title':
        if (this.isTitle) {
          this.isTitle = false;
        } else {
          this.isTitle = true;
        }
        break;
      case 'subtitle':
        if (this.isSubtitle) {
          this.isSubtitle = false;
        } else {
          this.isSubtitle = true;
        }
        break;
      case 'description':
        if (this.isDescription) {
          this.isDescription = false;
        } else {
          this.isDescription = true;
        }
        break;
    
      default:
        this.isDescription = false;
        this.isTitle = false;
        this.isSubtitle = false;
      break;
    }
  }
}
