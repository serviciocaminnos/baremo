import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Caminno } from '@core/model/caminnos/home/caminnos.model';
import { Doing } from '@core/model/caminnos/home/doing.model';
import { BannerHome } from '@core/model/caminnos/home/home-banner.model';
import { MainInfoHome } from '@core/model/caminnos/home/home-main-info.model';
import { MainInfo } from '@core/model/caminnos/home/main-info.model';
import { MoreInfo } from '@core/model/caminnos/home/more-info.model';
import { Ods } from '@core/model/caminnos/home/ods.model';
import { InformationHome } from 'src/app/core/model/caminnos/home-information.model';
import { HomeOds } from 'src/app/core/model/caminnos/home-ods.model';
import { HomeCaminnosService } from 'src/app/core/services/caminnos/home-caminnos/home-caminnos.service';
import { SwiperComponent } from 'swiper/angular';
// import Swiper core and required components
import SwiperCore, {
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  Virtual,
  Zoom,
  Autoplay,
  Thumbs,
  Controller
} from 'swiper/core';
import { EditBannerComponent } from './edit-banner/edit-banner.component';
import { EditInformationComponent } from './edit-information/edit-information.component';
import { EditOdsComponent } from './edit-ods/edit-ods.component';

// install Swiper components
SwiperCore.use([
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  Virtual,
  Zoom,
  Autoplay,
  Thumbs,
  Controller
]);

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  thumbsSwiper: any;
  bannerList: BannerHome[];
  informationList: InformationHome[];
  odsList: HomeOds[];

  status = true;
  sections: MainInfoHome[];
  subCollectionData: Caminno[] | Doing[] | MoreInfo[] | MainInfo[] |BannerHome[] | Ods[]; 
  data: Caminno | Doing | MoreInfo | MainInfo | BannerHome | Ods;
  state: boolean = false;
  constructor(
    private homeServices: HomeCaminnosService,
    private dialog: MatDialog
  ) { }
  ngOnInit(): void {
    this.getBanners();
    this.getMainInfo();
  }

  getMainInfo(): void {
    this.homeServices.getSectionHome().subscribe(
      ( res: MainInfoHome[] ) => {
        this.sections = res;
        // const section = res[0];
        // this.getInfoBySection(section.id);
        // console.log('Section', res);
        // this.bannerList = res;
        // this.getInformations();
      }
    );
  }

  getInfoBySection(id: string): void {
    this.state = true;
    if (id === 'ods') {
      this.homeServices.getOds().subscribe(
        (res: Ods[]) => {
          setTimeout(() => {
            this.subCollectionData = res;
          }, 1800);
          // console.log(res);
          this.getDataInfoBySection(id);
        }
      );
    } else {
      this.homeServices.getSubCollection(id)
      .subscribe(
        (res: Caminno[] | Doing[] | MoreInfo[] | MainInfo[] | BannerHome[] | Ods[]) => {
          setTimeout(() => {
            this.subCollectionData = res;
          }, 1800);
          // console.log(res);
          this.getDataInfoBySection(id);
        }
      );
    }
  }
  getDataInfoBySection(id: string): void {
    this.homeServices.getDocumentHome(id)
      .subscribe(
        (res: Caminno | Doing | MoreInfo | MainInfo | BannerHome | Ods ) => {
          setTimeout(() => {
            this.state = false;
            this.data = res;
          }, 1800);
          // console.log(res);
        }
      );
  }
  
  getBanners(): void {
    this.homeServices.getBanners().subscribe(
      (res: any) => {
        // console.log(res);
        this.bannerList = res;
        this.getInformations();
      }
    );
  }

  getInformations(): void {
    this.homeServices.getInformations().subscribe(
      (res: InformationHome[]) => {
        this.informationList = res;
        this.status = false;
      }
    );
  }

  editBaner(banner: BannerHome): void{
    const dialogRef = this.dialog.open(EditBannerComponent, {
      width: '1000px',
      height: '500px',
      panelClass: 'custom-dialog',
      data: {
        dataBanner: banner
      }
    });
  }
  editInformation(information: InformationHome): void{
    const dialogRef = this.dialog.open(EditInformationComponent, {
      width: '500px',
      height: '500px',
      panelClass: 'custom-dialog',
      data: {
        dataInformation: information
      }
    });
  }
  editOds(ods: HomeOds): void{
    const dialogRef = this.dialog.open(EditOdsComponent, {
      width: '1000px',
      height: '500px',
      panelClass: 'custom-dialog',
      data: {
        dataOds: ods
      }
    });
  }
}
