import { Component, Inject, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { HomeOds } from 'src/app/core/model/caminnos/home-ods.model';
import { HomeCaminnosService } from 'src/app/core/services/caminnos/home-caminnos/home-caminnos.service';

@Component({
  selector: 'app-edit-ods',
  templateUrl: './edit-ods.component.html',
  styleUrls: ['./edit-ods.component.css']
})
export class EditOdsComponent implements OnInit {
  odsForm: FormGroup;

  statusUpload: boolean;
  imageSrc: Observable<any>;
  uploadPercent: Observable<number>;

  ods: HomeOds;
  imgOds: string;

  status: boolean;
  constructor(
    public dialogRef: MatDialogRef<EditOdsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder,
    private storage: AngularFireStorage,
    private snackBar: MatSnackBar,
    private homeService: HomeCaminnosService

  ) {
    this.buildForm();
    this.ods = this.data.dataOds;
    this.imgOds = this.ods.path_image;
    this.odsForm.patchValue(this.ods);
    this.odsForm.get('path_image').valueChanges.subscribe(url => {
      this.imgOds = url;
    });
  }

  ngOnInit(): void {
  }

  buildForm(): void {
    this.odsForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      path_image: ['', [Validators.required]]
    });
  }

  saveChange(): void {
    this.status = true;
    this.homeService.editOds(this.odsForm.value, this.ods.id).then(
      (res: any) => {
        this.dialogRef.close();
        console.log(res);
        this.status = false;
      },
      (error: any) => {
        this.status = false;
        console.log(error);
      }
    );
  }

  clearPath(): void {
    this.odsForm.controls.path_image.reset();
  }

  upload(event: any): void {
    this.statusUpload = true;
    const file = event.target.files[0];
    const dir = 'image-ods-home/' + this.ods.id;
    const fileRef = this.storage.ref(dir);
    const task = this.storage.upload(dir, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges()
    .pipe(
      finalize(() => {
        this.statusUpload = false;
        this.imageSrc = fileRef.getDownloadURL();
        this.openSnackBar();
        this.imageSrc.subscribe(url => {
          this.odsForm.controls.path_image.setValue(url);
          console.log(this.odsForm.value);
          this.imgOds = url;
        });
      })
    ).subscribe();
    // console.log(dir);
  }
  openSnackBar(): void {
    this.snackBar.open('La imagen se subio correctamente', 'Done', {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }

}
