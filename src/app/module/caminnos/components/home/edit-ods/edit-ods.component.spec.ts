import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditOdsComponent } from './edit-ods.component';

describe('EditOdsComponent', () => {
  let component: EditOdsComponent;
  let fixture: ComponentFixture<EditOdsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditOdsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditOdsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
