import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { AngularMaterialModule } from 'src/app/shared/material-angular/angular-material.module';

import { SwiperModule } from 'swiper/angular';
import { EditBannerComponent } from './edit-banner/edit-banner.component';
import { EditInformationComponent } from './edit-information/edit-information.component';
import { ReactiveFormsModule } from '@angular/forms';
import { EditOdsComponent } from './edit-ods/edit-ods.component';
import { SharedModule } from 'src/app/shared/shared.module';

import { CaminnoContainer } from './edit-information/container/caminno/caminno.container';
import { DoingContainer } from './edit-information/container/doing/doing.container';
import { MainInfoContainer } from './edit-information/container/main-info/main-info.container';
import { VideoComponent } from './edit-information/container/video/video.component';
import { AddVideoComponent } from './edit-information/components/add-video/add-video.component';
import { EditVideoComponent } from './edit-information/components/edit-video/edit-video.component';
import { OdsContainer } from './edit-information/container/ods/ods.container';

@NgModule({
  declarations: [
    HomeComponent,
    EditBannerComponent,
    EditInformationComponent,
    EditOdsComponent,
    CaminnoContainer,
    DoingContainer,
    MainInfoContainer,
    VideoComponent,
    AddVideoComponent,
    EditVideoComponent,
    OdsContainer
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    AngularMaterialModule,
    SwiperModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class HomeModule { }
