import { Component, Inject, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { BannerHome } from '@core/model/caminnos/home/home-banner.model';
import { HomeCaminnosService } from 'src/app/core/services/caminnos/home-caminnos/home-caminnos.service';

@Component({
  selector: 'app-edit-banner',
  templateUrl: './edit-banner.component.html',
  styleUrls: ['./edit-banner.component.css']
})
export class EditBannerComponent implements OnInit {
  bannerForm: FormGroup;

  statusUpload: boolean;
  imageSrc: Observable<any>;
  uploadPercent: Observable<number>;
  banner: BannerHome;

  imgBanner: string;
  status: boolean;
  constructor(
    public dialogRef: MatDialogRef<EditBannerComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder,
    private storage: AngularFireStorage,
    private snackBar: MatSnackBar,
    private homeService: HomeCaminnosService
  ) {
    this.buildForm();
    this.banner = this.data.dataBanner;
    this.imgBanner = this.banner.image_path;
    this.bannerForm.patchValue(this.banner);
    this.bannerForm.get('image_path').valueChanges.subscribe(url => {
      this.imgBanner = url;
    });
  }

  buildForm(): void {
    this.bannerForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      image_path: ['', [Validators.required]]
    });
  }

  ngOnInit(): void {
  }

  saveChange(): void {
    this.status = true;
    this.homeService.editBanner(this.bannerForm.value, this.banner.id).then(
      (res: any) => {
        this.status = false;
        this.dialogRef.close();
        console.log(res);
      },
      (error: any) => {
        this.status = false;
        console.log(error);
      }
    );
  }

  clearPath(): void {
    this.bannerForm.controls.image_path.reset();
  }

  upload(event: any): void {
    this.statusUpload = true;
    const file = event.target.files[0];
    const dir = 'image-banner-home/' + this.banner.id;
    const fileRef = this.storage.ref(dir);
    const task = this.storage.upload(dir, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges()
    .pipe(
      finalize(() => {
        this.statusUpload = false;
        this.imageSrc = fileRef.getDownloadURL();
        this.openSnackBar();
        this.imageSrc.subscribe(url => {
          this.bannerForm.controls.image_path.setValue(url);
          console.log(this.bannerForm.value);
        });
      })
    ).subscribe();
    // console.log(dir);
  }
  
  openSnackBar(): void {
    this.snackBar.open('La imagen se subio correctamente', 'Done', {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }

}
