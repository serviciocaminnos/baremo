import { Component, OnInit } from '@angular/core';
import { Award } from '@core/model/caminnos/award.model';
import { AwardService } from '@core/services/caminnos/award/award.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-awards',
  templateUrl: './awards.component.html',
  styleUrls: ['./awards.component.css']
})
export class AwardsComponent implements OnInit {
  state: boolean = false;
  awards: Award[];
  constructor(
    private awardService: AwardService
  ) { }

  ngOnInit(): void {
    this.getAward();
  }

  getAward(): void {
    this.state = true;
    this.awardService.getAwards().subscribe(
      (res: Award[]) => {
        this.awards = res;
        this.state = false;
        console.log(res);
      },
      (error: any) => {
        this.state = false;
        console.log(error);
      },
    );
  }

  addAward(): void {
    const award: Award = {
      id: '',
      title: 'Nuevo Premio',
      description: 'Reseña acerca del premio',
      image: 'https://scontent.fsrz1-2.fna.fbcdn.net/v/t1.6435-9/80206598_149659809777922_537181438135500800_n.jpg?_nc_cat=104&ccb=1-5&_nc_sid=973b4a&_nc_ohc=AI_9pvtwniYAX8AIj4J&_nc_ht=scontent.fsrz1-2.fna&oh=79e3e148c39c4b5984e1df488bc2e230&oe=61983F4E',
      date: new Date(),
    }
    this.awardService.addAward(award)
    .then(
      (res: any) => {
        this.state = false;
        // this.projects = res;
        Swal.fire({
          title: 'Agregado Correctamente',
          text: 'El nuevo premio se agregó correctamente',
          icon: 'success',
          showConfirmButton: false,
          timer: 2000
        })
        console.log(res);
      }
      )
    .catch(
      (error: any) => {
        this.state = false;
        console.log(error);
      },
    );
  }

}
