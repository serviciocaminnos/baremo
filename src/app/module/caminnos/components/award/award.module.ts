import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AwardRoutingModule } from './award-routing.module';
import { AwardsComponent } from './components/awards/awards.component';
import { AwardComponent } from './container/award/award.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from '@material/angular-material.module';


@NgModule({
  declarations: [AwardsComponent, AwardComponent],
  imports: [
    CommonModule,
    AwardRoutingModule,
    ReactiveFormsModule,
    AngularMaterialModule
  ]
})
export class AwardModule { }
