import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Countrie } from '@core/model/caminnos/countrie.model';
import { Project } from '@core/model/caminnos/project.model';
import { ProjectCaminnosService } from '@core/services/caminnos/project-caminnos/project-caminnos.service';

@Component({
  templateUrl: './edit-project.component.html',
  styleUrls: ['./edit-project.component.css']
})
export class EditProjectComponent implements OnInit {
  projectForm: FormGroup;

  countries: Countrie[];
  country: FormControl = new FormControl();
  state: boolean;

  constructor(
    private dialogRef: MatDialogRef<EditProjectComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Project,
    private formBuilder: FormBuilder,
    private projectService: ProjectCaminnosService,
  ) {
    console.log(this.data);
    this.buildForm();
    this.country.valueChanges.subscribe((res) => {
      this.projectForm.controls.country.setValue(res);
    });
  }

  ngOnInit(): void {
    this.projectForm.patchValue(this.data);
    this.projectForm.controls.date.setValue(this.data.date.toDate());
    this.getCountries();
  }

  buildForm(): void {
    this.projectForm = this.formBuilder.group({
      title: [''],
      subtitle: [''],
      description: [''],
      organization: [''],
      country: [''],
      image: [''],
      date: [''],
    });
  }
  
  getCountries(): void {
    this.projectService.getCountries().subscribe(
      (res: Countrie[]) => {
        this.countries = res;
        // console.log(res);
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  save(): void {
    this.state = true;
    const dataForm = this.projectForm.value;
    const data: Project = {
      id: this.data.id,
      title: dataForm.title,
      subtitle: this.data.subtitle,
      image: this.data.image,
      icon_country: this.data.icon_country,
      country: dataForm.country,
      date: dataForm.date,
      description: dataForm.description,
      organization: dataForm.organization
    }
    this.projectService.editProject(data, this.data.id)
    .then(
      (res: any) => {
        // console.log(res);
        this.dialogRef.close();
        this.state = false;
      }
      )
    .catch(
      (error: any) => {
        this.state = false;
        console.log(error);
      }
    );
  }
}
