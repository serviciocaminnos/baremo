import { Component, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import SwiperCore, {
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  Virtual,
  Zoom,
  Autoplay,
  Thumbs,
  Controller
} from 'swiper/core';

// install Swiper components
SwiperCore.use([
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  Virtual,
  Zoom,
  Autoplay,
  Thumbs,
  Controller
]);
@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.css']
})
export class CreateProjectComponent implements OnInit {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  statusUpload: boolean;
  imageSrc: Observable<any>;
  uploadPercent: Observable<number>;

  imgProject: string;
  thumbsSwiper: any;

  public uploadedFiles: Array<File> = [];
  imageList: any[] = [];

  status = false;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private storage: AngularFireStorage,
    private snackBar: MatSnackBar,
  ) {
    this.buildForm();
    this.firstFormGroup.get('first_img').valueChanges.subscribe(url => {
      this.imgProject = url;
    });
  }

  buildForm(): void {
    this.firstFormGroup = this.formBuilder.group({
      title: ['', Validators.required],
      subtitle: ['', Validators.required],
      description: ['', Validators.required],
      date: [''],
      first_img: [''],
      country: [''],
      town: [''],
    });
    this.secondFormGroup = this.formBuilder.group({
      about: ['', Validators.required],
      first_img_banner: ['https://ichef.bbci.co.uk/news/976/cpsprodpb/C342/production/_88068994_thinkstockphotos-493881770.jpg'],
      first_title_banner: ['Titulo 1'],
      second_img_banner: ['https://blog.greencarrier.com/wp-content/uploads/2019/03/Road-3-wpv_770x440_left_center.jpg'],
      second_title_banner: ['Titulo 2'],
      third_img_banner: ['https://www.teknos.com/globalassets/teknos.com/industrial-coatings/special-applications/road-marking-and-parking-lots/road-marking.jpg'],
      third_title_banner: ['Titulo 3'],
    });
  }
  clearPath(option: number): void {
    switch (option) {
      case 0:
        this.firstFormGroup.controls.first_img.reset();
        break;
      case 1:
        this.secondFormGroup.controls.first_img_banner.reset();
        break;
      case 2:
        this.secondFormGroup.controls.second_img_banner.reset();
        break;
      case 3:
        this.secondFormGroup.controls.third_img_banner.reset();
        break;
      default:
        console.log('No hacer nada');
        break;
    }
  }
  ngOnInit(): void {

  }

  back(): void {
    this.router.navigate(['admin/caminnos/project-caminnos']);
  }

  upload(event: any, option: number): void {
    this.statusUpload = true;
    const file = event.target.files[0];

    let dir;
    switch (option) {
      case 0:
        dir = 'image-project/' + this.firstFormGroup.controls.title.value;
        break;
        case 1:
          dir = 'image-project-banner/' + this.secondFormGroup.controls.first_title_banner.value;
          break;
          case 2:
            dir = 'image-project-banner/' + this.secondFormGroup.controls.second_title_banner.value;
            break;
            case 3:
              dir = 'image-project-banner/' + this.secondFormGroup.controls.third_title_banner.value;
              break;
      default:
        console.log('No hacer Nada');
        break;
    }

    const fileRef = this.storage.ref(dir);
    const task = this.storage.upload(dir, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges()
    .pipe(
      finalize(() => {
        this.statusUpload = false;
        this.imageSrc = fileRef.getDownloadURL();
        this.openSnackBar();
        this.imageSrc.subscribe(url => {
          switch (option) {
            case 0:
              this.firstFormGroup.controls.first_img.setValue(url);
              break;
              case 1:
                this.secondFormGroup.controls.first_img_banner.setValue(url);
                break;
                case 2:
                  this.secondFormGroup.controls.second_img_banner.setValue(url);
                  break;
                  case 3:
                    this.secondFormGroup.controls.third_img_banner.setValue(url);
                    break;
            default:
              console.log('No hacer Nada');
              break;
          }
        });
      })
    ).subscribe();
    // console.log(dir);
  }
  openSnackBar(): void {
    this.snackBar.open('La imagen se subio correctamente', 'Done', {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }


  public clear(): void {
    this.uploadedFiles = [];
  }

  // tslint:disable-next-line: typedef
  async uploadMultipleFile() {
    this.status = true;
    await this.uploadedFiles.forEach(file => {
      this.uploadImage(file);
    });
  }

  // tslint:disable-next-line: typedef
  async uploadImage(file: File) {
    const dir = 'image-by-project/' + this.firstFormGroup.controls.title.value + '/' + file.name;
    const fileRef = this.storage.ref(dir);
    const task = this.storage.upload(dir, file);
    this.uploadPercent = task.percentageChanges();
    await task.snapshotChanges()
    .pipe(
      finalize(() => {
        this.statusUpload = false;
        this.imageSrc = fileRef.getDownloadURL();
        this.openSnackBar();
        this.imageSrc.subscribe(url => {
          this.imageList.push(url);
          // console.log(url);
          if (this.uploadedFiles.length === this.imageList.length) {
            this.status = false;
            this.uploadedFiles = [];
          } else {
            console.log('Salio un error');
          }
        });
      })
    ).subscribe();

  }
}
