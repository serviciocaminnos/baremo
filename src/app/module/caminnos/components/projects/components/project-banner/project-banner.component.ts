import { Component, Input, OnInit } from '@angular/core';
import { ProjectBanner } from '@core/model/caminnos/project-banner.model';

@Component({
  selector: 'app-project-banner',
  templateUrl: './project-banner.component.html',
  styleUrls: ['./project-banner.component.css']
})
export class ProjectBannerComponent implements OnInit {
  @Input() projectBanner: ProjectBanner;
  constructor() { }

  ngOnInit(): void {
  }

}
