import { Component, Input, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Project } from '@core/model/caminnos/project.model';
import { ProjectCaminnosService } from '@core/services/caminnos/project-caminnos/project-caminnos.service';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { EditProjectComponent } from '../../components/edit-project/edit-project.component';

@Component({
  selector: 'app-project',
  templateUrl: './project.container.html',
  styleUrls: ['./project.container.css']
})
export class ProjectContainer implements OnInit {
  @Input() project: Project;

  statusUpload: boolean = false;
  imageSrc: Observable<string | null>;
  uploadPercent: Observable<number>;
  imageForm: string;

  constructor(
    private dialog: MatDialog,
    private storage: AngularFireStorage,
    private snackBar: MatSnackBar,
    private projectService: ProjectCaminnosService,
  ) { }

  ngOnInit(): void {
  }

  editProject(): void {
    const dialogRef = this.dialog.open(EditProjectComponent, {
      width: '500px',
      height: '700px',
      panelClass: 'custom-dialog-container',
      data: this.project
    });
  }

  upload(event: any): void {
    this.statusUpload = true;
    const file = event.target.files[0];
    const dir = 'projects/' + this.project.id;
    const fileRef = this.storage.ref(dir);
    const task = this.storage.upload(dir, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges()
    .pipe(
      finalize(() => {
        this.statusUpload = false;
        this.imageSrc = fileRef.getDownloadURL();
        this.openSnackBar();
        this.imageSrc.subscribe(url => {
          this.imageForm = url;
          // this.image = url;
          // console.log(this.image);
          this.saveChange();
          // console.log(this.bannerForm.value);
        });
      })
    ).subscribe(() => {
    });
    // console.log(dir);
  }
  
  saveChange(): void {
    const data: Project = {
      id: this.project.id,
      title: this.project.title,
      subtitle: this.project.subtitle,
      image: this.imageForm,
      icon_country: this.project.icon_country,
      country: this.project.country,
      date: this.project.date,
      description: this.project.description,
      organization: this.project.organization
    }
    this.projectService.editProject(data, this.project.id)
    .then(
      (res: any) => {
        // console.log(res);
        // this.state = false;
      }
      )
    .catch(
      (error: any) => {
        // this.state = false;
        console.log(error);
      }
    );
  }

  deleteProject(): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: '¿Estas seguro de eliminar el proyecto:' + this.project.title + '?',
      text: "No volveras a recuperarlo una vez eliminado",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.projectService.deleteProject(this.project)
        .then(
          (res: any) => {
            swalWithBootstrapButtons.fire(
              '¡Eliminado!',
              'El proyecto se eliminó correctamente',
              'success'
            )
            this.deleteImage(this.project.image);
          }
        ).catch(
          (error: any) => {
            swalWithBootstrapButtons.fire(
              'Error',
              'Verifica tu conexión a internet o vuelva a intentarlo',
              'error'
            )
          }
        )

      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'El proyecto esta a salvo',
          'error'
        )
      }
    })

  }

  openSnackBar(): void {
    this.snackBar.open('La imagen se subio correctamente', 'Done', {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }

  deleteImage(url) {
    return this.storage.storage.refFromURL(url).delete();
  }
}
