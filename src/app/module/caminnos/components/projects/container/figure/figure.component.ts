import { Component, Input, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ProjectBanner } from '@core/model/caminnos/project-banner.model';
import { ProjectCaminnosService } from '@core/services/caminnos/project-caminnos/project-caminnos.service';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-figure',
  templateUrl: './figure.component.html',
  styleUrls: ['./figure.component.css']
})
export class FigureComponent implements OnInit {
  @Input() banner: ProjectBanner;

  statusUpload: boolean = false;
  imageSrc: Observable<string | null>;
  uploadPercent: Observable<number>;
  bannerForm: FormGroup;

  constructor(
    private dialog: MatDialog,
    private storage: AngularFireStorage,
    private snackBar: MatSnackBar,
    private projectService: ProjectCaminnosService,
    private formBuilder: FormBuilder
  ) {
    this.buildForm();
  }

  ngOnInit(): void {
    this.bannerForm.patchValue(this.banner);
  }

  buildForm(): void {
    this.bannerForm = this.formBuilder.group({
      id: [''],
      title: [''],
      description: [''],
      image: [''],
    });
  }

  upload(event: any): void {
    this.statusUpload = true;
    const file = event.target.files[0];
    const dir = 'projects-banners/' + this.banner.id;
    const fileRef = this.storage.ref(dir);
    const task = this.storage.upload(dir, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges()
    .pipe(
      finalize(() => {
        this.statusUpload = false;
        this.imageSrc = fileRef.getDownloadURL();
        this.openSnackBar();
        this.imageSrc.subscribe(url => {
          this.bannerForm.controls.image.setValue(url);
          // this.image = url;
          // console.log(this.image);
          this.saveChange();
          // console.log(this.bannerForm.value);
        });
      })
    ).subscribe(() => {
    });
    // console.log(dir);
  }
  
  saveChange(): void {
    const data: ProjectBanner = this.bannerForm.value;
    this.projectService.editProjectBanner(data, this.banner.id)
    .then(
      (res: any) => {
        // console.log(res);
        // this.state = false;
      }
    )
    .catch(
      (error: any) => {
        // this.state = false;
        console.log(error);
      }
    );
  }

  openSnackBar(): void {
    this.snackBar.open('La imagen se subio correctamente', 'Done', {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }

  deleteImage(url) {
    return this.storage.storage.refFromURL(url).delete();
  }


}
