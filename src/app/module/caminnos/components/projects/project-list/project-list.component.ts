import { Component, OnInit } from '@angular/core';
import { ProjectBanner } from '@core/model/caminnos/project-banner.model';
import { Project } from '@core/model/caminnos/project.model';
import { ProjectCaminnosService } from '@core/services/caminnos/project-caminnos/project-caminnos.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit {
  projects: Project[];
  state: boolean = false;
  projectBanner!: ProjectBanner[];

  constructor(
    private projectService: ProjectCaminnosService
  ) { }
  ngOnInit(): void {
    this.getProjects();
    this.getProjectBanner();
  }

  getProjectBanner(){
    this.projectService.getInformationBanner()
    .subscribe(
      (res: ProjectBanner[])=>{
        this.projectBanner = res;
      },
      (error: any)=>
      {
        console.log(error);
        
      }
    )
  }

  getProjects(): void {
    this.state = true;
    this.projectService.getProjects().subscribe(
      (res: Project[]) => {
        this.projects = res;
        this.state = false;
        // console.log(res);
      },
      (error: any) => {
        this.state = false;
        console.log(error);
      },
    );
  }

  addProject(): void {
    this.state = true;
    const data: Project = {
      id: '',
      title: 'No Asignado',
      subtitle: 'No Asignado',
      image: 'https://scontent.fsrz1-2.fna.fbcdn.net/v/t1.6435-9/80206598_149659809777922_537181438135500800_n.jpg?_nc_cat=104&ccb=1-5&_nc_sid=973b4a&_nc_ohc=AI_9pvtwniYAX8AIj4J&_nc_ht=scontent.fsrz1-2.fna&oh=79e3e148c39c4b5984e1df488bc2e230&oe=61983F4E',
      icon_country: ['bo'],
      country: 'Bolivia',
      date: new Date,
      description: 'Ninguna',
      organization: 'Ninguna'
    }
    this.projectService.addProject(data)
    .then(
      (res: any) => {
        this.state = false;
        // this.projects = res;
        Swal.fire({
          title: 'Agregado Correctamente',
          text: 'El nuevo proyecto se agregó correctamente',
          icon: 'success',
          showConfirmButton: false,
          timer: 2000
        })
        console.log(res);
      }
      )
    .catch(
      (error: any) => {
        this.state = false;
        console.log(error);
      },
    );
  }

}
