import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectsRoutingModule } from './projects-routing.module';
import { ProjectListComponent } from './project-list/project-list.component';
import { AngularMaterialModule } from 'src/app/shared/material-angular/angular-material.module';
import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { CreateProjectComponent } from './components/create-project/create-project.component';
import { SwiperModule } from 'swiper/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { FileUploadModule } from '@iplab/ngx-file-upload';
import { ProjectContainer } from './container/project/project.container';
import { EditProjectComponent } from './components/edit-project/edit-project.component';
import { ProjectBannerComponent } from './components/project-banner/project-banner.component';
import { FigureComponent } from './container/figure/figure.component';


@NgModule({
  declarations: [
    ProjectListComponent, 
    ProjectDetailComponent, 
    CreateProjectComponent, 
    ProjectContainer,
    EditProjectComponent,
    ProjectBannerComponent,
    FigureComponent
  ],
  imports: [
    CommonModule,
    ProjectsRoutingModule,
    AngularMaterialModule,
    SwiperModule,
    ReactiveFormsModule,
    SharedModule,
    FileUploadModule,
    FormsModule
  ]
})
export class ProjectsModule { }
