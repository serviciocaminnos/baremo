import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutUsContainer } from './about-us.container';

describe('AboutUsContainer', () => {
  let component: AboutUsContainer;
  let fixture: ComponentFixture<AboutUsContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AboutUsContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutUsContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
