import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { About } from '@core/model/caminnos/about/about.model';
import { AboutUsService } from '@core/services/caminnos/about/about-us.service';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.container.html',
  styleUrls: ['./about-us.container.css']
})
export class AboutUsContainer implements OnInit {
  @Input() about: About;
  isTitle: boolean;
  isDescription: boolean;
  statusUpload: boolean;
  imageSrc: any;
  aboutForm: FormGroup;
  uploadPercent: any;
  state: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private aboutUsService: AboutUsService,
  ) {
    this.buildForm();
  }
  
  ngOnInit(): void {
    this.aboutForm.patchValue(this.about);
  }

  buildForm(): void {
    this.aboutForm = this.formBuilder.group({
      id: [''],
      title: [''],
      description: [''],
    });
  }

  saveChange(option: string): void {
    this.state = true;
    this.showInput(option);
    const about: About = this.aboutForm.value;
    // console.log(data);
    this.aboutUsService.editAboutUs(about, this.about.id)
    .then(
      (res: any) => {
        this.state = false;
      }
      )
      .catch(
        (error: any) => {
        console.log(error);
      }
    );
  }


  changeDate(): void {
    this.saveChange('date');
  }

  showInput(option: string): void {
    switch (option) {
      case 'title':
        if (this.isTitle) {
          this.isTitle = false;
        } else {
          this.isTitle = true;
        }
        break;
      case 'description':
        if (this.isDescription) {
          this.isDescription = false;
        } else {
          this.isDescription = true;
        }
        break;
    
      default:
        this.isDescription = false;
        this.isTitle = false;
      break;
    }
  }
}
