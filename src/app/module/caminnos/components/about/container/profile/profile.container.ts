import { Component, Input, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AboutTeam } from '@core/model/caminnos/about/about-team.model';
import { AboutTeamProfile } from '@core/model/caminnos/about/team-profile.model';
import { AboutUsService } from '@core/services/caminnos/about/about-us.service';
import { finalize } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.container.html',
  styleUrls: ['./profile.container.css']
})
export class ProfileContainer implements OnInit {

  @Input() profile: AboutTeamProfile | any;
  @Input() team: AboutTeam | any;
  
  isName: boolean;
  isBiography: boolean;
  isWorkPosition: boolean;

  statusUpload: boolean;
  imageSrc: any;
  profileForm: FormGroup;
  uploadPercent: any;
  state: boolean = false;

  constructor(
    private storage: AngularFireStorage,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private aboutService: AboutUsService,
  ) {
    this.buildForm();
  }
  
  ngOnInit(): void {
    this.profileForm.patchValue(this.profile);
    this.profileForm.controls.date.setValue(this.profile.date.toDate());
  }

  buildForm(): void {
    this.profileForm = this.formBuilder.group({
      id: [''],
      name: [''],
      biography: [''],
      image: [''],
      date: [''],
      work_position: [''],
    });
  }

  saveChange(option: string): void {
    this.state = true;
    this.showInput(option);
    const profile: AboutTeamProfile = this.profileForm.value;
    // console.log(profile);
    this.aboutService.editProfile(profile, this.team)
    .then(
      (res: any) => {
        this.state = false;
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
      }
    );
  }


  changeDate(): void {
    this.saveChange('date');
  }

  deleteAward(): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: '¿Estas seguro de eliminar el perfil: ' + this.profile.name + '?',
      text: "No volveras a recuperarlo una vez eliminado",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Eliminar!',
      cancelButtonText: 'Cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.aboutService.deleteProfile(this.profile, this.team)
        .then(
          (res: any) => {
            swalWithBootstrapButtons.fire(
              '¡Eliminado!',
              'El perfil se eliminó correctamente',
              'success'
            )
            this.deleteImage(this.profile.image);
          }
        ).catch(
          (error: any) => {
            swalWithBootstrapButtons.fire(
              'Error',
              'Verifica tu conexión a internet o vuelva a intentarlo',
              'error'
            )
          }
        )

      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'El perfil esta a salvo',
          'error'
        )
      }
    })

  }

  upload(event: any): void {
    this.statusUpload = true;
    const file = event.target.files[0];
    const dir = 'profile/' + this.profile.id;
    const fileRef = this.storage.ref(dir);
    const task = this.storage.upload(dir, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges()
    .pipe(
      finalize(() => {
        this.statusUpload = false;
        this.imageSrc = fileRef.getDownloadURL();
        this.openSnackBar();
        this.imageSrc.subscribe(url => {
          this.profileForm.controls.image.setValue(url);
          // this.image = url;
          // console.log(this.image);
          this.saveChange('image');
          // console.log(this.bannerForm.value);
        });
      })
    ).subscribe(() => {
    });
    // console.log(dir);
  }
  
  openSnackBar(): void {
    this.snackBar.open('La imagen se subio correctamente', 'Done', {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }

  showInput(option: string): void {
    switch (option) {
      case 'name':
        if (this.isName) {
          this.isName = false;
        } else {
          this.isName = true;
        }
        break;
      case 'biography':
        if (this.isBiography) {
          this.isBiography = false;
        } else {
          this.isBiography = true;
        }
        break;
      case 'work':
        if (this.isWorkPosition) {
          this.isWorkPosition = false;
        } else {
          this.isWorkPosition = true;
        }
        break;
    
      default:
        this.isName = false;
        this.isBiography = false;
        this.isWorkPosition = false;
      break;
    }
  }
  deleteImage(url) {
    return this.storage.storage.refFromURL(url).delete();
  }
}
