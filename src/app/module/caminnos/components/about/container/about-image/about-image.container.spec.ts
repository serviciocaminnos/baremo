import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutImageContainer } from './about-image.container';

describe('AboutImageContainer', () => {
  let component: AboutImageContainer;
  let fixture: ComponentFixture<AboutImageContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AboutImageContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutImageContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
