import { Component, OnInit, Input } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AboutImage } from '@core/model/caminnos/about/about-image.model';
import { AboutUsService } from '@core/services/caminnos/about/about-us.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-about-image',
  templateUrl: './about-image.container.html',
  styleUrls: ['./about-image.container.css']
})
export class AboutImageContainer implements OnInit {
  @Input() aboutImage!: AboutImage;

  statusUpload: boolean;
  imageSrc: any;
  aboutImageForm: FormGroup;
  uploadPercent: any;
  state: boolean = false;

  constructor(
    private storage: AngularFireStorage,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private formBuilder: FormBuilder,
    private aboutService: AboutUsService,
  ) {
    this.buildForm();
  }

  ngOnInit(): void {
    this.aboutImageForm.patchValue(this.aboutImage);
  }


  buildForm(): void {
    this.aboutImageForm = this.formBuilder.group({
      id: [''],
      image: [''],
    });
  }
  saveChange(option: string): void {
    this.state = true;
    const aboutImage: AboutImage = this.aboutImageForm.value;
    // console.log(data);
    this.aboutService.editAboutImage(aboutImage, this.aboutImage.id)
    .then(
      (res: any) => {
        this.state = false;
      }
      )
      .catch(
        (error: any) => {
        console.log(error);
      }
    );
  }

  upload(event: any): void {
    this.statusUpload = true;
    const file = event.target.files[0];
    const dir = 'about-image/' + this.aboutImage.id;
    const fileRef = this.storage.ref(dir);
    const task = this.storage.upload(dir, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges()
    .pipe(
      finalize(() => {
        this.statusUpload = false;
        this.imageSrc = fileRef.getDownloadURL();
        this.openSnackBar();
        this.imageSrc.subscribe(url => {
          this.aboutImageForm.controls.image.setValue(url);
          // this.image = url;
          // console.log(this.image);
          this.saveChange('image');
          // console.log(this.bannerForm.value);
        });
      })
    ).subscribe(() => {
    });
    // console.log(dir);
  }
  
  openSnackBar(): void {
    this.snackBar.open('La imagen se subio correctamente', 'Done', {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }

}
