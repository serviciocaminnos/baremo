import { AboutUsService } from '@core/services/caminnos/about/about-us.service';
import { Component, OnInit } from '@angular/core';
import { About } from '@core/model/caminnos/about/about.model';
import { AboutImage } from '@core/model/caminnos/about/about-image.model';
import { AboutTeam } from '@core/model/caminnos/about/about-team.model';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  title: string = 'Nosotros'
  isHideBtn: boolean = false;
  aboutUsList: About[];
  aboutImageList: AboutImage[];
  aboutTeamList: AboutTeam[];

  constructor(
    private aboutUsService: AboutUsService
  ) { }

  ngOnInit(): void {
    this.getAboutUs();
    this.getAboutImage();
    this.getAboutTeam();
  }
  
  getAboutTeam(): void {
    this.aboutUsService.getAboutTeam().subscribe(
      (res: AboutTeam[]) => {
        // console.log(res);
        this.aboutTeamList = res;
      }
    )
  }

  getAboutImage(): void {
    this.aboutUsService.getAboutImage().subscribe(
      (res: AboutImage[]) => {
        // console.log(res);
        
        this.aboutImageList = res;
      }
    )
  }
  
  getAboutUs(): void {
    this.aboutUsService.getAboutUs().subscribe(
      (res: About[]) => {
        this.aboutUsList = res;
      }
    )
  }
  
  changeTab(event: any): void {
    console.log(event);
    if (event === 0) {
      this.title = 'Nosotros';
      this.isHideBtn = false;
    } else {
      this.title = 'Equipo';
      this.isHideBtn = true;
    }
  }

}
