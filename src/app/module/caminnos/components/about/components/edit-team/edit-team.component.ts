import { AboutTeam } from '@core/model/caminnos/about/about-team.model';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AboutUsService } from '@core/services/caminnos/about/about-us.service';
import Swal  from 'sweetalert2';

@Component({
  templateUrl: './edit-team.component.html',
  styleUrls: ['./edit-team.component.css']
})
export class EditTeamComponent implements OnInit {

  aboutTeamForm: FormGroup;

  constructor(
    private dialogRef: MatDialogRef<EditTeamComponent>,
    @Inject(MAT_DIALOG_DATA) public data: AboutTeam,
    private formBuilder: FormBuilder,
    private aboutService: AboutUsService,
  ) {
    this.buildForm();
  }
  
  ngOnInit(): void {
    this.aboutTeamForm.patchValue(this.data);
  }

  buildForm(): void {
    this.aboutTeamForm = this.formBuilder.group({
      id: [''],
      title: ['']
    });
  }

  saveData(): void {
    const aboutData: AboutTeam = this.aboutTeamForm.value;
    this.aboutService.editAboutTeam(aboutData, this.data.id)
    .then(
      (res: any) => {
        // console.log(res);
        Swal.fire({
          icon: 'success',
          title: 'Guardado Correctamente',
          text: 'Los datos se guardaron correctamente',
          showConfirmButton: false,
          timer: 2000
        });
        this.dialogRef.close();
      }
    )
    .catch(
      (error: any) => {
        // console.log(res);
      }
    );
  }

}
