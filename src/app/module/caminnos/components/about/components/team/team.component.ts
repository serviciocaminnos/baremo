import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AboutTeam } from '@core/model/caminnos/about/about-team.model';
import { AboutTeamProfile } from '@core/model/caminnos/about/team-profile.model';
import { AboutUsService } from '@core/services/caminnos/about/about-us.service';
import Swal from 'sweetalert2';
import { EditTeamComponent } from '../edit-team/edit-team.component';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit {
  @Input() aboutTeam: AboutTeam[];
  
  team: AboutTeam;
  index: number;
  numberStorage: string;
  state: boolean = false;
  teamProfiles: AboutTeamProfile[];
  stateTeam: boolean = false;
  constructor(
    private aboutService: AboutUsService,
    private dialog: MatDialog,
  ) {
    this.numberStorage = localStorage.getItem('index-team');
    //   this.team = this.aboutTeam[this.index];
      
    // }, 2000);
  }

  ngOnInit(): void {
    this.index = this.numberStorage? + this.numberStorage : 0;
    // setTimeout(() => {
    // this.team = this.aboutTeam[this.index];
  }

  addTeam(): void {
    this.state = true;
    const team: AboutTeam = {
      id: '',
      title: 'Nuevo Equipo'
    }
    this.aboutService.addAboutTeam(team)
    .then(
      (res: any) => {
        this.state = false;
        console.log(res);
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
      }
    )
  }

  editTeam(team: AboutTeam): void {
    const dialogRef = this.dialog.open( EditTeamComponent, {
      width: '500px',
      height: '250px',
      panelClass: 'custom-dialog-container',
      data: team
    });
  }

  deleteTeam(team: AboutTeam): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: '¿Estas seguro de eliminar el equipo: ' + team.title + '?',
      text: "No volveras a recuperarlo una vez eliminado",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Eliminar',
      cancelButtonText: 'Cancelar',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.aboutService.deleteAboutTeam(team)
        .then(
          (res: any) => {
            swalWithBootstrapButtons.fire(
              '¡Eliminado!',
              'El equipo se eliminó correctamente',
              'success'
            )
          }
        ).catch(
          (error: any) => {
            swalWithBootstrapButtons.fire(
              'Error',
              'Verifica tu conexión a internet o vuelva a intentarlo',
              'error'
            )
          }
        )

      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'El equipo esta a salvo',
          'error'
        )
      }
    })
  }

  getTeamProfile(team: AboutTeam): void {
    this.team = team;
    this.stateTeam = true;
    this.aboutService.getAboutTeamProfile(team)
    .subscribe(
      (res: AboutTeamProfile[]) => {
        this.stateTeam = false;
        this.teamProfiles = res;
        // console.log(res);
        
      },
      (error: any) => {
        this.stateTeam = false;
        console.log(error);
      },
    );
  }

  addTeamProfile(): void {
    const profile: AboutTeamProfile = {
      id: '',
      name: 'Nombre',
      biography: 'Biografia acerca del nuevo integrante',
      work_position: 'Cargo del integrante',
      image: 'https://scontent.fsrz1-2.fna.fbcdn.net/v/t1.6435-9/80206598_149659809777922_537181438135500800_n.jpg?_nc_cat=104&ccb=1-5&_nc_sid=973b4a&_nc_ohc=AI_9pvtwniYAX8AIj4J&_nc_ht=scontent.fsrz1-2.fna&oh=79e3e148c39c4b5984e1df488bc2e230&oe=61983F4E',
      date: new Date()
    };

    this.aboutService.addProfile(profile, this.team.id)
    .then(
      (res: any) => {
        Swal.fire({
          icon: 'success',
          title: 'Agregado Correctamente',
          text: 'El nuevo perfile se agregó correctamente',
          showConfirmButton: false,
          timer: 2000
        });
      },
    )
    .catch(
      (error: any) => {
        console.log(error);
      },
    )
  }
}
