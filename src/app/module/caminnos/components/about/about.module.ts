import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AboutRoutingModule } from './about-routing.module';
import { AboutComponent } from './about.component';
import { AngularMaterialModule } from '@material/angular-material.module';
import { AboutUsContainer } from './container/about-us/about-us.container';
import { ReactiveFormsModule } from '@angular/forms';
import { AboutImageContainer } from './container/about-image/about-image.container';
import { TeamComponent } from './components/team/team.component';
import { EditTeamComponent } from './components/edit-team/edit-team.component';
import { ProfileContainer } from './container/profile/profile.container';


@NgModule({
  declarations: [
    AboutComponent,
    AboutUsContainer,
    AboutImageContainer,
    TeamComponent,
    EditTeamComponent,
    ProfileContainer
  ],
  imports: [
    CommonModule,
    AboutRoutingModule,
    AngularMaterialModule,
    ReactiveFormsModule
  ]
})
export class AboutModule { }
