import { Component, Input, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Organization } from '@core/model/caminnos/organization.model';
import { AlianceCaminnosService } from '@core/services/caminnos/aliance-caminnos/aliance-caminnos.service';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-organization-data',
  templateUrl: './organization-data.container.html',
  styleUrls: ['./organization-data.container.css']
})
export class OrganizationDataContainer implements OnInit {
  @Input() organization: Organization;
  statusUpload: boolean;
  imageSrc: Observable<string | null>;
  organizationForm: FormGroup;
  uploadPercent: Observable<number>;
  state: boolean = false;
  constructor(
    private storage: AngularFireStorage,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private allianceService: AlianceCaminnosService,
  ) {
    this.buildForm();
  }
  
  ngOnInit(): void {
    this.organizationForm.patchValue(this.organization);
  }

  buildForm(): void {
    this.organizationForm = this.formBuilder.group({
      id: [''],
      image: [''],
    });
  }

  saveChange(option: string): void {
    this.state = true;
    const organization: Organization = this.organizationForm.value;
    // console.log(data);
    this.allianceService.editOrganization(organization, this.organization.id)
    .then(
      (res: any) => {
        this.state = false;
      }
      )
      .catch(
        (error: any) => {
        console.log(error);
      }
    );
  }

  deleteOrganization(): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: '¿Estas seguro de eliminar la organización:?',
      text: "No volveras a recuperarlo una vez eliminado",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.allianceService.deleteOrganization(this.organization)
        .then(
          (res: any) => {
            swalWithBootstrapButtons.fire(
              '¡Eliminado!',
              'La organización se eliminó correctamente',
              'success'
            )
            this.deleteImage(this.organization.image);
          }
        ).catch(
          (error: any) => {
          }
        )

      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'La organización esta a salvo',
          'error'
        )
      }
    })

  }

  upload(event: any): void {
    this.statusUpload = true;
    const file = event.target.files[0];
    const dir = 'organization/' + this.organization.id;
    const fileRef = this.storage.ref(dir);
    const task = this.storage.upload(dir, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges()
    .pipe(
      finalize(() => {
        this.statusUpload = false;
        this.imageSrc = fileRef.getDownloadURL();
        this.openSnackBar();
        this.imageSrc.subscribe(url => {
          this.organizationForm.controls.image.setValue(url);
          // this.image = url;
          // console.log(this.image);
          this.saveChange('image');
          // console.log(this.bannerForm.value);
        });
      })
    ).subscribe(() => {
    });
    // console.log(dir);
  }
  
  openSnackBar(): void {
    this.snackBar.open('La imagen se subio correctamente', 'Done', {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }

  
  deleteImage(url) {
    return this.storage.storage.refFromURL(url).delete();
  }
}
