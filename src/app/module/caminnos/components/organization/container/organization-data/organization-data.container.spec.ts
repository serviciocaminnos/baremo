import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizationDataContainer } from './organization-data.container';

describe('OrganizationDataContainer', () => {
  let component: OrganizationDataContainer;
  let fixture: ComponentFixture<OrganizationDataContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrganizationDataContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizationDataContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
