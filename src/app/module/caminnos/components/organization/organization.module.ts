import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrganizationRoutingModule } from './organization-routing.module';
import { OrganizationComponent } from './organization.component';
import { AngularMaterialModule } from '@material/angular-material.module';
import { OrganizationDataContainer } from './container/organization-data/organization-data.container';


@NgModule({
  declarations: [
    OrganizationComponent, 
    OrganizationDataContainer
  ],
  imports: [
    CommonModule,
    OrganizationRoutingModule,
    AngularMaterialModule
  ]
})
export class OrganizationModule { }
