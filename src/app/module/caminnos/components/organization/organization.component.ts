import { Component, OnInit } from '@angular/core';
import { Organization } from '@core/model/caminnos/organization.model';
import { AlianceCaminnosService } from '@core/services/caminnos/aliance-caminnos/aliance-caminnos.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.css']
})
export class OrganizationComponent implements OnInit {
  organizations: Organization[];
  state: boolean;

  constructor(
    private allianceService: AlianceCaminnosService
  ) { }

  ngOnInit(): void {
    this.getOrganization();
  }
  getOrganization(): void {
    this.allianceService.getAllSponsors()
    .subscribe(
      (res: Organization[]) => {
        this.organizations = res;
      }
    )
  }

  addOrganization(): void {
    this.state = true;
    const data: Organization = {
      id: '',
      image: 'https://scontent.fsrz1-2.fna.fbcdn.net/v/t1.6435-9/80206598_149659809777922_537181438135500800_n.jpg?_nc_cat=104&ccb=1-5&_nc_sid=973b4a&_nc_ohc=AI_9pvtwniYAX8AIj4J&_nc_ht=scontent.fsrz1-2.fna&oh=79e3e148c39c4b5984e1df488bc2e230&oe=61983F4E',
    }
    this.allianceService.addOrganization(data)
    .then(
      (res: any) => {
        this.state = false;
        // this.projects = res;
        Swal.fire({
          title: 'Agregado Correctamente',
          text: 'El nuevo proyecto se agregó correctamente',
          icon: 'success',
          showConfirmButton: false,
          timer: 2000
        })
        console.log(res);
      }
      )
    .catch(
      (error: any) => {
        this.state = false;
        console.log(error);
      },
    );
  }
}
