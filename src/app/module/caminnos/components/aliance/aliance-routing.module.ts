import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlianceListComponent } from './aliance-list/aliance-list.component';

const routes: Routes = [
  { path: '', component: AlianceListComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AlianceRoutingModule { }
