import { Component, OnInit } from '@angular/core';
import { AllianceBanner } from '@core/model/caminnos/alliance-banner.model';
import { Alliance } from '@core/model/caminnos/alliance.model';
import { AlianceCaminnosService } from '@core/services/caminnos/aliance-caminnos/aliance-caminnos.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-aliance-list',
  templateUrl: './aliance-list.component.html',
  styleUrls: ['./aliance-list.component.css']
})
export class AlianceListComponent implements OnInit {
  alliances: Alliance[];
  state: boolean;
  banner: AllianceBanner;
  constructor(
    private allianceService: AlianceCaminnosService
  ) { }

  ngOnInit(): void {
    this.getAlliances();
    this.getBanner();
  }

  getBanner(){
    this.allianceService.getAllianceBanner()
    .subscribe(
      (res: AllianceBanner[])=>{
        this.banner = res[0];
      }
    ),(error: any)=>{
      console.log(error);
      
    }
  }
  
  getAlliances(): void {
    this.state = true;
    this.allianceService.getAlliances()
    .subscribe(
      (res: Alliance[]) => {
        this.state = false;
        this.alliances = res;
      }
    );
  }

  addAlliance(): void {
    const alliance: Alliance = {
      id: '',
      title: 'Nueva Alianza',
      description: 'Reseña acerca de la alizanza',
      image: 'https://scontent.fsrz1-2.fna.fbcdn.net/v/t1.6435-9/80206598_149659809777922_537181438135500800_n.jpg?_nc_cat=104&ccb=1-5&_nc_sid=973b4a&_nc_ohc=AI_9pvtwniYAX8AIj4J&_nc_ht=scontent.fsrz1-2.fna&oh=79e3e148c39c4b5984e1df488bc2e230&oe=61983F4E',
      date: new Date()
    }
    this.allianceService.addAlliance(alliance)
    .then(
      (res: any) => {
        this.state = false;
        // this.projects = res;
        Swal.fire({
          title: 'Agregado Correctamente',
          text: 'La nueva alianza se agregó correctamente',
          icon: 'success',
          showConfirmButton: false,
          timer: 2000
        })
        console.log(res);
      }
      )
    .catch(
      (error: any) => {
        this.state = false;
        console.log(error);
      },
    );
  }

}
