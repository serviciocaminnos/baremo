import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlianceListComponent } from './aliance-list.component';

describe('AlianceListComponent', () => {
  let component: AlianceListComponent;
  let fixture: ComponentFixture<AlianceListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlianceListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlianceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
