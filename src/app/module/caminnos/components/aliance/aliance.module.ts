import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AlianceRoutingModule } from './aliance-routing.module';
import { AlianceListComponent } from './aliance-list/aliance-list.component';
import { AngularMaterialModule } from '@material/angular-material.module';
import { AllianceContainer } from './container/alliance/alliance.container';
import { ReactiveFormsModule } from '@angular/forms';
import { AllianceBannerComponent } from './container/alliance-banner/alliance-banner.component';


@NgModule({
  declarations: [
    AlianceListComponent,
    AllianceContainer,
    AllianceBannerComponent
  ],
  imports: [
    CommonModule,
    AlianceRoutingModule,
    AngularMaterialModule,
    ReactiveFormsModule
  ]
})
export class AlianceModule { }
