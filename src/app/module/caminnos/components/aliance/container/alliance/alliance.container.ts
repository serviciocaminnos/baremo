import { Component, Input, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Alliance } from '@core/model/caminnos/alliance.model';
import { AlianceCaminnosService } from '@core/services/caminnos/aliance-caminnos/aliance-caminnos.service';
import { finalize } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-alliance',
  templateUrl: './alliance.container.html',
  styleUrls: ['./alliance.container.css']
})
export class AllianceContainer implements OnInit {
  @Input() alliance: Alliance | any;
  isTitle: boolean;
  isDescription: boolean;
  statusUpload: boolean;
  imageSrc: any;
  allianceForm: FormGroup;
  uploadPercent: any;
  state: boolean = false;
  constructor(
    private storage: AngularFireStorage,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private allianceService: AlianceCaminnosService,
  ) {
    this.buildForm();
  }
  
  ngOnInit(): void {
    this.allianceForm.patchValue(this.alliance);
    this.allianceForm.controls.date.setValue(this.alliance.date.toDate());
  }

  buildForm(): void {
    this.allianceForm = this.formBuilder.group({
      id: [''],
      title: [''],
      description: [''],
      image: [''],
      date: [''],
    });
  }

  saveChange(option: string): void {
    this.state = true;
    this.showInput(option);
    const alliance: Alliance = this.allianceForm.value;
    // console.log(data);
    this.allianceService.editAlliance(alliance, this.alliance.id)
    .then(
      (res: any) => {
        this.state = false;
      }
      )
      .catch(
        (error: any) => {
        console.log(error);
      }
    );
  }


  changeDate(): void {
    this.saveChange('date');
  }
  deleteAlliance(): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: '¿Estas seguro de eliminar la alianza:' + this.alliance.title + '?',
      text: "No volveras a recuperarlo una vez eliminado",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.allianceService.deleteAlliance(this.alliance)
        .then(
          (res: any) => {
            swalWithBootstrapButtons.fire(
              '¡Eliminado!',
              'La Alianza se eliminó correctamente',
              'success'
            )
            this.deleteImage(this.alliance.image);
          }
        ).catch(
          (error: any) => {
            swalWithBootstrapButtons.fire(
              'Error',
              'Verifica tu conexión a internet o vuelva a intentarlo',
              'error'
            )
          }
        )

      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'La alianza esta a salvo',
          'error'
        )
      }
    })

  }

  upload(event: any): void {
    this.statusUpload = true;
    const file = event.target.files[0];
    const dir = 'alliance/' + this.alliance.id;
    const fileRef = this.storage.ref(dir);
    const task = this.storage.upload(dir, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges()
    .pipe(
      finalize(() => {
        this.statusUpload = false;
        this.imageSrc = fileRef.getDownloadURL();
        this.openSnackBar();
        this.imageSrc.subscribe(url => {
          this.allianceForm.controls.image.setValue(url);
          // this.image = url;
          // console.log(this.image);
          this.saveChange('image');
          // console.log(this.bannerForm.value);
        });
      })
    ).subscribe(() => {
    });
    // console.log(dir);
  }

  deleteImage(url) {
    return this.storage.storage.refFromURL(url).delete();
  }
  
  openSnackBar(): void {
    this.snackBar.open('La imagen se subio correctamente', 'Done', {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }

  showInput(option: string): void {
    switch (option) {
      case 'title':
        if (this.isTitle) {
          this.isTitle = false;
        } else {
          this.isTitle = true;
        }
        break;
      case 'description':
        if (this.isDescription) {
          this.isDescription = false;
        } else {
          this.isDescription = true;
        }
        break;
    
      default:
        this.isDescription = false;
        this.isTitle = false;
      break;
    }
  }

}
