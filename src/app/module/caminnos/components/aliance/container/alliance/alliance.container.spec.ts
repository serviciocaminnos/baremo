import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllianceContainer } from './alliance.container';

describe('AllianceContainer', () => {
  let component: AllianceContainer;
  let fixture: ComponentFixture<AllianceContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllianceContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllianceContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
