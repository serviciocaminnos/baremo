import { Component, Input, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AllianceBanner } from '@core/model/caminnos/alliance-banner.model';
import { AlianceCaminnosService } from '@core/services/caminnos/aliance-caminnos/aliance-caminnos.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-alliance-banner',
  templateUrl: './alliance-banner.component.html',
  styleUrls: ['./alliance-banner.component.css']
})
export class AllianceBannerComponent implements OnInit {
  @Input() banner: AllianceBanner;
  isTitle: boolean;
  isDescription: boolean;
  statusUpload: boolean;
  imageSrc: any;
  bannerForm: FormGroup;
  uploadPercent: any;
  state: boolean = false;
  constructor(
    private storage: AngularFireStorage,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private formBuilder: FormBuilder,
    private allianceService: AlianceCaminnosService,
  ) {
    this.buildForm();
  }
  
  ngOnInit(): void {
    this.bannerForm.patchValue(this.banner);
  }

  buildForm(): void {
    this.bannerForm = this.formBuilder.group({
      id: [''],
      title: [''],
      description: [''],
      image: [''],
    });
  }

  saveChange(option: string): void {
    this.state = true;
    this.showInput(option);
    const banner: AllianceBanner = this.bannerForm.value;
    // console.log(data);
    this.allianceService.editAllianceBanner(banner, this.banner.id)
    .then(
      (res: any) => {
        this.state = false;
      }
      )
      .catch(
        (error: any) => {
        console.log(error);
      }
    );
  }


  changeDate(): void {
    this.saveChange('date');
  }

  upload(event: any): void {
    this.statusUpload = true;
    const file = event.target.files[0];
    const dir = 'alliance-banner/' + this.banner.id;
    const fileRef = this.storage.ref(dir);
    const task = this.storage.upload(dir, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges()
    .pipe(
      finalize(() => {
        this.statusUpload = false;
        this.imageSrc = fileRef.getDownloadURL();
        this.openSnackBar();
        this.imageSrc.subscribe(url => {
          this.bannerForm.controls.image.setValue(url);
          // this.image = url;
          // console.log(this.image);
          this.saveChange('image');
          // console.log(this.bannerForm.value);
        });
      })
    ).subscribe(() => {
    });
    // console.log(dir);
  }
  
  openSnackBar(): void {
    this.snackBar.open('La imagen se subio correctamente', 'Done', {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }

  showInput(option: string): void {
    switch (option) {
      case 'title':
        if (this.isTitle) {
          this.isTitle = false;
        } else {
          this.isTitle = true;
        }
        break;
      case 'description':
        if (this.isDescription) {
          this.isDescription = false;
        } else {
          this.isDescription = true;
        }
        break;
    
      default:
        this.isDescription = false;
        this.isTitle = false;
      break;
    }
  }
}
