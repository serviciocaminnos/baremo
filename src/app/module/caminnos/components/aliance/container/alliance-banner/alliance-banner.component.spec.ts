import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllianceBannerComponent } from './alliance-banner.component';

describe('AllianceBannerComponent', () => {
  let component: AllianceBannerComponent;
  let fixture: ComponentFixture<AllianceBannerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllianceBannerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllianceBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
